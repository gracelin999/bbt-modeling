from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Get features from result files.

This assumes features already being extracted and stored in
result files. This is called from files such as gen-table.py.

---------------------------------------------------------------
"""

import re, copy, sys, os, math

#RESOURCE_DIR = '/Users/grace/dialogue/repos'
RESOURCE_DIR = os.path.join('..', '..', '..', 'resources')
sys.path.append(os.path.join(RESOURCE_DIR))
import common

# make LIWC as global
#LIWC_FEATURES = {}
LIWC_WORD_COUNT = 0

####
#### Get LIWC features
#### LIWC dictionary is global
#### We will skip the non-linguistic categories.
####
LIWC_skiplist = ['Social Processes','Family','Friends','Humans',
                'Affective Processes', 'Cognitive Processes', 
                'Perceptual Processes', 'Biological Processes', 'Body', 
                'Health', 'Sexual', 'Ingestion', 'Relativity', 'Motion', 
                'Space', 'Time', 'Work', 'Achievement', 'Leisure', 'Home', 
                'Money', 'Religion', 'Death']

def get_liwc_features(lines):
    global LIWC_WORD_COUNT
    LIWC_FEATURES = {}

    for line in lines:
        if not line: continue
        arr = line.split(':')
        cat = arr[0].strip()
        if cat == 'Word Count':
            LIWC_WORD_COUNT = float(arr[1])
            #print '====>', LIWC_WORD_COUNT
        elif cat not in LIWC_skiplist:
            LIWC_FEATURES['liwc-'+cat] = float(arr[1])/100
    return LIWC_FEATURES




####
#### Get concession polarity
####
def get_concess_polarity_features(lines):
    features={}
    for line in lines:
        if not line: continue
        arr = line.split(":")
        if 'concess all pos' in line:
            features['concess-pol-all-pos']=float(arr[1])
        elif 'concess all neg' in line:
            features['concess-pol-all-neg']=float(arr[1])
        elif 'concess all obj' in line:
            features['concess-pol-all-obj']=float(arr[1])
        elif 'pos' in arr[0] or 'neg' in arr[0] or 'obj' in arr[0]:
            m = re.search(r'\[(.+)\] (.+)', arr[0])
            #print arr[0]
            word = m.group(1)
            word = re.sub(' ','-', word)
            pol = m.group(2)
            #print word, pol
            features['concess-pol-'+word+'-'+pol]=float(arr[1])
    if features['concess-pol-all-pos'] > features['concess-pol-all-neg']:
        features['concess-pol-all'] = 1
    elif features['concess-pol-all-pos'] < features['concess-pol-all-neg']:
        features['concess-pol-all'] = -1
    else:
        features['concess-pol-all'] = 0
    return features


####
#### Get merge ratio for: Merge the subject and verb of two propositions
#### e.g., "Chanpen Thai has great service and nice decor"
####
def get_merge_features(lines):
    features={}
    for line in lines:
        if not line: continue
        arr = line.split(":")
        if 'MERGE ratio' in arr[0]:
            features['merge-ratio'] = float(arr[1])
    return features


####
#### Get tag question features
####
def get_tag_features(lines):
    features={}
    for line in lines:
        if not line: continue
        arr = line.split(":")
        #print line
        if 'Sentences with tag question' in arr[0]:
            count = int(arr[1])
            #print 'count',count
        elif 'Total sentences' in arr[0]:
            total = int(arr[1])
            #print 'total',total
    # make sure 'total' is not zero
    if total==0:
        print 'NOTE: tag feature: total is 0'
        features['tag-ratio'] = 0
    else:
        features['tag-ratio'] = count/total
    return features


####
#### Get verb strength
####
def get_verb_strength_features(lines):
    features={}
    for line in lines:
        if not line: continue
        arr = line.split(":")
        if 'Average verb strength' in arr[0]:
            features['verb-strength']=float(arr[1])
    #if PRINT_FEATURES: print "get_verb_strength_features", features
    return features


####
#### Get hedges info
####
# -----------------------------
#### hedges per sentence with hedge(s) : 
#   num of hedges found / num of sets with hedges
#   1: one hedge per sentence
#   >1: more than one hedge per sentence
#### hedges per utterance with hedge(s) : 
#   num of hedges found / num of utts with hedges
#   1: one hedge per utt
#   >1: more than one hedge per utt
#### percent sents with hedges: num of sents with hedges / num of sents
#### percent utts with hedges: num of utts with hedges / num of utts
# percent hedge 'very': num of 'very' / num of hedges
# percent hedge 'literally': num of 'literally' / num of hedges
#### percent hedge <word>: num of <word> / num of hedges
# -----------------------------
def get_hedges_features(lines):
    features={}
    for line in lines:
        line = line.strip()
        if not line: continue
        arr = line.split(":")
        if 'Number of sents' == arr[0]: #### todo: look for exact match
            num_sents = float(arr[1])
        elif 'Number of utts' == arr[0]:
            num_utts = float(arr[1])
        elif 'Number of sents with hedges' == arr[0]:
            num_sents_with_hedges = float(arr[1])
        elif 'Number of utts with hedges' == arr[0]:
            num_utts_with_hedges = float(arr[1])
        elif 'Number of hedges found' == arr[0]:
            num_hedges = float(arr[1])
        elif 'Hedges found' == arr[0]:
            # [('very', 1), ('literally', 1), ('kind of', 1), ('almost', 1)]
            hlist = {}
            s1 = arr[1].strip().strip('[]').strip()
            if not s1: continue     # check if there's actual hedges
            s2 = s1.split('),')
            for s in s2:
                #print s1, s2, s
                t = s.strip().strip('()').split(',')
                hlist[t[0].strip('\'')]=float(t[1])
                #print t[0], t[1]
        #print line

    #### calculations
    #### hedges per sentence with hedge(s) : 
    ####    num of hedges found / num of sets with hedges
    #print feat_content
    if num_hedges > 0:
        features['hedges-per-sent'] = num_hedges / num_sents_with_hedges
        #### hedges per utterance with hedge(s) : num of hedges found / num of utts with hedges
        features['hedges-per-utt'] = num_hedges / num_utts_with_hedges
        #### percent sents with hedges: num of sents with hedges / num of sents
        features['num-hedges-sent-ratio'] = num_sents_with_hedges / num_sents
        #### percent utts with hedges: num of utts with hedges / num of utts
        features['num-hedges-utt-ratio'] = num_sents_with_hedges / num_utts
        #### percent hedge <word>: num of <word> / num of hedges
        for h in sorted(hlist.keys()):
            features['num-hedge-'+h+'-ratio'] = hlist[h]/num_hedges
    return features


####
#### Get average content word length
####
def get_content_wlen_features(lines):
    features={}
    for line in lines:
        if not line: continue
        arr = line.split(":")
        if 'Average content word length' in arr[0]:
            features['avg-content-wlen']=float(arr[1])
    #if PRINT_FEATURES: print "get_cpontent_wlen_features", features
    return features



def convert_v(v):
    w = re.sub('\'','APOSTROPHE',v)
    w = re.sub('!','EXCLAMATION',w)
    return w

####
#### Most repeating verbs
####

def get_verb_features(lines):
    """
    lines of interest:
        total sentences: 129
        total vb count: 169
        total pvb count: 99
        total repeating vb sents: 5
        total repeating pvb sents: 1
        rep vb sents ratio: 0.0388
        rep pvb sents ratio: 0.0078
        ...
        Sorted rep vbvb sents: [u'be', u'get', 'do']
        vb verb: be 2   0.0338983050847
        vb verb: get    1   0.0169491525424
        vb verb: do 1   0.0169491525424
        ...
        Sorted rep pvbvb sents: ['do', u'get']
        pvb verb: do    1   0.0169491525424
        pvb verb: get   1   0.0169491525424

    """

    features={}

    for line in lines:
        line = line.strip()
        if not line: continue

        m = re.match('^rep vb sents ratio:(.+)', line)
        if m:
            ratio = m.group(1).strip()
            features['rep-vb-ratio']=ratio
            continue

        m = re.match('^rep pvb sents ratio:(.+)',line)
        if m:
            ratio = m.group(1).strip()
            features['rep-pvb-ratio']=ratio
            continue

        m = re.match('^vb verb:(.+)',line)
        if m:
            arr = m.group(1).split('\t')
            v = arr[0].strip()
            v = convert_v(v)
            num_sents = arr[1].strip()
            ratio = arr[2].strip()
            features['rep-vb-'+v+'-ratio']=ratio
            continue

        m = re.match('^pvb verb:(.+)',line)
        if m:
            arr = m.group(1).split('\t')
            v = arr[0].strip()
            v = convert_v(v)
            num_sents = arr[1].strip()
            ratio = arr[2].strip()
            features['rep-pvb-'+v+'-ratio']=ratio
            continue

    return features



####
#### Get specific words count
####
# Can come from two different feats: words, words_personage
def get_words_features(lines):
    catcount={}
    features={}
    liwc_wc = LIWC_WORD_COUNT #LIWC_FEATURES['liwc-Word Count']
    #print "====>", LIWC_WORD_COUNT

    for line in lines:
        line = line.strip()
        if not line: continue

        arr = line.split(":")

        if re.search(r'^fuckall', arr[0]):
            features['fuckall-count'] = float(arr[1])
            features['fuckall-ratio'] = float(arr[1])/liwc_wc if liwc_wc else 0

        elif re.search(r'^category',arr[0]) and re.search("total count",arr[0]):
            #print '----> debug:', arr[0], ',', arr[1]
            catword = re.search(r'\[(.+)\]', arr[0]).group(1)
            #print '----> debug:', catword
            features['category-'+catword+'-ratio'] = int(arr[1])/liwc_wc if liwc_wc else 0
            catcount[catword]=int(arr[1])
            #print '----> debug:', catcount[catword]

        elif re.search(r'^word',arr[0]):
            #print '----> debug:', catcount['taboo']
            m = re.search(r'\[(.+)\] in \[(.+)\]', arr[0])
            word = m.group(1)
            word = re.sub(' ','-', word)
            word = re.sub(',','comma',word)
            cat = m.group(2)
            features['word-'+word]=float(arr[1])
            if not catcount or catcount[cat]==0:
                # if no categories listed, then everything is probably 0
                val = 0
                if float(arr[1]) != 0:
                    print "ERROR. Check:", f, cat, arr[0], arr[1]
            else:
                val = float(arr[1])/catcount[cat]
            features['word-'+word+'-ratio-catw'] = val
            features['word-'+word+'-ratio-allw'] = float(arr[1])/liwc_wc if liwc_wc else 0

    return features



####
#### Get dialogue act as features
####
def get_dialogue_act_features(lines):
    features={}
    for line in lines:
        line = line.strip()
        if not line: continue
        arr = line.split(':',1)
        cat = arr[0]
        cat_str = ''
        if cat=='dial act fdist': cat_str = 'dialact-'
        elif cat=='dial act first fdist': cat_str = 'dialact-first-'
        elif cat=='dial act last fdist': cat_str =  'dialact-last-'
        else: print 'ERROR: dial act category recognized:', cat
        dialact = arr[1].split(' ')
        sumval=0
        features_temp = {}
        for d in dialact:
            if not d: continue
            (w,n) = d.split(':')
            w = w.strip()
            features_temp[cat_str+w]=int(n)
            sumval += int(n)
        # find ratio
        for key in features_temp.keys():
            features[key+'-ratio']=features_temp[key]/sumval
            # TODO: need to know number of turns for this conversation
            #features[key+'-per-turn']=features[key]/int(charinfo[iTurns])
    return features



####
#### Get polarity from sentiwordnet
####
def get_pol_sentiword_features(lines):
    features={}
    for line in lines:
        line = line.strip()
        if not line: continue

        feat = ''
        if "doc average score" in line: # average score over all sentences
            feat='polarity-overall'
        elif "doc percent pos" in line:
            feat='polarity-percent-pos'
        elif "doc percent neg" in line:
            feat='polarity-percent-neg'
        elif "doc percent obj" in line:
            feat='polarity-percent-obj'

        if feat:
            arr = line.split('\t')
            v = float(arr[1])
            if math.isnan(v): v = 0
            features[feat]=v

    # get an overall polarity based on sents %
    if features['polarity-percent-pos'] > features['polarity-percent-neg']:
        features['polarity-sents'] = 1
    elif features['polarity-percent-pos'] < features['polarity-percent-neg']:
        features['polarity-sents'] = -1
    else:
        features['polarity-sents'] = 0

    # look at polorization (extreme values or neutral)
    if features['polarity-percent-pos'] >= 0.75 or \
        features['polarity-percent-neg'] <= -0.75:
        features['polarization'] = 1    # extreme

    elif features['polarity-percent-pos'] >= 0.25 or \
        features['polarity-percent-neg'] <= -0.25:
        features['polarization'] = 0.5  # medium
    
    else:
        features['polarization'] = 0    # neutral

    return features



def get_actions_features(lines):
    features={}
    for line in lines:
        line = line.strip()
        if not line: continue
        arr = line.split('\t')
        if 'num_' not in arr[0]: continue
        if arr[0]=='num_sents_file': continue
        if 'num_prep_for_rhs_' in arr[0]: continue
        if 'num_prep_for_verbs_' in arr[0]: continue
        if 'num_prep_for_verbclass_' in arr[0]: continue
        """
        if (arr[0]=='num_prep_for_roles_you') or \
            (arr[0]=='num_prep_for_roles_your') or \
            (arr[0]=='num_prep_for_roles_we') or \
            (arr[0]=='num_prep_for_roles_us') or \
            (arr[0]=='num_prep_for_roles_my') or \
            (arr[0]=='num_prep_for_roles_myself') or \
            (arr[0]=='num_prep_for_roles_mine') or \
            (arr[0]=='num_prep_for_roles_me') or \
            (arr[0]=='num_prep_for_roles_i') or \
            (arr[0]=='num_prep_for_roles_his') or \
            (arr[0]=='num_prep_for_roles_him') or \
            (arr[0]=='num_prep_for_roles_herself') or \
            (arr[0]=='num_prep_for_roles_her') or \
            (arr[0]=='num_prep_for_roles_he') or \
            (arr[0]=='num_prep_for_roles_she'):
        """
        features[arr[0]] = float(arr[1])
    return features



def get_basic_features(lines):
    features={}
    for line in lines:
        line = line.strip()
        if not line: continue
        arr = line.split(':')
        feat = arr[0].strip()
        if (' per ' in feat) or ('RID primary' in feat) or \
            ('RID secondary' in feat) or ('RID emotions' in feat) \
            or ('concept to actual' in feat):
            features[feat]=float(arr[1])
    return features


def get_info_features(lines):
    features = {}
    for line in lines:
        line = line.strip()
        if not line: continue
        arr = line.split(':')
        feat = arr[0].strip()
        if feat=='info-addressee':
            # make this categorical
            arr2 = arr[1].strip().split(',')
            for a in arr2:
                features[feat+'-'+a.strip()] = 1
        else:
            features[feat]=arr[1].strip()
    return features


def get_actionsapi_features(lines):
    features = {}
    for line in lines:
        line = line.strip()
        if not line: continue
        arr = line.split(':')
        feat = arr[0].strip()
        val = arr[1].strip()
        features[feat]=val
    return features


def get_passive_features(lines):
    # Total passive: 2
    # Total sentences: 59
    features = {}
    total_passive = 0
    total_sents = 0
    for line in lines:
        line = line.strip()
        if not line: continue
        arr = line.split(':')
        if 'Total passive' == arr[0].strip():
            total_passive = float(arr[1].strip())
        elif 'Total sentences' == arr[0].strip():
            total_sents = float(arr[1].strip())
    # ratio
    features['passive-ratio'] = total_passive/total_sents
    return features


####
#### n-grams
####    
def get_ngram_features(lines): 
    features = {}
    for line in lines:
        line = line.strip()
        if not line: continue

        if line[0]=='(':
            arr = line.split('\t')
            feat = arr[0].strip()
            count = arr[1].strip()
            ratio = float(arr[2].strip())

            # further process feature
            # e.g., bigram (',', 'I')
            feat = re.sub(r'\(|\)','',feat) # get rid of parenthesis
            # get the tokens and concatenate
            arr2 = feat.split(', ')
            ngram = len(arr2)
            feat2 = ''
            for item in arr2:
                feat2 += '-'+common.proc_str_words(item[1:-1]) # get rid of quotes
                #print item, feat2
            # translate punctuations into words
            #feat2 = feat2[1:] 
            featname = 'ngram'+str(ngram)+'-'+feat2
            featname = re.sub('--', '-', featname)
            features[featname] = ratio

    return features



####
#### n-grams
####    
def get_pos_ngram_features(lines): 
    features = {}
    for line in lines:
        line = line.strip()
        if not line: continue

        if line[0]=='(':
            arr = line.split('\t')
            feat = arr[0].strip()
            count = arr[1].strip()
            ratio = float(arr[2].strip())

            # further process feature
            # e.g., bigram 
            #   (('.', '.'), ('I', 'PRP'))      4       0.0105263157895
            feat = feat[1:-1]   # get rid of outer parenthesis
            arr2 = feat.split('),')
            ngram = len(arr2)
            feat2 = ''
            # go through each ngram item
            for item in arr2:
                tagged = re.sub(r'\(|\)','',item) # get rid of parenthesis
                # split to get ngram
                arr3 = tagged.split(', ')
                word = re.sub('\'', '', arr3[0].strip())
                pos = re.sub('\'', '', arr3[1].strip())
                feat2 += '-'+common.proc_str_words(pos)
                #print word, pos, feat2

            featname = 'pos-ngram'+str(ngram)+'-'+feat2
            featname = re.sub('--', '-', featname)
            features[featname] = ratio

    return features




####
#### get info about one particulat feature
####
def get_feature_info(feat_content, feat):
    lines = feat_content.split('\n')
    fset = {}
    if feat == 'extract-merge':
        fset = get_merge_features(lines)
    elif feat == 'extract-concess-words-pol':
        fset = get_concess_polarity_features(lines)
    elif feat == 'extract-tag':
        fset = get_tag_features(lines)
    elif feat == 'extract-content-words':
        fset = get_content_wlen_features(lines)
    elif feat == 'extract-hedges':
        fset = get_hedges_features(lines)
    elif feat == 'extract-verb-strength':
        fset = get_verb_strength_features(lines)
    elif feat == 'extract-verbs':
        fset = get_verb_features(lines)
    elif feat == 'extract-words-personage':
        fset = get_words_features(lines)
    elif feat == 'extract-liwc':
        fset = get_liwc_features(lines)
    elif feat == 'extract-pol-sentiword':
        fset = get_pol_sentiword_features(lines)
    elif feat == 'extract-dialogue-act':
        fset = get_dialogue_act_features(lines)
    elif feat == 'extract-actions': # TODO: replace by actionapi?
        fset = get_actions_features(lines)
    elif feat == 'extract-basic':
        fset = get_basic_features(lines)
    elif feat == 'extract-info':
        fset = get_info_features(lines)
    elif feat == 'extract-actionapi':
        fset = get_actionsapi_features(lines)
    elif feat == 'extract-passive':
        fset = get_passive_features(lines)
    elif feat == 'extract-words':
        fset = get_words_features(lines)    # same as extract-words-personage
    elif 'extract-ngram' in feat:
        fset = get_ngram_features(lines) 
    elif 'extract-pos-ngram' in feat:
        fset = get_pos_ngram_features(lines)
    else:
        print 'ERROR: feature', feat, 'not supported.'
        return fset

    #print fset
    #if len(fset)==0:
    #    print 'NOTE: Empty feature set:', feat_content, feat, fset

    return fset


if __name__ == '__main__':

    if len(sys.argv) < 3:
        print 'Usage:', sys.argv[0], '<file path>', '<type of feature>'
        print 'No argument or not enough arguments passed in. Will use pre-defined directories.'
    elif len(sys.argv) > 3:
        print 'Usage:', sys.argv[0], '<file path>', '<type of feature>'
        print "You've provided more than two arguments. Only the first two will be used:", sys.argv[1], sys.argv[2]
    else:
        ffile = sys.argv[1]
        feat = sys.argv[2]

    print "----> Your file:", ffile
    print "----> Your feature:", feat

    fcontent = open(ffile).read()
    get_feature_info(fcontent, feat)

