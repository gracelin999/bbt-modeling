from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract Concession Words Polarity
---------------------------------------------------------------
"""

import sys, time, os, string, copy, pickle
import re, pickle
from collections import defaultdict 

import nltk
from nltk.corpus import wordnet as wn


#///////////////////////////////////////////////////////////////////// 
THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common

####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print 'No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print "You've provided more than three arguments. Only the first three will be used:", \
        sys.argv[1], sys.argv[2]

INDIR = sys.argv[1]
POSDIR = sys.argv[2]
OUTDIR = sys.argv[3]



logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input pickle:", INDIR
print "----> Your pos pickle:", POSDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 


#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock

#### keep track of elapsed time
START_TIME = time.time()



####
#### get polarity of a penn tagged sentence
####
def get_polarity(pos):
    sum = 0
    count = 0
    found_neg = 0
    found_emp = 0
    #words = sent.split(" ")
    for w in pos:
        if not w: continue
        #arr = w.split("/")
        arr = w
        wntag = common.penn2wn(arr[1])
        found = 1
        # only adjective, adverb, noun, and verb are considered
        if wntag=='a' or wntag=='r' or wntag=='n' or wntag=='v':
            score = common.swn_extract(arr[0], wntag)
            if score=='null':
                # check on synsets
                synsets = wn.synsets(arr[0], wntag)
                if not synsets:
                    #print arr[0], wntag
                    found = 0
                else:
                    #import inspect
                    #print inspect.getmembers(synsets[0], predicate=inspect.ismethod)
                    #print dir(synsets[0]) # shows data structure
                    lem_w = synsets[0].lemma_names()[0]
                    score = common.swn_extract(lem_w, wntag)
                    #print lem_w, score
                    if score=='null':
                        found = 0
            if found:
                sum += score
                count += 1
    if (count>0):
        final_score = sum/count
    else:
        final_score = 0    # to indicate that no polarity found
    #print "final score:", final_score
    return final_score

#### testing with a sentence
#sent = "the little girl kicking and screaming."
#text = nltk.word_tokenize(sent)
#pos = nltk.pos_tag(text)
#score = get_polarity(pos)
#print pos, score



# concession words for personage
concession = ['but', 'yet', 'although', 'though', 'while', 'whereas', 'however', 'notwithstanding', 'nonetheless', 'nevertheless', 'despite']
concession2 = ['even though', 'on the other hand', 'all the same', 'all the same time', 'even if', 'in spite of']


# process file form pos/ rather than re-parse
"""
def process_posfile(line):
    # return this format:
    # words:  ['if', 'it', "'s", 'unobserved', 'it', 'will', ','] 
    # tagged: [('if', 'IN'), ('it', 'PRP'), ("'s", 'VBZ'), ('unobserved', 'JJ'), ('it', 'PRP'), ('will', 'MD'), (',', ',')]
    tagged = []
    tokens = []
    sents = []
    
    arr = line.split()

    tag = []
    token = []
    sent = ''

    for item in arr:
        item = item.strip()
        arr2 = item.split('/')

        if len(arr2)>2:
            # e.g., # TCP/IP/NN
            word = '/'.join(arr2[:-1]).strip()
            pos = arr2[-1].strip()
        else:
            word = arr2[0].strip()
            pos = arr2[1].strip()

        tag.append((word, pos))
        token.append(word)
        
        #if pos in string.punctuation:
        #    sent += word
        #else:
        sent += ' '+word

        if pos == '.' or item==arr[-1]: # last item
            sents.append(sent.strip())
            sent = ''
            tagged.append(tag)
            tag = []
            tokens.append(token)
            token = []
            
    return tokens, tagged, sents
"""


def get_tokens_pos_from_posfile(partial_line, line_tagged):
    text_out = []
    pos_out = []
    
    # quick tokenization
    #partial_tokens = nltk.word_tokenize(partial_line)
    #print 'partial line:', partial_line
    #partial_line_arr = partial_line.split(' ')
    #partial_line_arr = [w.lower() for w in partial_line_arr if w]
    partial_line_nospace = re.sub(' ','',partial_line)
    #print 'partial line nospace:', partial_line_nospace

    # if partial_line is all '.', 

    i = 0

    #print 'partial_line:', len(partial_line), partial_line
    #print 'line_tagged:', len(line_tagged), line_tagged
    #print 'len of line_tagged', len(line_tagged)
    
    # get partial_line's pos tags
    wordstr = ''
    prevw = ''
    previ = ''
    
    for i in range(len(line_tagged)):
        word, pos = line_tagged[i]
        word = word.lower()
        
        #print previ, i, 'word:', word, 'wordstr:', wordstr, \
        #        '(',partial_line_nospace,')'

        if wordstr == partial_line_nospace:
            break

        if word in partial_line_nospace:
            if previ=='' or (i-1)==previ: # in sequence
                wordstr += word
                if wordstr not in partial_line_nospace:
                    wordstr = prevw+word
                    text_out = [prevw]
                    pos_out = []
                text_out.append(word)
                pos_out.append((word,pos))
                previ = i
                prevw = word
            elif (i-1)!=previ:
                wordstr = word
                text_out = [word]
                pos_out = [(word,pos)]
                previ = i
                prevw = word
            else:
                wordstr = ''
                previ = ''
                text_out = []
                pos_out = []

        #print word, pos, partial_tokens[i]
        #if (i < len(partial_tokens)) and (partial_tokens[i]==word):
        #    #found = True
        #    text_out.append(word)
        #    pos_out.append((word,pos))
        #    i+=1

    # get rid of leading punctuations, unless it's all '.'
    text_out2 = copy.deepcopy(text_out)
    #print 'text_out2:', text_out2

    tmp = ''.join(text_out2)
    if not re.match('\.+', tmp):
        #print 'text_out2:', text_out2
        while len(text_out2)>0 and (text_out2[0] in string.punctuation):
            text_out2 = text_out2[1:]
    
    #print 'text_out2 (after):', text_out2

    #print 'partial_line_nospace:', partial_line_nospace 
    
    partial_line_nospace2 = partial_line_nospace
    # skip if it's all '.'
    if not re.match('\.+', partial_line_nospace2):
        if (partial_line_nospace2[0]=="'") and \
            (partial_line_nospace[1] not in string.punctuation):
            # e.g., 'cause is ok
            pass
        else:
            while (len(partial_line_nospace2)>0) and \
                (partial_line_nospace2[0] in string.punctuation):
                partial_line_nospace2 = partial_line_nospace2[1:]
    
    #print 'partial_line_nospace2:', partial_line_nospace2

    if partial_line_nospace2 != ''.join(text_out2):
        print 'ERROR. Different text_out:', \
            partial_line_nospace2, ''.join(text_out2) #, ''.join(text_out2)

    return text_out, pos_out


def get_concess_parts_word(index, words_lower):

    concess_location = None

    if index > 0:
        #part1 = line_lower[:index] # 1st part = main point
        part1 = ' '.join(words_lower[:index])
        #part2 = line_lower[index+len(w_lower):] # 2nd part = concession
        part2 = ' '.join(words_lower[index+1:])
        main = part1.strip()
        concession = part2.strip() # might be empty if w_lower is the last word
        #print 'main, concession:', main, ',', concession
        concess_location = '2nd'

    else: # concession phrase somewhere in beginning
        sep_index = ''
        if ',' in words_lower:
            sep_index = words_lower.index(',')
        #if re.search(',', line_lower):  # see if there's comma
        #    sep_index = line_lower.index(',');
        else:
            concess_location = '1st'
            #print "BEGIN WITH CONCESSION without comma: "+line+"\n"
            return 0, concess_location

        #part1 = line_lower[index+len(w_lower):sep_index] # 1st part = concession
        part1 = ' '.join(words_lower[1:sep_index])
        #part2 = line_lower[sep_index+1:] # wnd part = main point
        part2 = ' '.join(words_lower[sep_index+1:])
        main = part2.strip()
        concession = part1.strip()
        concess_location = '1st'

    return main, concession, concess_location



def get_concess_parts_phrase(index, w_lower, line_lower):

    if index >= len(w_lower): # concession word somewhere in middle
        part1 = line_lower[:index] # 1st part = main point
        part2 = line_lower[index+len(w_lower):] # 2nd part = concession
        main = part1.strip()
        concession = part2.strip() # might be empty if w_lower is the last word
        #print 'main, concession:', main, ',', concession
        concess_location = '2nd'

    else: # concession phrase somewhere in beginning
        sep_index = ''
        if re.search(',', line_lower):  # see if there's comma
            sep_index = line_lower.index(',');
        else:
            concess_location = '1st'
            #print "BEGIN WITH CONCESSION without comma: "+line+"\n"
            return 0, concess_location

        part1 = line_lower[index+len(w_lower):sep_index] # 1st part = concession
        part2 = line_lower[sep_index+1:] # wnd part = main point
        main = part2.strip()
        concession = part1.strip()
        concess_location = '1st'

    return main, concession, concess_location


def get_concess_polarity(w, line, line_tagged):
    words_lower = [ww.lower() for ww, pp in line_tagged]
    line_lower = line.strip().lower()
    w_lower = w.strip().lower()

    #print 'word:', w_lower

    if w_lower in words_lower:
        index = words_lower.index(w_lower)
        #print 'index, words_lower:', index, words_lower
        main, concession, concess_location = \
            get_concess_parts_word(index, words_lower)
    else:
        # e.g., "on the other hand"
        index = line_lower.index(w_lower)
        #print 'index, line_lower:', index, line_lower   
        main, concession, concess_location = \
            get_concess_parts_phrase(index, w_lower, line_lower)

    #print 'index', index, 'word', w_lower, 'line', line_lower, 'main', main, 'concession', concession
    
    #### look at polarity of main point
    
    #text = nltk.word_tokenize(main)
    #pos = nltk.pos_tag(text)
    
    # use stanford corenlp
    #(text0, pos0) = common.get_tokens_pos(main)
    #score0 = get_polarity(pos0)

    # use pre-parsed pos file
    (text, pos) = get_tokens_pos_from_posfile(main, line_tagged)

    if len(pos)==0:
        print 'ERROR. POS empty for:', main, line_tagged
    #print 'main:', main
    #print 'line_tagged:', line_tagged
    #print 'text, pos:', text, ',', pos
    score = get_polarity(pos)
    #print 'score', score
    # sanity check
    #print pos0, pos
    #print score0, score
    #if score0 != score:
    #    print 'ERROR. Different score.'
    #    print '\tprevious score:', score0
    #    print '\tnew score:', score

    #### look at polarity of concession
    #con_text = nltk.word_tokenize(concession)
    #con_pos = nltk.pos_tag(con_text)
    if not concession:  # happens if it's the last word (todo: is this still concession)
        con_score = 0
    else:
        #(con_text0, con_pos0) = common.get_tokens_pos(concession)
        #con_score0 = get_polarity(con_pos0)
        (con_text, con_pos) = get_tokens_pos_from_posfile(concession, line_tagged)
        con_score = get_polarity(con_pos)
        # sanity check
        #print con_score0, con_pos0
        #print con_score, con_pos
        #if con_score0 != con_score:
        #    print 'ERROR. Different score.'
        #    print '\tprevious score:', con_score0
        #    print '\tnew score:', con_score

    #print "main:", main, " score:", score
    #print "concession: ", concession, " score:", con_score
    final_score = con_score # concession polarity
    #print "final score: ", final_score
    return final_score, concess_location




def gen_concess_count_polarity(infile_lines, posfile_lines, outfile):

    outfile_path = os.path.join(OUTDIR, outfile)
    OUTF = open(outfile_path, 'w')
    #lines = open(os.path.join(INDIR, infile)).readlines()
    #poslines = open(os.path.join(POSDIR,posfilename)).readlines()

    dic_concess_polarity = defaultdict(list)
    dic_concess_sents = defaultdict(dict)
    
    #print 'num lines for i', len(lines)

    for i, line in enumerate(infile_lines):
        #line = infile_lines[i].strip()
        line = line.strip()
        if not line: continue

        posline = posfile_lines[i]
        # list of sents
        (tokens, tagged, posfile_sents) = common.process_posfile(posline)
        #print 'from pos file:', sents
        # use pos file
        #sents_array = common.split_sents(line)
        #print 'from common:', sents_array

        # sanity check - should match with line
        """
        linex = re.sub(' ', '', line)
        poslinex = re.sub(' ', '', ''.join(posfile_sents))

        linex = re.sub(r'(``|\'\')','"', linex)
        linex = re.sub('<','{', linex)
        linex = re.sub('>','}', linex)

        poslinex = re.sub(r'(``|\'\')','"', poslinex)

        if linex != poslinex:
            print 'ERROR. Different string:'
            print '\tline:', linex
            print '\tposline:', poslinex
        """
        """
        (correct, linex, poslinex) = common.posfile_sents_check(line, posfile_sents)
        if not correct:
            print 'ERROR. Different string:'
            print '\tline:', linex
            print '\tposline:', poslinex
        """

        #print 'number of sents for j', len(posfile_sents)

        for j in range(len(posfile_sents)): #sents_array)):
            #sent_obj = sents_array[j]
            #if not sent_obj: continue
            sent = posfile_sents[j] #sent_obj.toString()
            if not sent: continue

            sent_tagged = tagged[j]
            sent_tokens = tokens[j]

            for w in concession2:
                foundcnt = len(re.findall(w, sent))
                #print 'foundcnt', foundcnt

                if foundcnt: # concession found
                    #print 'p1', 'w', w, 'sent', sent
                    #print 'w, sent, sent_tagged:', w, sent, sent_tagged
                    pol_score, concess_location = \
                        get_concess_polarity(w, sent, sent_tagged)

                    dic_concess_polarity[w].append(pol_score)

                    if concess_location:
                        if concess_location not in dic_concess_sents[w]:
                            dic_concess_sents[w][concess_location]=[sent]
                        else:
                            dic_concess_sents[w][concess_location].append(sent)
            
            for w in sent_tokens: #words:
                if w in concession: # found one-word concession only
                    #print 'p2', 'w', w, 'sent', sent, 'words', words
                    #print 'w, sent, sent_tagged', w, sent, sent_tagged
                    pol_score, concess_location = \
                        get_concess_polarity(w, sent, sent_tagged)

                    dic_concess_polarity[w].append(pol_score)

                    if concess_location:
                        if concess_location not in dic_concess_sents[w]:
                            dic_concess_sents[w][concess_location]=[sent]
                        else:
                            dic_concess_sents[w][concess_location].append(sent)
                    
    # write to output
    num_all_pos = 0
    num_all_neg = 0
    num_all_obj = 0

    for w in dic_concess_polarity.keys():

        # write out examples
        if w in dic_concess_sents:
            for ctype in sorted(dic_concess_sents[w]):
                #print w, ctype
                OUTF.write('concess ['+w+'] example '+ctype+': ')
                for sent in dic_concess_sents[w][ctype]:
                    OUTF.write(sent+'|')
                OUTF.write('\n')

        OUTF.write("concess ["+w+"]: ")

        num_pos = 0
        num_neg = 0
        num_obj = 0

        v = dic_concess_polarity[w]        
        for score in v:
            if score>0: num_pos+=1
            elif score<0: num_neg+=1
            else: num_obj+=1
            OUTF.write(str(score)+" ")
        OUTF.write("\n")

        num_all_pos += num_pos
        num_all_neg += num_neg
        num_all_obj += num_obj
        
        total = num_pos+num_neg+num_obj
        
        if (total > 0):
            OUTF.write("concess ["+w+"] pos: "+str(num_pos/total)+"\n")
            OUTF.write("concess ["+w+"] neg: "+str(num_neg/total)+"\n")
            OUTF.write("concess ["+w+"] obj: "+str(num_obj/total)+"\n")
        else:
            OUTF.write("concess ["+w+"] pos: 0\n")
            OUTF.write("concess ["+w+"] neg: 0\n")
            OUTF.write("concess ["+w+"] obj: 0\n")

    total = num_all_pos+num_all_neg+num_all_obj

    if (total > 0):
        OUTF.write("concess all pos: "+str(num_all_pos/total)+"\n")
        OUTF.write("concess all neg: "+str(num_all_neg/total)+"\n")
        OUTF.write("concess all obj: "+str(num_all_obj/total)+"\n")
    else:
        OUTF.write("concess all pos: 0\n")
        OUTF.write("concess all neg: 0\n")
        OUTF.write("concess all obj: 0\n")

    OUTF.close()

    return outfile_path



def test():
    #### testing with a sentence
    sent = "Brazil won the match, although they conceded a goal to Spain."
    score = get_concess_polarity('although', sent)
    print "\n"


    sent = "Although they conceded a goal to Spain, Brazil won the match."
    score = get_concess_polarity('although', sent)
    print "\n"

    sent = "They conceded a goal to Spain, although Brazil won the match."
    score = get_concess_polarity('although', sent)
    print "\n"

    sent = "Although Brazil won the match, they conceded a goal to Spain."
    score = get_concess_polarity('although', sent)
    print "\n"


    #### another sentence
    sent = "Although modern life has many problems such as pollution and \
            stress, nevertheless, improvements in health mean that life is \
            better than in the past."
    score = get_concess_polarity('although', sent)
    print "\n"

    sent = "Improvements in health mean that life is better than in the past, \
            although modern life has many problems such as pollution and \
            stress, nevertheless."
    score = get_concess_polarity('although', sent)
    print "\n"

    sent = "Although improvements in health mean that life is better than \
            in the past, nevertheless, modern life has many problems such \
            as pollution and stress."
    score = get_concess_polarity('although', sent)
    print "\n"

    sent = "Modern life has many problems such as pollution and stress, \
            although improvements in health mean that life is better than \
            in the past, nevertheless."
    score = get_concess_polarity('although', sent)
    print "\n"



def run():
    """ @note: run
    """
    input_info = pickle.load(open(INDIR))
    pos_info = pickle.load(open(POSDIR))

    data_info = {}

    for infile in sorted(input_info):

        # test specific file
        #if infile != 'leonard.txt': continue

        print "processing: " + infile
        
        if infile[0]=='.' or infile[-4:]!='.txt':
            print 'Not text file. Skip.'
            continue     # avoids hidden files (on Mac)

        infile_lines = input_info[infile].split('\n')
        #print 'num input lines:', len(infile_lines)

        posfile = re.sub('.txt', '.pos.txt', infile)
        posfile_lines = pos_info[posfile].split('\n')
        #print 'num pos lines:', len(posfile_lines)

        outfile_name = infile+'.out'
        outfile_path = gen_concess_count_polarity(infile_lines, \
                            posfile_lines, outfile_name)

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+'.pickle','w'))


if __name__ == '__main__':
    run()


#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()



