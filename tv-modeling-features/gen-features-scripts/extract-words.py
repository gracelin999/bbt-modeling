from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract words 
---------------------------------------------------------------
"""

import sys, time, os, re
#import collections, difflib
import nltk, string, pickle

#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 3:
    print 'Usage:', sys.argv[0], '<input Dir>', '<output dir>'
    print 'No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 3:
    print 'Usage:', sys.argv[0], '<Input Dir>', '<output dir>'
    print "You've provided more than two arguments. Only the first two will be used:", \
        sys.argv[1], sys.argv[2]

INDIR = sys.argv[1]
OUTDIR = sys.argv[2]

logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = 'extract-words-'+logfilename+'.log'

print "----> Your input directory:", INDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 


#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()




taboo = ['fuck', 'shit', 'hell', 'damn', 'darn', 'goddamn', 'ass', 'bitch', 
        'sucks', 'piss','cunt','cocksucker','motherfucker','tits']
taboo2 = ['jesus christ','oh my god']
seq_cues = ['first','second','third','next','last',"finally",'then', 'during', 
            'now', 'as', "while", 'already', 'recent', 'earlier', 'later', 
            'until', 'by', 'following', 'soon']
seq_cues2 = ['at the same time']
opinion_cues = ['think','feel', 'believe','should', 'must', 'seem', 'good', 
                'better', 'best', 'wonderful', 'nice', 'beautiful', 'bad', 
                'worse', 'worst', 'terrible', 'more', 'most', 'less','least', 
                'greatest']
opinion_cues2 = []
agg_words = ['with','also','because','since','so','although','but','though']
agg_words2 = ["even if"]
soft_hedges = ['somewhat','quite','around','rather']
soft_hedges2 = ['i think','it seems','it seems to me']
emp_hedges = ['really','basically', 'actually','just']
emp_hedges2 = []
ack_words = ['yeah', 'right','ok', 'oh']
ack_words2 = ['i see', 'oh well']
filled_pauses = []
filled_pauses2 = ['i mean', 'you know']

allwords = taboo + seq_cues + opinion_cues + agg_words + soft_hedges + \
            emp_hedges + ack_words + filled_pauses

allwords2 = taboo2 + seq_cues2 + opinion_cues2 + agg_words2 + soft_hedges2 + \
            emp_hedges2 + ack_words2 + filled_pauses2


def get_words(lines, outfile):

    outfile_path = os.path.join(OUTDIR, outfile)
    OUTF = open(outfile_path, 'w')
    
    fuck_count=0    # all, not just tokens
    total_words=0   # no punctuations
    sum_all=nltk.defaultdict(int)
    sum_cat=nltk.defaultdict(int)
    for line in lines:
        if not line: continue
        for w in allwords2:
            foundcnt=len(re.findall(w, line))   # count each phrase
            sum_all[w]+=foundcnt
            fuck_count += len(re.findall('fuck',line))
            if w in taboo2: sum_cat['taboo']+=foundcnt
            elif w in seq_cues2: sum_cat['seq']+=foundcnt
            elif w in opinion_cues2: sum_cat['opinion']+=foundcnt
            elif w in agg_words2: sum_cat['agg']+=foundcnt
            elif w in soft_hedges2: sum_cat['soft']+=foundcnt
            elif w in emp_hedges2: sum_cat['emp']+=foundcnt
            elif w in ack_words2: sum_cat['ack']+=foundcnt
            elif w in filled_pauses2: sum_cat['fill']+=foundcnt
        tokens = nltk.word_tokenize(line)
        tokens2 = [t.strip(string.punctuation) for t in tokens if t]
        tokens = [t for t in tokens2 if t]
        total_words += len(tokens)
        fdist = nltk.FreqDist([t for t in tokens])
        for w in allwords:
            foundcnt=fdist[w]
            sum_all[w]+=foundcnt
            if w in taboo: sum_cat['taboo']+=foundcnt
            elif w in seq_cues: sum_cat['seq']+=foundcnt
            elif w in opinion_cues: sum_cat['opinion']+=foundcnt
            elif w in agg_words: sum_cat['agg']+=foundcnt
            elif w in soft_hedges: sum_cat['soft']+=foundcnt
            elif w in emp_hedges: sum_cat['emp']+=foundcnt
            elif w in ack_words: sum_cat['ack']+=foundcnt
            elif w in filled_pauses: sum_cat['fill']+=foundcnt
    features={}
    #features['fuckall-ratio-allw']=fuck_count/total_words   # todo: might not be totally accurate

    #OUTF = open(windowroot+words_dir+f, 'w')
    OUTF.write("fuckall: "+str(fuck_count)+"\n")
    #print "---->sum_all", sorted(sum_all.items(), key=itemgetter(1), reverse=True)
    #print "---->sum_cat", sorted(sum_cat.items(), key=itemgetter(1), reverse=True)
 
    
    # total by category
    for cat in sorted(sum_cat.keys()):
        OUTF.write("category ["+cat+"] total count: "+str(sum_cat[cat])+"\n")
        
    # words in category
    for cat in sorted(sum_cat.keys()):
        if cat=='taboo': words = taboo+taboo2
        elif cat=='seq': words = seq_cues+seq_cues2
        elif cat=='opinion': words = opinion_cues+opinion_cues2
        elif cat=='agg': words = agg_words+agg_words2
        elif cat=='soft': words = soft_hedges+soft_hedges2
        elif cat=='emp': words = emp_hedges+emp_hedges2
        elif cat=='ack': words = ack_words+ack_words2
        elif cat=='fill': words = filled_pauses+filled_pauses2
        else: words = ["!!!! Not found !!!!"]
        OUTF.write("category ["+cat+"] words: "+str(words)+"\n")           

            
    for w in allwords+allwords2:
        #print "---->sum_all[w]", w, sum_all[w]
        #w2=re.sub(' ','-',w)
        #features[w2+'-ratio-allw']=sum_all[w]/total_words  # word count divided by total words
        catname = ''
        if w in taboo+taboo2: 
            total_cat = sum_cat['taboo']
            catname = 'taboo'
        elif w in seq_cues+seq_cues2: 
            total_cat = sum_cat['seq']
            catname = 'seq'
        elif w in opinion_cues+opinion_cues2: 
            total_cat = sum_cat['opinion']
            catname = 'opinion'
        elif w in agg_words+agg_words2: 
            total_cat = sum_cat['agg']
            catname = 'agg'
        elif w in soft_hedges+soft_hedges2: 
            total_cat = sum_cat['soft']
            catname = 'soft'
        elif w in emp_hedges+emp_hedges2: 
            total_cat = sum_cat['emp']
            catname = 'emp'
        elif w in ack_words+ack_words2: 
            total_cat = sum_cat['ack']
            catname = 'ack'
        elif w in filled_pauses+filled_pauses2: 
            total_cat = sum_cat['fill']
            catname = 'fill'
        else:
            catname = "!!!! Not found !!!!"
            
        if total_cat==0:
            #print "---->NOTE: divided by 0", w, sum_all[w], "/", total_cat
            if sum_all[w] != 0: print "---->ERR: numerator not 0"
            ratio = 0
        else:
            ratio = sum_all[w]/total_cat            
        #features[w2+'-ratio-catw']=ratio
        OUTF.write("word ["+w+"] in ["+catname+"] total count: "+str(sum_all[w])+"\n")
        
    #print "====>", features
    OUTF.close()
    #return features
            
    return outfile_path




def run():
    """ @note: run
    """
    input_info = pickle.load(open(INDIR))

    data_info = {}

    for infile in sorted(input_info):

        print "processing: " + infile

        if infile[0]=='.' or infile[-4:]!='.txt':
            continue     # avoids hidden files (on Mac)

        infile_lines = input_info[infile].split('\n')

        outfile_name = infile+'.out'
        outfile_path = get_words(infile_lines, outfile_name)

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+'.pickle','w'))



if __name__ == '__main__':
    run()



#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()