from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract polarity information based on SentiWordNet

Note: pass in 4 arguments, including path to SWN text/csv file.
---------------------------------------------------------------
"""

import sys, time, os
#import collections, difflib
import re, subprocess, pickle

#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

#change this to your location of tools
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')


####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 5:
    print 'Usage:', sys.argv[0], '<path to SWN3> <input dir>', '<output dir>', '<sent output dir>'
    print 'ERROR. No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 5:
    print 'Usage:', sys.argv[0], '<path to SWN3> <input dir>', '<output dir>', '<sent output dir>'
    print "You've provided more than four arguments. Only the first four will be used:", sys.argv[1], sys.argv[2], sys.argv[3]

SWN3_FPATH = sys.argv[1]
INDIR = sys.argv[2]
OUTDIR = sys.argv[3]
SENT_OUTDIR = sys.argv[4]


logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input dir:", INDIR
print "----> Your output dir:", OUTDIR
print "----> Your sentence output dir:", SENT_OUTDIR
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 

"""
sent = "Ah, not yet. I want to talk science with the science dudes."
open('test.out','w') # clear it out
outfile = open('test.out', 'a')
common.get_pos_to_file(sent, outfile)
"""

#quit()

#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()


"""
SWN3 requires input files tokenized with wordnet tag.
This should have been run ahead of time.
"""

#### remember to compile SWN3.java if modified
cmd = 'java -cp '+RESOURCE_DIR+' SWN3 '+ \
        SWN3_FPATH+' '+INDIR+' '+OUTDIR+' '+SENT_OUTDIR
print cmd
p=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
print p.communicate()[0]


data_info = {}

# store result to pickle file
for outf in os.listdir(OUTDIR):
    if outf[0]=='.': continue 
    data_info[outf]=open(os.path.join(OUTDIR,outf)).read()

# save to pickle file
pickle.dump(data_info, open(THISHEADER+'.pickle','w'))


#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()



