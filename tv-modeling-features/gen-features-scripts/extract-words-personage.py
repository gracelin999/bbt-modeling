from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract words related to Personage
---------------------------------------------------------------
"""

import sys, time, os, re, pickle
#import collections, difflib
import nltk, string


#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

#change this to your location of tools
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common



####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print 'No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print "You've provided more than three arguments. Only the first three will be used:", \
        sys.argv[1], sys.argv[2]

INDIR = sys.argv[1]
POSDIR = sys.argv[2]
OUTDIR = sys.argv[3]

logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input pickle:", INDIR
print "----> Your pos pickle:", POSDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 

#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()



####
#### Words for personage
####
# - concession words and polarity
# - contrast, justify, concede, in-group

# concession words
concession = ['but', 'yet', 'although', 'though', 'while', 'whereas', 'however', 'notwithstanding', 'nonetheless', 'nevertheless', 'despite']
concession2 = ['even though', 'on the other hand', 'all the same', 'all the same time', 'even if', 'in spite of']

concede = ['although', 'but', 'though']
concede2 = ['even if']
justify = ['because', 'since', 'so']
justify2 = []
contrast = ['while', 'but', 'however']
contrast2 = ['on the other hand']
conjunction = ['for', 'and', 'nor', 'but', 'or', 'yet', 'so' ]
conjunction2 = []
ingroup = ['pal', 'mate', 'buddy']
ingroup2 = []
near_swear = ['hell', 'heck', 'darn', 'sucks']
near_swear2 = ['jesus christ','oh my god']
with_word = []
with_word2 = [",with", ", with"]
also_word = []
also_word2 = [",also", ", also"]
relative = ['who','whom', 'whose', 'that', 'which', 'when', 'where', 'why']
relative2= []
misc = []
misc2 = ['kind of', 'sort of']

allwords = concession + concede + justify + contrast + conjunction + ingroup + near_swear + with_word + also_word+ relative + misc
allwords2 = concession2 + concede2 + justify2 + contrast2 + conjunction2 + ingroup2 + near_swear2 + with_word2 + also_word2+ relative2 + misc2

####
#### do a straight count of words
####
def gen_words_count(lines, poslines, outfile):

    outfile_path = os.path.join(OUTDIR, outfile)
    OUTF = open(outfile_path, 'w')

    total_words=0   # without punctuations
    total_words2=0

    sum_all=nltk.defaultdict(int)
    sum_cat=nltk.defaultdict(int)
    #dic_concess_polarity = {}
    # This is one line in the input file, which is actulaly an utterance.
    # An utterance can composed of several sentences.

    for i in range(len(lines)):

        line = lines[i].strip()
        if not line: continue

        for w in allwords2: # before tokenize, go through each phrase to see if they are in the current utterance
            foundcnt=len(re.findall(w, line))   # count each phrase
            sum_all[w]+=foundcnt
            if w in concession2:
                sum_cat['concession']+=foundcnt
                #if foundcnt: # if concession found, then determine polarity
                #    pol_score = get_concess_polarity(w, line)
                #    if w in dic_concess_polarity:
                #        dic_concess_polarity[w].append(pol_score)
                #    else:
                #        dic_concess_polarity[w]=[pol_score]
            if w in concede2: sum_cat['concede']+=foundcnt
            if w in justify2: sum_cat['justify']+=foundcnt
            if w in contrast2: sum_cat['contrast']+=foundcnt
            if w in conjunction2: sum_cat['conjunction']+=foundcnt
            if w in ingroup2: sum_cat['ingroup']+=foundcnt
            if w in near_swear2: sum_cat['near_swear']+=foundcnt
            if w in with_word2: sum_cat['with']+=foundcnt
            if w in also_word2: sum_cat['also']+=foundcnt
            if w in relative2: sum_cat['relative']+=foundcnt
            if w in misc2: sum_cat['misc']+=foundcnt

        #print 'line:', line

        #tokens = common.get_tokens_original_text(line)
        #tokens2 = [t.strip(string.punctuation) for t in tokens if t]
        #tokens = [t for t in tokens2 if t]
        #total_words += len(tokens)

        posline = poslines[i]
        (posfile_tokens, posfile_tagged, posfile_sents) = common.process_posfile(posline)
        posfile_tokens2 = []
        for item in posfile_tokens:
            posfile_tokens2 += item
        #posfile_tokens2 += [t1 for t in for t in posfile_tokens]
        posfile_tokens3 = [t.strip(string.punctuation) for t in posfile_tokens2 if t]
        posfile_tokens = [t for t in posfile_tokens3 if t]
        total_words2 += len(posfile_tokens)

        #if tokens != posfile_tokens:
        #    print 'ERROR. Mismatch found.'
        #    print '\t', tokens
        #    print '\t', posfile_tokens

        fdist = nltk.FreqDist([t for t in posfile_tokens])
        for w in allwords: # after tokenize
            foundcnt=fdist[w]
            sum_all[w]+=foundcnt
            if w in concession:
                sum_cat['concession']+=foundcnt
                #if foundcnt: # if concession found, then determine polarity
                #    pol_score = get_concess_polarity(w, line)
                #    if w in dic_concess_polarity:
                #        dic_concess_polarity[w].append(pol_score)
                #    else:
                #        dic_concess_polarity[w]=[pol_score]
            if w in concede: sum_cat['concede']+=foundcnt
            if w in justify: sum_cat['justify']+=foundcnt
            if w in contrast: sum_cat['contrast']+=foundcnt
            if w in conjunction: sum_cat['conjunction']+=foundcnt
            if w in ingroup: sum_cat['ingroup']+=foundcnt
            if w in near_swear: sum_cat['near_swear']+=foundcnt
            if w in with_word: sum_cat['with']+=foundcnt
            if w in also_word: sum_cat['also']+=foundcnt
            if w in relative: sum_cat['relative']+=foundcnt
            if w in misc: sum_cat['misc']+=foundcnt

    features={}

    #print "---->sum_all", sorted(sum_all.items(), key=itemgetter(1), reverse=True)
    #print "---->sum_cat", sorted(sum_cat.items(), key=itemgetter(1), reverse=True)

    # total by category
    for cat in sorted(sum_cat.keys()):
        OUTF.write("category ["+cat+"] total count: "+str(sum_cat[cat])+"\n")

    # words in category
    for cat in sorted(sum_cat.keys()):
        if cat=='concession': words = concession+concession2
        elif cat=='concede': words = concede+concede2
        elif cat=='justify': words = justify+justify2
        elif cat=='contrast': words = contrast+contrast2
        elif cat=='conjunction': words = conjunction+conjunction2
        elif cat=='ingroup': words = ingroup+ingroup2
        elif cat=='near_swear': words = near_swear+near_swear2
        elif cat=='with': words = with_word+with_word2
        elif cat=='also': words = also_word+also_word2
        elif cat=='relative': words = relative+relative2
        elif cat=='misc': words = misc+misc2
        else: words = ["!!!! Not found !!!!"]

        total_cat = sum_cat[cat]

        for w in words:
            if total_cat==0:
                #print "---->NOTE: divided by 0", w, sum_all[w], "/", total_cat
                if sum_all[w] != 0: print "---->ERROR. numerator not 0"
                ratio = 0
            else:
                ratio = sum_all[w]/total_cat

            OUTF.write("word ["+w+"] in ["+cat+"] total count: "+str(sum_all[w])+"\n")

    OUTF.close()
    return outfile_path


def get_words_personage(lines, poslines, outfile):
    # straight counting words
    outfile_path = gen_words_count(lines, poslines, outfile)
    # a little more involved ? TODO
    return outfile_path




def run():
    """ @note: run
    """
    input_info = pickle.load(open(INDIR))
    pos_info = pickle.load(open(POSDIR))

    data_info = {}

    for infile in input_info:
        print "processing: " + infile

        if infile[0]=='.' or infile[-4:]!='.txt':
            continue     # avoids hidden files (on Mac)

        infile_lines = input_info[infile].split('\n')
        
        posfile = re.sub('.txt', '.pos.txt', infile)
        posfile_lines = pos_info[posfile].split('\n')
        #print 'num pos lines:', len(posfile_lines)

        outfile_name = infile+'.out'
        outfile_path = get_words_personage(infile_lines, posfile_lines, outfile_name)

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+'.pickle','w'))


if __name__ == '__main__':
    run()



#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()

