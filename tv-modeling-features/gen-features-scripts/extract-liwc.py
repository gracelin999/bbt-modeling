from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract LIWC
---------------------------------------------------------------
"""

import sys, time, os, re, pickle
#import collections, difflib
#import re, nltk
from collections import defaultdict, Counter

#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

#change this to your location of tools
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common
import word_category_counter_new.word_category_counter as liwc

# output of running the LIWC 2007 software
#LIWC_SW_OUTPUT = './liwc-2007-output/LIWC results.txt'


####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 3:
    print 'Usage:', sys.argv[0], '<input liwc dir>', '<output dir>'
    print 'No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 3:
    print 'Usage:', sys.argv[0], '<input liwc Dir>', '<output dir>'
    print "You've provided more than two arguments. Only the first two will be used:", \
        sys.argv[1:]

INDIR = sys.argv[1]
OUTDIR = sys.argv[2]


logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input liwc dir:", INDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 

#quit()

#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


"""
From word_category_counter_new.word_category_counter:
To access it: liwc._dictionary._liwc_categories

    #(long_name, LIWC2007_number, LIWC2007_short, LIWC2001_number, LIWC2001_short)
    _liwc_categories =  [
    ('Total Function Words',1,'funct',None,None),
    ('Total Pronouns',2,'pronoun',1,'pronoun'),
    ('Personal Pronouns',3,'ppron',None,None),
    ('First Person Singular',4,'i',2,'i'),
    ('First Person Plural',5,'we',3,'we'),
    ('Second Person',6,'you',5,'you'),
    ('Third Person Singular',7,'shehe',None,None),
    ('Third Person Plural',8,'they',None,None),
    (' Impersonal Pronouns',9,'ipron',None,None),
    ('Articles',10,'article',9,'article'),
    ('Common Verbs',11,'verb',None,None),
    ('Auxiliary Verbs',12,'auxverb',None,None),
    ('Past Tense',13,'past',38,'past'),
    ('Present Tense',14,'present',39,'present'),
    ('Future Tense',15,'future',40,'future'),
    ('Adverbs',16,'adverb',None,None),
    ('Prepositions',17,'preps',10,'preps'),
    ('Conjunctions',18,'conj',None,None),
    ('Negations',19,'negate',7,'negate'),
    ('Quantifiers',20,'quant',None,None),
    ('Number',21,'number',11,'number'),
    ('Swear Words',22,'swear',66,'swear'),
    ('Social Processes',121,'social',31,'social'),
    ('Family',122,'family',35,'family'),
    ('Friends',123,'friend',34,'friends'),
    ('Humans',124,'humans',36,'humans'),
    ('Affective Processes',125,'affect',12,'affect'),
    ('Positive Emotion',126,'posemo',13,'posemo'),
    ('Negative Emotion',127,'negemo',16,'negemo'),
    ('Anxiety',128,'anx',17,'anx'),
    ('Anger',129,'anger',18,'anger'),
    ('Sadness',130,'sad',19,'sad'),
    ('Cognitive Processes',131,'cogmech',20,'cogmech'),
    ('Insight',132,'insight',22,'insight'),
    ('Causation',133,'cause',21,'cause'),
    ('Discrepancy',134,'discrep',23,'discrep'),
    ('Tentative',135,'tentat',25,'tentat'),
    ('Certainty',136,'certain',26,'certain'),
    ('Inhibition',137,'inhib',24,'inhib'),
    ('Inclusive',138,'incl',44,'incl'),
    ('Exclusive',139,'excl',45,'excl'),
    ('Perceptual Processes',140,'percept',27,'senses'),
    ('See',141,'see',28,'see'),
    ('Hear',142,'hear',29,'hear'),
    ('Feel',143,'feel',30,'feel'),
    ('Biological Processes',146,'bio',None,None),
    ('Body',147,'body',61,'body'),
    ('Health',148,'health',None,None),
    ('Sexual',149,'sexual',62,'sexual'),
    ('Ingestion',150,'ingest',63,'eating'),
    ('Relativity',250,'relativ',None,None),
    ('Motion',251,'motion',46,'motion'),
    ('Space',252,'space',41,'space'),
    ('Time',253,'time',37,'time'),
    ('Work',354,'work',49,'job'),
    ('Achievement',355,'achieve',50,'achieve'),
    ('Leisure',356,'leisure',51,'leisure'),
    ('Home',357,'home',52,'home'),
    ('Money',358,'money',56,'money'),
    ('Religion',359,'relig',58,'relig'),
    ('Death',360,'death',59,'death'),
    ('Assent',462,'assent',8,'assent'),
    ('Nonfluencies',463,'nonfl',67,'nonfl'),
    ('Fillers',464,'filler',68,'fillers'),
    ('Total first person',None,None,4,'self'),
    ('Total third person',None,None,6,'other'),
    ('Positive feelings',None,None,14,'posfeel'),
    ('Optimism and energy',None,None,15,'optim'),
    ('Communication',None,None,32,'comm'),
    ('Other references to people',None,None,33,'othref'),
    ('Up',None,None,42,'up'),
    ('Down',None,None,43,'down'),
    ('Occupation',None,None,47,'occup'),
    ('School',None,None,48,'school'),
    ('Sports',None,None,53,'sports'),
    ('TV',None,None,54,'tv'),
    ('Music',None,None,55,'music'),
    ('Metaphysical issues',None,None,57,'metaph'),
    ('Physical states and functions',None,None,60,'physcal'),
    ('Sleeping',None,None,64,'sleep'),
    ('Grooming',None,None,65,'groom')]
"""
def get_liwc_long_by_short(shortname):
    longname = ''
    found = False
    for tup in liwc._dictionary._liwc_categories:
        if tup[2]==shortname:
            longname = tup[0].strip()
            found = True
            break
    if not found:
        if shortname=='WC': # word count
            longname = 'Word Count'
        elif shortname=='WPS': # words per sentence
            longname = 'Words Per Sentence'
        elif shortname=='Qmarks':
            longname = 'Question Marks'
        elif shortname=='Sixltr':
            longname = 'Six Letter Words'
        elif shortname=='Unique':
            longname = 'Unique Words'
        elif shortname=='Dic':
             longname = 'Dictionary Words'
        else:
            longname = shortname
    return longname


"""
Parse the output of LIWC 2007 software.

wc = word_category_counter
sw = software

Make sure to run the software and save the result as a text file as 
"../liwc-2007-output/LIWC results.txt".

We use the 2007 lite version.

Header: 
Filename        WC      WPS     Qmarks  Unique  Dic     Sixltr  funct   
pronoun ppron   i       we      you     shehe   they    ipron   article verb    
auxverb past    present future  adverb  preps   conj    negate  quant   number  
swear   social  family  friend  humans  affect  posemo  negemo  anx     anger   
sad     cogmech insight cause   discrep tentat  certain inhib   incl    excl    
percept see     hear    feel    bio     body    health  sexual  ingest  relativ motion  
space   time    work    achieve leisure home    money   relig   death   assent  nonfl   
filler
"""


def get_liwc_from_software():
    # read liwc files
    lines = open(INDIR).readlines()
    header = lines[0].split('\t')
    #print LIWC_SW_OUTPUT, header
    #print 'Number of lines:', len(lines)

    data_info = {}

    for line in lines[1:]:
        if 'Filename\tWC' in line: 
            print line
            continue     # skip current line, in case of concatenated file

        arr = line.strip().split('\t')
        infile = arr[0]
        outfile_name = infile+'.out'
        outfile_path = os.path.join(OUTDIR, outfile_name)
        outf = open(outfile_path, 'w')

        # go through each category
        for i in range(1,len(arr)):
            value = arr[i]
            shortname = header[i].strip()
            longname = get_liwc_long_by_short(shortname)
            outf.write(longname+':'+str(value)+'\n')
        outf.close()

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()
    
    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+'.pickle','w'))


#### keep track of elapsed time
START_TIME = time.time()

def run():
    """ @note: run
    """
    get_liwc_from_software()


if __name__ == '__main__':
    run()


#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()