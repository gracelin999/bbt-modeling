from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract tag questions
---------------------------------------------------------------
Output file:
    TQ? indicates possible tag question
    T   indicates utterance labeled as tag question
"""

#import nltk,
import re, pprint, os, sys, time, random, codecs, string, pickle
from operator import itemgetter
import nltk

#### change this to your location of tools
#### use it to call Stanford's Core NLP
#sys.path.append(os.path.join('..','..','..','..','resources'))
#RESOURCE_DIR = '/Users/grace/dialogue/repos'
#sys.path.append(os.path.join(RESOURCE_DIR,'resources'))
#import common

#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print 'No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print "You've provided more than three arguments. Only the first three will be used:", \
        sys.argv[1], sys.argv[2]

INDIR = sys.argv[1]
POSDIR = sys.argv[2]
OUTDIR = sys.argv[3]


logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input pickle:", INDIR
print "----> Your pos pickle:", POSDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE


#///////////////////////////////////////////////////////////////////// 

#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock

#### keep track of elapsed time
START_TIME = time.time()

sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

####
#### tag question
####
def get_tag_question(lines, poslines, outfile):

    outfile_path = os.path.join(OUTDIR, outfile)
    OUTF = open(outfile_path, 'w')

    #OUTF = open(os.path.join(OUTDIR, outfile), 'w')
    #poslines = open(POSDIR+os.sep+posfilename).readlines()
    #lines = codecs.open(os.path.join(INDIR, infile),'r').readlines()
    #lines = text.split('\n')

    if len(poslines) != len(lines):
        print 'ERROR. pos file different size from input file.', len(lines), len(poslines)

    #print "---->", outfile
    OUTF.write("----> "+outfile+"\n")
    
    tag_questions = []
    count = 0   # count total num of sents with tag q's
    total = 0   # count total num of sents
    total_q = 0 # count total num of sents with possible tag q's (, ?)
    total_questions = 0 # count total questions

    for i in range(len(lines)):

        line = lines[i].strip()
        #sents_array = common.split_sents(line)
        
        #posline = poslines[i]
        #(tokens, tagged, posfile_sents) = common.process_posfile(posline)

        #(correct, linex, poslinex) = common.posfile_sents_check(line, posfile_sents)
        #if not correct:
        #    print 'ERROR. Different string:'
        #    print '\tline:', linex
        #    print '\tposline:', poslinex
        
        # returns an ArrayList of stanford.nlp.pipeline.Annotation objects
        # use sents[i].toString() to get the text
        
        sents = sent_tokenizer.tokenize(line)

        #for sent_obj in sents_array:
        for j in range(len(sents)):
            #if not sent_obj: continue
            #sent = sent_obj.toString()
            sent = sents[j]
            if not sent: continue
            #print '****', sent, type(sent)

            # all possible tag questions
            m = re.search(".+(,|-)+.+\?$", sent)
            if m:
                #print "TQ?", sent
                OUTF.write("TQ? "+sent+"\n")
                total_q += 1
            total += 1

            # all questions
            m = re.search("\?$", sent)
            if m:
                total_questions += 1

            # look for tag questions
            # TODO:
            # , you remember dont'ya?
            # , how are they?
            #exp = ".+(,|-)+\s*.*\s*(have|haven't|has|hasn't|would|wouldn't|should|shouldn't|can|can't|will|won't|was|wasn't|had|hadn't|must|mustn't|were|weren't|is|isn't|right|could|couldn't|ain't|are|aren't|am|do|don't|does|doesn't|did|didn't)\s+(i|you|ya|he|she|they|we|it|there)\s*"
            exp = ".+(,|-)+\s*(?!how)\w*(have|haven't|has|hasn't|would|wouldn't|should|shouldn't|can|can't|will|won't|was|wasn't|had|hadn't|must|mustn't|were|weren't|is|isn't|right|could|couldn't|ain't|are|aren't|am|do|don't|dont|does|doesn't|did|didn't)[\s']+(i|you|ya|he|she|they|we|it|there)\s*\w{0,5}"
            end = "\?$"
            x = re.search(exp+end, sent)
            end2 = "([,-]+\s*(?!how).*|miss\s*[a-z0-9]*|mister\s*[a-z0-9]*|mr\.*\s*[a-z0-9]*|mrs\.*\s*[a-z0-9]*|ms\.*\s*[a-z0-9]*|\s*)\?$"
            i = re.search(exp+end2, sent)

            #if not i:
            #    end3 = "\s*([a-z0-9]+)\s*\?$"
            #    i = re.search(exp+end3, sent)
            #    if i:
            #        i = re.search(end3, sent)
            #        possiblename = i.group(1)
            #        print "\NAME?", i.group(1)
                    #if possiblename not in allnames:
                    #    i = None

            if x or i:
                z = re.search(".+(,|-)+\s*(where).*", sent) # found "where"
            else:
                z = None

            j = re.search(".+(,|-)+\s*(right|am i right|is that right|okay|ok|all right|alright|eh|no|remember|good|pretty good|doncha|correct|ya know|you know|yeah|willya|shall we|you see|see|don't you think|don't you see|do you think|do you see|y'hear|is that it|is this it|hear|you hear|crystal)\s*(\w{0,5},+\w{0,5}|\s*)\?$", sent)

            #if not j:
            #    k1 = re.search("(,|-)+.*(right)\?$", sent)
            #    if k1: print "CHECK THIS:", sent

            k = re.search("[,|-]+\s*([a-z0-9\s]+)\s*\?$", sent)
            if k:
                miscitems = ['tough guy', 'hear', 'boy']
                if k.group(1) not in miscitems:
                   k = None

            l = re.search(".+(,|-)+.*hu(n*)h.*\?$",sent)
            if (z==None and (x or i)) or j or k or l:
                tag_questions.append(sent)
                #print "====> T:", sent
                OUTF.write("====> T: "+sent+"\n")
                count += 1

            #OUTF.write(str(i)+" "+str(j)+" "+str(k)+" "+str(l)+" "+str(x)+" "+str(z)+"\n");

    #for t in tag_questions:
    #    print "T:", t

    OUTF.write("\n")
    OUTF.write("Sentences with tag question: "+str(count)+"\n")
    OUTF.write("Sentences with possible tag questions: "+str(total_q)+"\n")
    OUTF.write("Sentences with question: "+str(total_questions)+"\n")
    OUTF.write("Total sentences: "+str(total)+"\n")
    OUTF.close()

    return outfile_path


def run():
    #common.split_sents("Bob the Builder quickly ate a stinky cheese sandwich. And he loved it!  Tom, on the other hand, isn't too impressed.")

    input_info = pickle.load(open(INDIR))
    pos_info = pickle.load(open(POSDIR))

    data_info = {}

    for infile in sorted(input_info):
        # test specific 
        #if infile != 'amy.txt': continue

        print "processing: " + infile

        if infile[0]=='.' or infile[-4:]!='.txt':
            continue     # avoids hidden files (on Mac)

        infile_lines = input_info[infile].split('\n')
        
        posfile = re.sub('.txt', '.pos.txt', infile)
        posfile_lines = pos_info[posfile].split('\n')
        #print 'num pos lines:', len(posfile_lines)

        outfile_name = infile+'.out'
        outfile_path = get_tag_question(infile_lines, posfile_lines, outfile_name)

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+'.pickle','w'))


if __name__ == '__main__':
    run()



#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time

sys.stdout = saveout
fsock.close()

