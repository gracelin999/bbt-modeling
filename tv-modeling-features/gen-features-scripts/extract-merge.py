from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract merging of subject and verb of two propositions
e.g., "Chanpen Thai has great service and nice decor"
---------------------------------------------------------------
"""

import sys, time, os
#import difflib, collections
import re, nltk, pickle


#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

#change this to your location of tools
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common


####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print 'No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print "You've provided more than three arguments. Only the first three will be used:", \
        sys.argv[1], sys.argv[2]

INDIR = sys.argv[1]
POSDIR = sys.argv[2]
OUTDIR = sys.argv[3]


logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input pickle:", INDIR
print "----> Your pos pickle:", POSDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 

#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()

# Merge:
# Merge subject and verb of two propositions
# e.g., "Chanpen Thai has great service and nice decor"

# verb + noun + conjunction + noun
#grammar_merge = "MERGE: {<NN.*|PRP.*>+<MD>*<RB.*>*<VB.*>+<JJ.*>*<NN.*>+<CC>+<JJ.*>*<NN.*>+}"
grammar_merge = "MERGE: {<VB.*>+<PDT>*<DT>*<JJ.*>*<PDT>*<DT>*<NN.*>+<CC><PDT>*<DT>*<JJ.*>*<PDT>*<DT>*<NN.*>+}"
cp_merge = nltk.RegexpParser(grammar_merge)


####
####  Chunking with grammar
####
def get_merge(lines, poslines, outfile):

    outfile_path = os.path.join(OUTDIR, outfile)
    OUTF = open(outfile_path, 'w')
    #OUTF = open(os.path.join(OUTDIR, outfile), 'w')
    #poslines = open(POSDIR+os.sep+posfilename).readlines()
    #lines = open(os.path.join(INDIR, infile)).readlines()

    count = 0
    total = 0

    for i in range(len(lines)):
        line = lines[i].strip()
        #sents_array = common.split_sents(line)

        posline = poslines[i]
        (posfile_tokens, posfile_tagged, posfile_sents) = common.process_posfile(posline)

        for j in range(len(posfile_sents)): #sents_array)):

            #sent_obj = sents_array[j]
            #if not sent_obj: continue
            #sent = sent_obj.toString()
            sent = posfile_sents[j]
            if not sent: continue

            #tokens_obj = common.get_tokens(sent) # use CoreNLP to get POS
            #tokens_arr = tokens_obj[0]


            # form an array of tuples (text, pos)
            #pos = []
            #for token_item in tokens_arr:
            #    pos.append(
            #        (token_item['OriginalText'],
            #        token_item['PartOfSpeech']))

            pos2 = posfile_tagged[j]

            #if pos != pos2:
            #    print 'ERROR. Mismatch:', pos, pos2

            found = 0
            result_merge = cp_merge.parse(pos2)
            productions = result_merge.productions()
            for prod in productions:
                if str(prod.lhs())=="MERGE":
                    OUTF.write(str(prod)+"\n")
                    found = 1
            if found:
                count+=1
                found = 0
            total+=1

    OUTF.write("Total sentences: "+str(total)+"\n")
    OUTF.write("MERGE sentences: "+str(count)+"\n")
    if total == 0:
        OUTF.write("MERGE ratio: 0\n")
    else:
        OUTF.write("MERGE ratio: "+str(count/total)+"\n")
    OUTF.close()

    return outfile_path


####
####  test sentences
####
def test():
    sents = ["hey look, Chanpen Thai has great service and nice decor",
             "okay, i'll just sleep but stay awake, okay?",
             "i'm supposed to buy her some noodles and a book and sit around listening to chicks who can't play their instruments",
             "the other one kinda short and undersexed",
             "my dog went to sleep with a concussion and woke up a vegetable"]
    for sent in sents:
        tokens_obj = common.get_tokens(sent) # use CoreNLP to get POS
        tokens_arr = tokens_obj[0]

        # form an array of tuples (text, pos)
        pos = []
        for token_item in tokens_arr:
            pos.append(
                (token_item['OriginalText'],
                token_item['PartOfSpeech']))

        result = cp_merge.parse(pos)
        productions = result.productions()
        #print pos
        #for prod in productions:
        #    if str(prod.lhs())=="MERGE":
        #        print prod



def run():
    """ @note: run
    """
    #test()

    input_info = pickle.load(open(INDIR))
    pos_info = pickle.load(open(POSDIR))

    data_info = {}

    for infile in sorted(input_info):
        #print "current file is: " + infile
        print "processing: " + infile

        # skip empty files
        if not input_info[infile]:
            print 'NOTE. Empty content. Skip:', infile
            continue

        # also avoid hidden files
        if infile[0]=='.' or infile[-4:]!='.txt':
            continue     # avoids hidden files

        infile_lines = input_info[infile].split('\n')

        #if 'sheldon.txt' not in infile: continue
        posfile = re.sub('.txt', '.pos.txt', infile)
        posfile_lines = pos_info[posfile].split('\n')
        #print 'num pos lines:', len(posfile_lines)

        outfile_name = infile+'.out'
        outfile_path = get_merge(infile_lines, posfile_lines, outfile_name)

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+'.pickle','w'))



if __name__ == '__main__':
    run()



#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()

