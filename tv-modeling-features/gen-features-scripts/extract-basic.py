from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract basic features 
(words per sentence, words per utterance, sents per utterance, etc.)
---------------------------------------------------------------
"""

import os, sys, time, re, pickle
import nltk
from nltk.corpus import stopwords

#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

#change this to your location of tools
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import en 	# nodebox.net linguistics

####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 4:
	print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>', '<output dir>'
	print 'No argument or not enough arguments passed in.'
	exit()

if len(sys.argv) > 4:
	print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>', '<output dir>'
	print "You've provided more than three arguments. Only the first three will be used:", sys.argv[1], sys.argv[2], sys.argv[3]

INDIR = sys.argv[1]
POSDIR = sys.argv[2]
OUTDIR = sys.argv[3]


logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input pickle:", INDIR
print "----> Your pos pickle:", POSDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 


#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()


# sentence tokenizer
sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
# WordNet lemmatizer: removes affixes if resulting word is in its dictionary
wnl = nltk.WordNetLemmatizer()


def lemm_words(wlist):
	wlist2 = []
	for w in wlist:
		w2 = wnl.lemmatize(w.strip().lower())
		wlist2.append(w2)
	return wlist2

def remove_stopwords(wlist):
	important_words = []
	for w in wlist:
		if w not in stopwords.words('english'):
			important_words.append(w)
	return important_words


def get_tokens_by_pos(lines, pos):
	if pos == 'verb':
		pat = '/VB'
	elif pos == 'adjective':
		pat = '/JJ'
	elif pos == 'adverb':
		pat = '/RB'
	elif pos == 'all':
		pat = ''

	words = []
	#count = 0
	for line in lines:
		arr = line.split()
		#arr = [m for m in re.finditer(pat, line)]
		for item in arr:
			if pat in item:
				#count+=1
				w = item.split('/')[0].strip()
				words.append(w)
	return words 
	

def get_words(wlist):
	words = []
	for w in wlist:
		if re.match('\w',w):
			words.append(w)
	return words

def get_lower(wlist):
	words = [w.lower() for w in wlist]
	return words


def get_concepts(toks_lemm):
	# Ogden's basic English words for expressing concepts
	arr = [w for w in toks_lemm if w.lower() in en.basic.words]
	wc=len(arr)
	arr = [w for w in toks_lemm if w.lower() in en.basic.verbs]
	vc=len(arr)
	arr = [w for w in toks_lemm if w.lower() in en.basic.adjectives]
	adjc=len(arr)
	arr = [w for w in toks_lemm if w.lower() in en.basic.adverbs]
	advc=len(arr)
	return wc, vc, adjc, advc


def get_emotions(words_lemm, type):
	# check if the words express emotion
	if (type != 'verb') and (type != 'adjective') and (type != 'adverb'):
		print 'ERROR. type not supported: ', type
		return

	words = []
	for w in words_lemm:
		if (type=='verb') and (en.verb.is_emotion(w)==True):
			words.append(w)
		elif (type=='adjective') and (en.adjective.is_emotion(w)==True):
			words.append(w)
		elif (type=='adverb') and (en.adverb.is_emotion(w)==True):
			words.append(w)
	return words

def get_categories(words_lemm):
	# Categorise words as emotional, persuasive or connective
	emo_words = []
	per_words = []
	conn_words = []
	for w in words_lemm:
		if en.is_basic_emotion(w)==True:
			emo_words.append(w)
		if en.is_persuasive(w)==True:
			per_words.append(w)
		if en.is_connective(w)==True:
			conn_words.append(w)
	return emo_words, per_words, conn_words


def process_text(lines, outfile):
	outfile.write("==== Process Text ===="+'\n')

	token_count = 0
	words_count = 0
	important_words_count = 0
	sents_count = 0
	utt_count = 0
	basic_concept_words_count = 0
	basic_concept_verbs_count = 0
	basic_concept_adjectives_count = 0
	basic_concept_adverbs_count = 0
	# important words
	basic_concept_iwords_count = 0
	basic_concept_iverbs_count = 0
	basic_concept_iadjectives_count = 0
	basic_concept_iadverbs_count = 0

	#txt = """Hello. This is goodbye. Or maybe not.\n But then again. So are we. Runs to me."""
	#lines = txt.split('\n')
	utt_count+=len(lines)
	for line in lines:
		sents = sent_tokenizer.tokenize(line.strip())
		sents_count+=len(sents)
		#print len(sents), sents
		for sent in sents:
			toks = nltk.word_tokenize(sent)
			words = get_words(toks)
			words_lemm = lemm_words(get_lower(words))
			words2 = remove_stopwords(words_lemm)			

			# Ogden's basic English words for expressing concepts
			w, v, adj, adv = get_concepts(words_lemm)
			#print w, v, adj, adv
			basic_concept_words_count+=int(w)
			basic_concept_verbs_count+=int(v)
			basic_concept_adjectives_count+=int(adj)
			basic_concept_adverbs_count+=int(adv)

			w, v, adj, adv = get_concepts(words2)
			basic_concept_iwords_count+=int(w)
			basic_concept_iverbs_count+=int(v)
			basic_concept_iadjectives_count+=int(adj)
			basic_concept_iadverbs_count+=int(adv)
			# token count
			token_count+=len(toks)
			words_count+=len(words)
			important_words_count+=len(words2)


	# tokesn per sentence; tokens per utterance
	outfile.write('tokens: '+str(token_count)+'\n')
	outfile.write('words: '+str(words_count)+'\n')
	outfile.write('important words: '+str(important_words_count)+'\n')
	outfile.write('sents: '+str(sents_count)+'\n')
	outfile.write('utts: '+str(utt_count)+'\n')
	tps = token_count/sents_count if sents_count else 0
	tpu = token_count/utt_count if utt_count else 0
	outfile.write('tokens per sent: '+str(tps)+'\n')
	outfile.write('tokesn per utt: '+str(tpu)+'\n')
	wps = words_count/sents_count if sents_count else 0
	wpu = words_count/utt_count if utt_count else 0
	outfile.write('words per sent: '+str(wps)+'\n')
	outfile.write('words per utt: '+str(wpu)+'\n')
	iwps = important_words_count/sents_count if sents_count else 0
	iwpu = important_words_count/utt_count if utt_count else 0
	outfile.write('important words per sent: '+str(iwps)+'\n')
	outfile.write('important words per utt: '+str(iwpu)+'\n')

	#print '---- From all words ----'
	if words_count==0:
		outfile.write('all words - concept words per word: 0\n')
		outfile.write('all words - concept verbs per word: 0\n')
		outfile.write('all words - concept adj per word: 0\n')
		outfile.write('all words - concept adverbs per word: 0\n')
	else:
		outfile.write('all words - concept words per word: '+str(basic_concept_words_count/words_count)+'\n')
		outfile.write('all words - concept verbs per word: '+str(basic_concept_verbs_count/words_count)+'\n')
		outfile.write('all words - concept adj per word: '+str(basic_concept_adjectives_count/words_count)+'\n')
		outfile.write('all words - concept adverbs per word: '+str(basic_concept_adverbs_count/words_count)+'\n')
		
	if basic_concept_words_count == 0:
		outfile.write('all words - concept verbs per concept word: 0\n')
		outfile.write('all words - concept adj per concept word: 0\n')
		outfile.write('all words - concept adverbs per concept word: 0\n')
	else:
		outfile.write('all words - concept verbs per concept word: '+str(basic_concept_verbs_count/basic_concept_words_count)+'\n')
		outfile.write('all words - concept adj per concept word: '+str(basic_concept_adjectives_count/basic_concept_words_count)+'\n')
		outfile.write('all words - concept adverbs per concept word: '+str(basic_concept_adverbs_count/basic_concept_words_count)+'\n')



	#print '---- From important words ----'
	if important_words_count==0:
		outfile.write('important words - concept words per important word: 0\n')
		outfile.write('important words - concept verbs per important word: 0\n')
		outfile.write('important words - concept adj per important word: 0\n')
		outfile.write('important words - concept adverbs per important word: 0\n')
	else:
		outfile.write('important words - concept words per important word: '+str(basic_concept_iwords_count/important_words_count)+'\n')
		outfile.write('important words - concept verbs per important word: '+str(basic_concept_iverbs_count/important_words_count)+'\n')
		outfile.write('important words - concept adj per important word: '+str(basic_concept_iadjectives_count/important_words_count)+'\n')
		outfile.write('important words - concept adverbs per important word: '+str(basic_concept_iadverbs_count/important_words_count)+'\n')

	if basic_concept_iwords_count==0:
		outfile.write('important words - concept verbs per concept word: 0\n')
		outfile.write('important words - concept adj per concept word: 0\n')
		outfile.write('important words - concept adverbs per concept word: 0\n')
	else:
		outfile.write('important words - concept verbs per concept word: '+str(basic_concept_iverbs_count/basic_concept_iwords_count)+'\n')
		outfile.write('important words - concept adj per concept word: '+str(basic_concept_iadjectives_count/basic_concept_iwords_count)+'\n')
		outfile.write('important words - concept adverbs per concept word: '+str(basic_concept_iadverbs_count/basic_concept_iwords_count)+'\n')
	

	#print en.basic.verbs

	# NodeBox English Linguistics is able to do psychological content analysis using John Wiseman's Python implementation of the Regressive Imagery Dictionary. 
	# The RID asigns scores to primary, secondary and emotional process thoughts in a text.
	# Primary: free-form associative thinking involved in dreams and fantasy
	# Secondary: logical, reality-based and focused on problem solving
	# Emotions: expressions of fear, sadness, hate, affection, etc.

	# You can find all the categories for primary, secondary and emotional scores in the en.rid.primary, en.rid.secondary and en.rid.emotions lists.
	summary = en.content.categorise(' '.join(lines))
	outfile.write('RID primary: '+str(summary.primary)+'\n')
	outfile.write('RID secondary: '+str(summary.secondary)+'\n')
	outfile.write('RID emotions: '+str(summary.emotions)+'\n')

	# The top 5 categories in the text:
	for category in summary[:5]:
		outfile.write('RID category '+category.name+' '+str(category.count)+'\n')

	# Words in the top <first> category:
	if len(summary) == 0:
		outfile.write('RID first category words:'+'\n')
	else:
		outfile.write('RID first category words:'+str(summary[0].words)+'\n')

	# NodeBox English Linguistics is able to strip keywords from a given text.
	# text to key words
	key_words = en.content.keywords(' '.join(lines))
	#outfile.write('key words: '+str(len(key_words))+' '+str(key_words)+'\n')
	if words_count==0:
		outfile.write('key words per word: 0\n')
	else:
		outfile.write('key words per word: '+str(len(key_words)/words_count)+'\n')


def process_pos(poslines, outfile):

	#f = open()
	"""
	txt = "About/IN time/NN ,/, right/NN ?/. \n \
				Hey/UH ./. \n \
				Wow/UH ,/, lucky/JJ you/PRP ./.\n \
				Sweet/JJ ./.\n \
				Yippee/RB ./. \n \
				Nice/JJ ./. Now/RB ,/, what/WP exactly/RB are/VBP toes/NNS ?/.\n \
				Oh/UH ,/, yeah/NN ./. Right/RB ./. Anyway/RB ,/, she/PRP takes/VBZ off/RP all/DT of/IN her/PRP$ clothes/NNS ,/, climbs/VBZ into/IN the/DT hot/JJ tub/NN ,/, and/CC the/DT first/JJ thing/NN I/PRP notice/VBP\n \
				Great/NNP ,/, I/PRP 'll/MD make/VB a/DT beer/NN run/NN ./."

	poslines = txt.split('\n')
	"""
	outfile.write('==== Process POS ====\n')
	# get words
	toks = get_tokens_by_pos(poslines, 'all')
	words = get_words(toks)
	words_lemm = lemm_words(get_lower(words))
	words2 = remove_stopwords(words_lemm)
	#print len(toks), 'tokens', toks
	#print len(words_lemm), 'words', words_lemm
	#print len(words2), 'important words', words2

	# get categories
	emo_words, per_words, conn_words = get_categories(words_lemm)
	outfile.write('all words - persuasive words:'+','.join(per_words)+'\n')
	if words_lemm:
		outfile.write('all words - emo words per word:'+str(len(emo_words)/len(words_lemm))+'\n')
		outfile.write('all words - persuasive words per word:'+str(len(per_words)/len(words_lemm))+'\n')
		outfile.write('all words - conn words per word:'+str(len(conn_words)/len(words_lemm))+'\n')
	else:
		outfile.write('all words - emo words per word: 0\n')
		outfile.write('all words - persuasive words per word: 0\n')
		outfile.write('all words - conn words per word: 0\n')		

	"""
	print '---- From all words ----'
	arr = [w for w in words_lemm if w in en.basic.verbs]
	print 'all words - contains concept verbs', arr
	print '\t', 'with ratio to important words', len(arr)/len(words_lemm)
	arr = [w for w in words_lemm if w in en.basic.adjectives]
	print 'all words - contains concept adj', arr
	print '\t', 'with ratio to important words', len(arr)/len(words_lemm)
	arr = [w for w in words_lemm if w in en.basic.adverbs]
	print 'all wrods - contains concept adv', arr
	print '\t', 'with ratio to important words', len(arr)/len(words_lemm)

	print '---- From important words ----'
	arr = [w for w in words2 if w in en.basic.verbs]
	print 'Contains concept verbs', arr
	print '\t', 'with ratio to important words', len(arr)/len(words2)
	arr = [w for w in words2 if w in en.basic.adjectives]
	print 'Contains concept adj', arr
	print '\t', 'with ratio to important words', len(arr)/len(words2)
	arr = [w for w in words2 if w in en.basic.adverbs]
	print 'Contains concept adv', arr
	print '\t', 'with ratio to important words', len(arr)/len(words2)
	"""

	#print '---- From actual POS words ----'
	# look for actual verbs
	verbs = get_tokens_by_pos(poslines, 'verb')
	verbs_lemm = lemm_words(get_lower(verbs))
	# how many of these verbs are concept verbs
	arr = [w for w in verbs_lemm if w in en.basic.verbs]
	outfile.write('actual verbs: '+str(verbs_lemm)+'\n')
	outfile.write('actual verbs that are concept verbs: '+str(arr)+'\n')
	# concept verbs per actual verb
	if len(verbs_lemm)==0:
		outfile.write('actual verbs - concept to actual: 0\n')
	else:
		outfile.write('actual verbs - concept to actual: '+str(len(arr)/len(verbs_lemm))+'\n')
		# emotions value of a word
		verbs_emo = get_emotions(verbs_lemm, 'verb')
		outfile.write('actual verbs - emo concept to actual: '+str(len(verbs_emo)/len(verbs_lemm))+'\n')
		# get categories
		emo_words, per_words, conn_words = get_categories(verbs_lemm)
		outfile.write('actual verbs - persuasive words:'+','.join(per_words)+'\n')
		outfile.write('actual verbs - emo words per verb:'+str(len(emo_words)/len(verbs_lemm))+'\n')
		outfile.write('actual verbs - persuasive words per verb:'+str(len(per_words)/len(verbs_lemm))+'\n')
		outfile.write('actual verbs - conn words per verb:'+str(len(conn_words)/len(verbs_lemm))+'\n')




	# look for actual adjectives
	adj = get_tokens_by_pos(poslines, 'adjective')
	adj_lemm = lemm_words(get_lower(adj))
	# how many of these are concepts
	arr = [w for w in adj_lemm if w in en.basic.adjectives]
	outfile.write('actual adj: '+str(adj_lemm)+'\n')
	outfile.write('actual adj that are concept adj: '+str(arr)+'\n')
	# concept per actual 
	if len(adj_lemm)==0:
		outfile.write('actual adj - concept to actual: 0\n')
	else:
		outfile.write('actual adj - concept to actual: '+str(len(arr)/len(adj_lemm))+'\n')
		# emotions value of a word
		adj_emo = get_emotions(adj_lemm, 'adjective')
		outfile.write('actual adj - emo concept to actual: '+str(len(adj_emo)/len(adj_lemm))+'\n')
		# get categories
		emo_words, per_words, conn_words = get_categories(adj_lemm)
		outfile.write('actual adj - persuasive words:'+','.join(per_words)+'\n')
		outfile.write('actual adj - emo words per adj:'+str(len(emo_words)/len(adj_lemm))+'\n')
		outfile.write('actual adj - persuasive words per adj:'+str(len(per_words)/len(adj_lemm))+'\n')
		outfile.write('actual adj - conn words per adj:'+str(len(conn_words)/len(adj_lemm))+'\n')

	# look for actual adverbs
	adv = get_tokens_by_pos(poslines, 'adverb')
	adv_lemm = lemm_words(get_lower(adv))
	# how many of these are concepts
	arr = [w for w in adv_lemm if w in en.basic.adverbs]
	outfile.write('actual adv: '+str(adv_lemm)+'\n')
	outfile.write('actual adv that are concept adv: '+str(arr)+'\n')
	# concept per actual 
	if len(adv_lemm)==0:
		outfile.write('actual adv - concept to actual: 0\n')
	else:
		outfile.write('actual adv - concept to actual: '+str(len(arr)/len(adv_lemm))+'\n')
		# emotions value of a word
		adv_emo = get_emotions(adv_lemm, 'adverb')
		outfile.write('actual adv - emo concept to actual: '+str(len(adv_emo)/len(adv_lemm))+'\n')
		# get categories
		emo_words, per_words, conn_words = get_categories(adv_lemm)
		outfile.write('actual adv - persuasive words:'+','.join(per_words)+'\n')
		outfile.write('actual adv - emo words per adv:'+str(len(emo_words)/len(adv_lemm))+'\n')
		outfile.write('actual adv - persuasive words per adv:'+str(len(per_words)/len(adv_lemm))+'\n')
		outfile.write('actual adv - conn words per adv:'+str(len(conn_words)/len(adv_lemm))+'\n')


def process_basic(lines, poslines, outfile):
	process_text(lines, outfile)
	process_pos(poslines, outfile)


def run():

	input_info = pickle.load(open(INDIR))
	pos_info = pickle.load(open(POSDIR))

	data_info = {}

	for infile in sorted(input_info):
		#print "current file is: " + infile
		print "processing: " + infile

		if infile[0]=='.' or infile[-4:]!='.txt':
			continue     # avoids hidden files 

		lines = input_info[infile].split('\n')
		print len(lines), lines[0]

		posfile = re.sub('.txt', '.pos.txt', infile)
		posfile_lines = pos_info[posfile].split('\n')
		print len(posfile_lines), lines[0]

		outfile_name = infile+'.out'
		outfile_path = os.path.join(OUTDIR, outfile_name)
		OUTF = open(outfile_path, 'w')

		process_basic(lines, posfile_lines, OUTF)
		OUTF.close()

		# store to dict for saving to pickle file later
		data_info[outfile_name]=open(outfile_path).read()

	# save to pickle file
	pickle.dump(data_info, open(THISHEADER+'.pickle','w'))



if __name__ == "__main__":
	run()

#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()

