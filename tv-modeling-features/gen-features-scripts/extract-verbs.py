from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract verb information
---------------------------------------------------------------
"""

import sys, time, os, copy, pickle, operator
from collections import defaultdict

import re, nltk
from nltk.corpus import propbank

#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

#change this to your location of tools
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common


####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print 'No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print "You've provided more than three arguments. Only the first three will be used:", \
        sys.argv[1], sys.argv[2], sys.argv[3]

INDIR = sys.argv[1]
POSDIR = sys.argv[2]
OUTDIR = sys.argv[3]


logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input pickle:", INDIR
print "----> Your pos pickle:", POSDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 


#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()


####
#### get repeating verbs and prop verbs 
####
propverbs = propbank.verbs()
wnl = nltk.WordNetLemmatizer()

####
#### Verbs and prop verbs
####
def process_verbs(sents_tag):
    tsents = 0
    tcnt = 0
    pcnt = 0
    
    repeat_vb_sent_cnt = 0    # regular verb
    repeat_pvb_sent_cnt = 0  # prop verb
    rep_vb_sents = defaultdict(list) # keep track of repeating verbs and sents
    rep_pvb_sents = defaultdict(list)

    allvb=[]  # regular verb
    allpvb=[] # prop verb

    for sent in sents_tag:
        if not sent: continue

        tsents += 1
        
        vb=[]
        pvb=[]
        sent_str = ''

        for (word, tag) in sent:
            sent_str += word+' '
            if re.search('VB', tag):
                tcnt += 1
                word = str(word).strip().lower()
                lemm = wnl.lemmatize(word, 'v')
                vb.append(lemm)
                if lemm in propverbs:
                    #print sent,":",word,"(",lemm,")", tag
                    pvb.append(lemm)
                    pcnt += 1

        allvb += vb
        allpvb += pvb

        if vb:
            fdist = nltk.FreqDist([p for p in vb])
            #print fdist.most_common()
            key, cnt = fdist.most_common()[0]
            if cnt>1: 
                repeat_vb_sent_cnt += 1
                rep_vb_sents[key].append(sent_str)
        else:
            #print "vb fdist: none"
            fdist = "none"

        if pvb:
            fdist = nltk.FreqDist([p for p in pvb])
            key, cnt = fdist.most_common()[0]
            if cnt>1:
                repeat_pvb_sent_cnt += 1
                rep_pvb_sents[key].append(sent_str)
        else:
            #print "pvb fdist: none"
            fdist = "none"

    if allvb:
        fdist_allvb = nltk.FreqDist([p for p in allvb])
        key, cnt = fdist_allvb.most_common()[0]
        #if cnt>1: print "vb fdist all:", fdist_allvb
    else:
        #print "vb fdist all: none"
        fdist_allvb = "none"

    if allpvb:
        fdist_allpvb = nltk.FreqDist([p for p in allpvb])
        key, cnt = fdist_allpvb.most_common()[0]
        #if cnt>1: print "pvb fdist all:", fdist_allpvb
    else:
        #print "pvb fdist all: none"
        fdist_allpvb = "none"

    return tsents, tcnt, pcnt, repeat_pvb_sent_cnt, fdist_allpvb, \
            repeat_vb_sent_cnt, fdist_allvb, \
            rep_vb_sents, rep_pvb_sents


####
#### tokenize the input file
####
def tokenize(lines, poslines):

    #lines = open(os.path.join(INDIR, infile)).readlines()
    #poslines = open(POSDIR+os.sep+posfilename).readlines()

    tokens_tag = []
    tokens_tag2 = []
    tokens = []
    tokens2 = []

    # go through each line
    for i in range(len(lines)):

        line = lines[i].strip()
        #sents_array = common.split_sents(line)

        posline = poslines[i]
        (posfile_tokens, posfile_tagged, posfile_sents) = \
            common.process_posfile(posline)

        for j in range(len(posfile_sents)): #sents_array)):
            #sent_obj = sents_array[j]
            #if not sent_obj: continue
            #sent = sent_obj.toString()
            sent = posfile_sents[j]
            if not sent: continue

            sent_tagged = posfile_tagged[j]
            tags2 = []
            for w, p in sent_tagged:
                tokens2.append(w)
                tags2.append((w,p))
            tokens_tag2.append(copy.deepcopy(tags2))

            # get tokens and POS
            #(t, pos) = common.get_tokens_pos(sent)
            #tokens += t  # merge lists
            #tokens_tag.append(pos)


    #if (tokens != tokens2):
    #    print 'ERROR. mismatch:'
    #    print tokens
    #    print tokens2
    #if (tokens_tag != tokens_tag2):
    #    print 'ERROR. mismatch:'
    #    print tokens_tag#[0][:10]
    #    print tokens_tag2#[0][:10]
    
    return tokens2, tokens_tag2



####
#### Get verb and propverb
####
def get_verbs(lines, poslines, outfile):

    (tokens, tokens_tag) = tokenize(lines, poslines)

    (tsents, tcnt, pcnt, repeat_pvb_sent_cnt, \
        fdist_allpvb, repeat_vb_sent_cnt, fdist_allvb, \
        rep_vb_sents, rep_pvb_sents) = \
            process_verbs(tokens_tag)

    #### write to output file
    outfile_path = os.path.join(OUTDIR, outfile)
    OUTF = open(outfile_path, 'w')

    #OUTF.write("total sentences|total verb count|total prop verb count|num repeat prop verb sent count|distr prop verb|num repeat verb sent count|distr all verb\n")
    OUTF.write('total sentences: '+str(tsents)+'\n')
    OUTF.write('total vb count: '+str(tcnt)+'\n')
    OUTF.write('total pvb count: '+str(pcnt)+'\n')
    OUTF.write('total repeating vb sents: '+str(repeat_vb_sent_cnt)+'\n')
    OUTF.write('total repeating pvb sents: '+str(repeat_pvb_sent_cnt)+'\n')

    if tsents:
        perc_rep_vb_sents = repeat_vb_sent_cnt/tsents
        perc_rep_pvb_sents = repeat_pvb_sent_cnt/tsents
    else:
        perc_rep_vb_sents = 0
        perc_rep_pvb_sents = 0

    OUTF.write('rep vb sents ratio: '+str(round(perc_rep_vb_sents,4))+'\n')
    OUTF.write('rep pvb sents ratio: '+str(round(perc_rep_pvb_sents,4))+'\n')

    #OUTF.write(str(tsents)+'|'+str(tcnt)+'|'+str(pcnt)+'|')
    #OUTF.write(str(repeat_pvb_sent_cnt)+'|')

    # sort by number of sents for each verb 

    for vtype in ['vb', 'pvb']:
        OUTF.write('\nSorted rep '+vtype+'vb sents: ')

        if vtype=='vb':
            rep_sents = rep_vb_sents
        else:
            rep_sents = rep_pvb_sents

        rep_sents_cnt = {}
        for v in rep_sents:
            rep_sents_cnt[v]=len(rep_sents[v])

        sorted_rep_sents_cnt = sorted(rep_sents_cnt.items(), \
                                key=operator.itemgetter(1), reverse=True)

        # print all verbs before listing ind sents
        vlist = [item[0] for item in sorted_rep_sents_cnt]
        OUTF.write(str(vlist)+'\n')

        i = 1
        for v, cnt in sorted_rep_sents_cnt:
            sents = rep_sents[v]
            ratio = len(sents)/tsents
            OUTF.write(vtype+' verb: '+v+'\t'+str(len(sents))+'\t'+str(ratio)+'\n')
            for sent in sents:
                OUTF.write('\t'+str(i)+'\t'+sent+'\n')
                i+=1

    OUTF.close()

    return outfile_path


def run():
    """ @note: run
    """
    input_info = pickle.load(open(INDIR))
    pos_info = pickle.load(open(POSDIR))

    data_info = {}

    for infile in sorted(input_info):
        # test specific 
        #if infile != 'amy.txt': continue
        print "processing: " + infile

        if infile[0]=='.' or infile[-4:]!='.txt':
            continue     # avoids hidden files (on Mac)

        infile_lines = input_info[infile].split('\n')
        
        posfile = re.sub('.txt', '.pos.txt', infile)
        posfile_lines = pos_info[posfile].split('\n')
        #print 'num pos lines:', len(posfile_lines)

        outfile_name = infile+'.out'
        outfile_path = get_verbs(infile_lines, posfile_lines, outfile_name)

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+'.pickle','w'))



if __name__ == '__main__':
    run()


#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()