from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract information related to the series, episodes, scene, etc.
---------------------------------------------------------------
"""

import os, sys, time, re

if len(sys.argv) < 3:
    print 'Usage:', sys.argv[0], '<input Dir>', '<output dir>'
    print 'No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 4:
    print 'Usage:', sys.argv[0], '<Input Dir>', '<output dir>'
    print "You've provided more than three arguments. Only the first three will be used:", \
        sys.argv[1], sys.argv[2], sys.argv[3]

INDIR = sys.argv[1]
OUTDIR = sys.argv[2]


logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = 'extract-info-'+logfilename+'.log'

print "----> Your input directory:", INDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE


#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()
"""
SCENE_CATEGORIES = ['living-room', 'cafeteria', 'cheesecake', 'comic', 'office', 'stair',
                    'leonards-car', 'pennys-car', 'bowling', 'laundry', 'lobby',
                    'hallway', 'restaurant', 'kitchen', 'hotel', 'bookstore',
                    'pennys-apartment', 'sheldon-and-leonard', 'sheldons-bedroom',
                    'pennys-door', 'the-apartment', 'leonards-bedroom', 'cinema',
                    'howards-bedroom', 'rajs-apartment', 'howards-house', 'hospital',
                    'gift-shop', 'train', 'leonards-lab', 'bernadettes-car',
                    'movie', 'amys-lab', 'gymnasium', 'leonards-room', 'supermarket',
                    'university', 'coffee', 'book', 'airport', 'party',
                    'physics', 'elevator', 'police', 'church', 'bathroom', 'casino', 
                    '-bar', 'sheldons-mothers-house', 'pennys-bed', 'tattoo', '-club',
                    '-camp', 'park', 'corridor', 'rajs-lab', 'conference', 'taxi',
                    'amys-apartment', 'wedding', 'rajs-car',
                    'bernadettes-bedroom', 'amys-car', 'amys-bedroom', 'pennys-bedroom',
                    'leonard-and-lesleys-lab', 'pennys-flat', 'electrical-store',
                    'renaissance', 'dmv', 'the-lab', 'climbing', 'paint', 
                    'pennys-front-door', 'courtroom', 'the-street',
                    'bernadettes-apartment', 'howard-and-bernadettes-apartment',
                    'amy-and-howards-apartment',
                    'dry-cleaners', 'roof', 'city-hall', 'capsule', 'space-station',
                    'howard-and-lesleys-lab', 'howards-lab', 'szechuan-palace',
                    'control-room', 'howards-front-door', 'girls-apartment', 'kurts-door',
                    'alicias-apartment', 'leonard-and-penny', 'shoe-shop', 'waiting-room',
                    'howards-workshop', 'the-store', 'rajs-room', '-cloth',
                    'basketball', 'mrs-wolowitzs', 'diner']
"""
SCENE_CATEGORIES = ['sheldon', 'leonard', 'penny', 'howard', 'raj',
					'amy', 'bernadette', 'living-room', 'bedroom', 'cafeteria', 'corridor',
					'-lab', '-car', 'university', 'kitchen', 'restaurant',
					'hallway', 'lobby', 'laundry']


#SCENE_CATEGORIES = ['apartment', 'cafeteria', 'cheesecake', 'comic', 'office', 'stair']
#SCENE_CATEGORIES = ['sheldon', 'leonard', 'penny', 'howard', 'raj',
#					'amy', 'bernadette',]

def extract_info(fname, outfile):
	# file name
	# e.g., series-6-episode-10-6-the-stairwell-char-howard-inpresenceof-raj-leonard.txt
	
    series = ''
    episode = ''
    scene = ''
    scene_desc = ''
    speaker = ''
    addressee = ''

    if 'all' in INDIR:
        m = re.match(r'\-char\-(.+)\-inpresenceof\-(.*)\.txt',fname)
        speaker = m.group(1)
        addressee = m.group(2)
    elif 'series' in INDIR:
        m = re.match(r'series\-(\d+)\-char\-(.+)\-inpresenceof\-(.*)\.txt',fname)
        series = int(m.group(1))
        speaker = m.group(2)
        addressee = m.group(3)
    elif 'episode' in INDIR:
    	m = re.match(r'series\-(\d+)\-episode\-(\d+)\-char\-(.+)\-inpresenceof\-(.*)\.txt',fname)
        series = int(m.group(1))
        episode = int(m.group(2))
        speaker = m.group(3)
        addressee = m.group(4)
    else:
        m = re.match(r'series\-(\d+)\-episode\-(\d+)\-(\d+)\-(.+)\-char\-(.+)\-inpresenceof\-(.*)\.txt',fname)
        series = int(m.group(1))
        episode = int(m.group(2))
        scene = int(m.group(3))
        scene_desc = m.group(4)
        speaker = m.group(5)
        addressee = m.group(6)

    if series:
        outfile.write('info-series:'+str(series)+'\n')
    if episode:
        outfile.write('info-episode:'+str(episode)+'\n')
    if scene:
        outfile.write('info-scene:'+str(scene)+'\n')
	#outfile.write('info-category:'+scene_desc+'\n')
    """
    found = False
    for c in SCENE_CATEGORIES:
        if c in scene_desc:
    		outfile.write('info-category:'+c+'\n')
    		found = True
    		break
    if not found:
    	outfile.write('info-category:other\n')
    	print 'other category:',scene_desc
    """
    if addressee:
        arr = addressee.split('-')
        outfile.write('info-addressee:'+','.join(arr))
        #outfile.write('info-addressee:'+addressee+'\n')


def run():
    listing = os.listdir(INDIR)
    for infile in listing:
        #print "current file is: " + infile
        if infile[0]=='.' or infile[-4:]!='.txt':
            continue     # avoids hidden files (on Mac)
        outfile = open(OUTDIR+os.sep+infile+'.out', 'w')
        extract_info(infile, outfile)
        outfile.close()




if __name__ == "__main__":
	run()



#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()




