from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract ngrams
---------------------------------------------------------------
"""

import sys, time, os, re
#import collections, difflib
import nltk, string, pickle


#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 4:
    print 'Usage:', sys.argv[0], '<input Dir>', '<output dir>', '<ngrams>'
    print 'No argument or not enough arguments passed in.'
    exit()

elif len(sys.argv) > 4:
    print 'Usage:', sys.argv[0], '<Input Dir>', '<output dir>', '<ngrams>'
    print "You've provided more than three arguments. Only the first three will be used:", \
			sys.argv[1], sys.argv[2], sys.argv[3]

INDIR = sys.argv[1]			
OUTDIR = sys.argv[2]
NGRAMS = int(sys.argv[3])


logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = 'extract-ngram-'+logfilename+'.log'

print "----> Your input directory:", INDIR
print "----> Your output directory:", OUTDIR
print "----> Your ngrams:", NGRAMS
print "----> Your log file:", LOGFILE

#quit()


#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()


def get_ngram(lines, outfile):

    outfile_path = os.path.join(OUTDIR, outfile)
    OUTF = open(os.path.join(OUTDIR, outfile), 'w')

    ngram_list = []
    for line in lines:
        if not line: continue
        tokens = nltk.word_tokenize(line)
        [ngram_list.append(p) for p in nltk.ngrams(tokens, NGRAMS)]
            
    fdist = nltk.FreqDist([p for p in ngram_list])
                
    OUTF.write(str(fdist)+"\n")
    OUTF.write(str(len(ngram_list))+"\n")

    # get top 10 (TODO: get rid of stop words)
    for b in fdist.keys()[:10]: # pick top 10
        OUTF.write(str(b)+"\t"+str(fdist[b])+"\t"+str(fdist[b]/len(ngram_list))+"\n")
        
    OUTF.close()
    
    return outfile_path
    
    

def run():
    """ @note: run
    """
    input_info = pickle.load(open(INDIR))

    data_info = {}

    for infile in sorted(input_info):
        #print "current file is: " + infile
        print "processing: " + infile

        if infile[0]=='.' or infile[-4:]!='.txt':
            continue     # avoids hidden files (on Mac)

        infile_lines = input_info[infile].split('\n')

        outfile_name = infile+'.out'
        outfile_path = get_ngram(infile_lines, outfile_name)

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+str(NGRAMS)+'.pickle','w'))



if __name__ == '__main__':
    run()



#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()