from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract passive sentences
Note: input directory rather than pickle file
---------------------------------------------------------------
"""

import sys, time, os, re, pickle

#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

#change this to your location of tools
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import passive

####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 3:
    print 'Usage:', sys.argv[0], '<input dir>', '<output dir>'
    print 'No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 3:
    print 'Usage:', sys.argv[0], '<input dir>', '<output dir>'
    print "You've provided more than two arguments. Only the first two will be used:", \
        sys.argv[1], sys.argv[2]

INDIR = sys.argv[1]
OUTDIR = sys.argv[2]

logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input pickle:", INDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 


#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock

#### keep track of elapsed time
START_TIME = time.time()


def gen_passive(infile, outfile):
    outfile_path = os.path.join(OUTDIR, outfile)
    OUTF = open(outfile_path, 'w')
    #lines = open(os.path.join(INDIR, infile)).readlines()
    passive.findpassives(os.path.join(INDIR, infile), OUTF)
    return outfile_path


def run():
    """ @note: run
    """
    #test()

    data_info = {}

    listing = os.listdir(INDIR)

    for infile in sorted(listing):
        #print "current file is: " + infile
        print "processing: " + infile
        
        if infile[0]=='.' or infile[-4:]!='.txt':
            continue     # avoids hidden files (on Mac)

        outfile_name = infile+'.out'
        outfile_path = gen_passive(infile, outfile_name)

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+'.pickle','w'))



if __name__ == '__main__':
    run()



#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()



