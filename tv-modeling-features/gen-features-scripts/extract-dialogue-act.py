from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract dialogue act
---------------------------------------------------------------
"""

import nltk, os, sys, time, re, pickle
from nltk.corpus import nps_chat

#///////////////////////////////////////////////////////////////////// 
THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

#### change this to your location of tools
#### including using it to call Stanford's Core NLP
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import swda
import common


####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print 'No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print "You've provided more than three arguments. Only the first three will be used:", \
        sys.argv[1], sys.argv[2]

INDIR = sys.argv[1]
POSDIR = sys.argv[2]
OUTDIR = sys.argv[3]


logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input pickle:", INDIR
print "----> Your pos pickle:", POSDIR
print "----> Your output pickle:", OUTDIR
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 

#### start a log file

saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()



####
#### Get dialogue act features
#### TODO: features could be better
####
def dialogue_act_features(post, posfile_tokens):
    #features={}
    #for word in nltk.word_tokenize(post):
    #    features['contains(%s)' % word.lower()] = True
    if posfile_tokens != '':
        #print posfile_tokens
        features2 = {}
        for word in posfile_tokens:
            #print word
            features2['contains(%s)' % word.lower()] = True

        #if features != features2:
        #    print 'ERROR. Mismatch.'
        #    print 'features', features
        #    print 'features2', features2
    else:
        features2 = {}
        for word in nltk.word_tokenize(post):
            features2['contains(%s)' % word.lower()] = True

    return features2



#### Train classifier with NB
posts = nps_chat.xml_posts()#[:10000]
featuresets = [(dialogue_act_features(post.text, ''), post.get('class')) for post in posts]
size = int(len(featuresets) * 0.1)
train_set, test_set = featuresets[size:], featuresets[:size]
classifier = nltk.NaiveBayesClassifier.train(featuresets)
print nltk.classify.accuracy(classifier, featuresets)


####
#### process dialogue
####
def process_dial_act(dial, poslines, outfile):
    results=[]
    first=[]
    last=[]
    first_sents=[]
    last_sents=[]

    for i in range(len(dial)):
        turn = dial[i].strip()
        if not turn: continue
        #sents = nltk.sent_tokenize(turn)

        #first_sents.append(sents[0])
        #last_sents.append(sents[-1])

        posline = poslines[i]
        (posfile_tokens, posfile_tagged, posfile_sents) = \
            common.process_posfile(posline)
        
        #print '\n', turn
        #print posfile_sents

        first_sents.append(posfile_sents[0])
        last_sents.append(posfile_sents[-1])

        #print sents[0]
        #print posfile_tokens[0]

        features_first = dialogue_act_features(posfile_sents[0], posfile_tokens[0])  # first sent in utterance
        first.append(classifier.classify(features_first))
        
        features_last = dialogue_act_features(posfile_sents[-1], posfile_tokens[-1]) # last sent in utterance
        last.append(classifier.classify(features_last))
        
        for j in range(len(posfile_sents)): #sents)):
            s = posfile_sents[j].strip()
            features = dialogue_act_features(s, posfile_tokens[j])
            results.append(classifier.classify(features))
            #print "dial act (",s,"):", results[-1]
    
    #### write to output file
    outfile_path = os.path.join(OUTDIR, outfile)
    OUTF = open(outfile_path, 'w')

    fdist=nltk.FreqDist([r for r in results])
    OUTF.write('dial act fdist: ')
    for key in fdist.keys():
        OUTF.write(key+':'+str(fdist[key])+' ')
    OUTF.write('\n')
    #print "dial act fdist:", fdist
    #print '""" First:', first_sents
    fdist_first=nltk.FreqDist([f for f in first])
    OUTF.write('dial act first fdist: ')
    for key in fdist_first.keys():
        OUTF.write(key+':'+str(fdist_first[key])+' ')
    OUTF.write('\n')
    #print "dial act first fdist:", fdist_first
    #print '""" Last:', last_sents
    fdist_last=nltk.FreqDist([f for f in last])
    OUTF.write('dial act last fdist: ')
    for key in fdist_last.keys():
        OUTF.write(key+':'+str(fdist_last[key])+' ')
    OUTF.write('\n')
    #print "dial act last fdist:", fdist_last
    #return fdist, fdist_first, fdist_last
    return outfile_path




####
#### Get dialogue act features
#### TODO: features could be better
####
def dialogue_act_features2(words):
    features={}
    for word in words:
        w = 'contains(%s)' % word.lower()
        if w not in features.keys():
            features[w] = True
    return features



####
#### dialogue act from SWDA
####
#if __name__ == "__main__":
def process_swda(dial='', outfile=''):
    # Transcript objects
    #file = os.path.join('swda','sw00utt','sw_0001_4325.utt.csv')
    metadata = os.path.join('swda','swda-metadata.csv')
    swda_dirs = ['sw00utt', 'sw01utt', 'sw02utt', 'sw03utt', 'sw04utt', \
                 'sw05utt', 'sw06utt', 'sw07utt', 'sw08utt', 'sw09utt', \
                 'sw10utt', 'sw11utt', 'sw12utt', 'sw13utt']
    featuresets2 = []
    for swda_dir in swda_dirs:
        listing = os.listdir(os.path.join(RESOURCE_DIR, 'resources', 'swda',swda_dir))
        for file in listing:
            if file[-4:] != '.csv': continue
            trans = swda.Transcript( \
                    os.path.join(RESOURCE_DIR,'resources','swda',swda_dir, file), \
                    os.path.join(RESOURCE_DIR,'resources',metadata))
            # trans.utterances: a list of utterances
            # trans.topic_description
            # trans.prompt
            # trans.talk_day
            # trans.talk_day.year
            # trans.talk_day.month
            # trans.from_caller_sex
            # utt = trans.utterances[80]
            # utt.act_tag           # original act tag
            # utt.damsl_act_tag()   # converted to 44 damsl act tag
            for utt in trans.utterances:
                # or use this: utt.pos_words(wn_lemmatize=True)
                fset = dialogue_act_features2(utt.pos_words())
                featuresets2.append((fset, utt.damsl_act_tag())) # as tuple

    classifier2 = nltk.NaiveBayesClassifier.train(featuresets2)
    print nltk.classify.accuracy(classifier2, featuresets2)





def run():

    input_info = pickle.load(open(INDIR))
    pos_info = pickle.load(open(POSDIR))

    data_info = {}

    for infile in sorted(input_info):
        #print "current file is: " + infile
        print "processing: " + infile

        if infile[0]=='.' or infile[-4:]!='.txt':
            continue     # avoids hidden files (on Mac)
        
        #if 'amy.txt' != infile: continue
        infile_lines = input_info[infile].split('\n')

        posfile = re.sub('.txt', '.pos.txt', infile)
        posfile_lines = pos_info[posfile].split('\n')
        
        outfile_name = infile+'.out'
        outfile_path = process_dial_act(infile_lines, posfile_lines, outfile_name)
        
        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+'.pickle','w'))



if __name__ == "__main__":
    run()


#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()
