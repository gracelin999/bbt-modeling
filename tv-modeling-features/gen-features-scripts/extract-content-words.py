from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract content words
---------------------------------------------------------------
"""

import sys, time, os, re, pickle
#import difflib, collections


#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

#change this to your location of tools
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common


####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print 'ERROR. No argument or not enough arguments passed in.'
    exit()

if len(sys.argv) > 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print "You've provided more than three arguments. Only the first three will be used:", \
        sys.argv[1], sys.argv[2]

INDIR = sys.argv[1]
POSDIR = sys.argv[2]
OUTDIR = sys.argv[3]

logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input pickle:", INDIR
print "----> Your pos pickle:", POSDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 

#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()


####
#### Lexicon word length
####
# use wordnet tag to find content words (noun, adj, adv, verb),
# then average the length of words (number of letters)
def get_lex_wlen(lines, poslines, outfile):

    outfile_path = os.path.join(OUTDIR, outfile)
    OUTF = open(outfile_path, 'w')

    total_words = 0
    sum = 0

    for i in range(len(lines)):
        line = lines[i].strip()
        #sents_array = common.split_sents(line)

        posline = poslines[i]
        (posfile_tokens, posfile_tagged, posfile_sents) = common.process_posfile(posline)


        for j in range(len(posfile_sents)): #sents_array)):
            #sent_obj = sents_array[j]
            #if not sent_obj: continue
            #sent = sent_obj.toString()
            sent = posfile_sents[j]
            if not sent: continue

            #tokens_obj = common.get_tokens(sent) # use CoreNLP to get POS
            #tokens_arr = tokens_obj[0]  # [[]]
            #print tokens_obj
            #print tokens_arr

            tagged = posfile_tagged[j]
            #print 'tokens_arr', tokens_arr
            #print 'tagged', tagged


            for k in range(len(tagged)): #tokens_arr)):

                #token_item = tokens_arr[k]
                #wntag = common.penn2wn(token_item['PartOfSpeech'])

                word, pos = tagged[k]
                wntag2 = common.penn2wn(pos)

                # sanity check
                #if token_item['OriginalText'] != word or \
                #    token_item['PartOfSpeech'] != pos or \
                #    wntag != wntag2:
                #    print 'ERROR. Mismatch.'
                #    print 'posfilename, posline', posfilename, posline
                #    print 'word', token_item['OriginalText'], 'pos', token_item['PartOfSpeech'], 'wntag', wntag
                #    print 'word', word, 'pos', pos, 'wntag2', wntag2, '\n'

                # only adjective, adverb, noun, and verb are considered
                if wntag2=='a' or wntag2=='r' or wntag2=='n' or wntag2=='v':
                    #OUTF.write(word+"\t"+tag+"\t"+wntag+"\t"+str(len(word))+"\n")
                    sum += len(word) #token_item['OriginalText'])
                    total_words += 1

    OUTF.write("Sum content words length: "+str(sum)+"\n")
    OUTF.write("Total content words: "+str(total_words)+"\n")
    if total_words:
        OUTF.write("Average content word length: "+str(sum/total_words)+"\n")
    OUTF.close()

    return outfile_path
    

def run():
    """ @note: run
    """
    input_info = pickle.load(open(INDIR))
    pos_info = pickle.load(open(POSDIR))

    data_info = {}

    for infile in sorted(input_info):
        print "processing: " + infile

        if infile[0]=='.' or infile[-4:]!='.txt':
            continue     # avoids hidden files (on Mac)

        infile_lines = input_info[infile].split('\n')
        
        posfile = re.sub('.txt', '.pos.txt', infile)
        posfile_lines = pos_info[posfile].split('\n')
        #print 'num pos lines:', len(posfile_lines)

        outfile_name = infile+'.out'
        outfile_path = get_lex_wlen(infile_lines, posfile_lines, outfile_name)

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+'.pickle','w'))


if __name__ == '__main__':
    run()


#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()