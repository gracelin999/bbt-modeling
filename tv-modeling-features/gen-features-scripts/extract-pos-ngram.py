from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract POS ngrams
---------------------------------------------------------------
"""

import sys, time, os, re
#import collections, difflib
import nltk, string, pickle


#///////////////////////////////////////////////////////////////////// 

THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

#change this to your location of tools
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common


####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 5:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>', '<output dir>', '<ngram>'
    print 'No argument or not enough arguments passed in.'
    exit()


if len(sys.argv) > 5:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>', '<output dir>', '<ngram>'
    print "You've provided more than four arguments. Only the first four will be used:", \
            sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]

INDIR = sys.argv[1]
POSDIR = sys.argv[2]
OUTDIR = sys.argv[3]
NGRAM = int(sys.argv[4])


logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input pickle:", INDIR
print "----> Your POS pickle:", POSDIR
print "----> Your output directory:", OUTDIR
print "----> Your ngrams:", NGRAM
print "----> Your log file:", LOGFILE

#///////////////////////////////////////////////////////////////////// 

#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()



def process_pos(line):
    # return this format:
    #    [('Hi', 'NNP'), (',', ','), ('Raj', 'NNP'), ('.', '.')]
    tagged = []
    tokens = []
    arr = line.split()
    for item in arr:
        item = item.strip()
        arr2 = item.split('/')
        if len(arr2)>2:
            # e.g., # TCP/IP/NN
            word = '/'.join(arr2[:-1]).strip()
            pos = arr2[-1].strip()
        else:
            word = arr2[0].strip()
            pos = arr2[1].strip()
        tagged.append((word, pos))
        tokens.append(word)
    return tagged, tokens


def get_pos_ngram(lines, poslines, outfile):

    #### write to output file
    outfile_path = os.path.join(OUTDIR, outfile)
    OUTF = open(outfile_path, 'w')

    ngram_list = []

    #mismatch = 0

    for i in range(len(lines)):
        line = lines[i].strip()
        if not line: continue

        # use nltk just to double check
        #sents = nltk.sent_tokenize(line)
        #tokens = []
        #for sent in sents:
        #    tokens += nltk.word_tokenize(sent)

        if i >= len(poslines):
            print 'ERROR. Ran out of POS lines:'
            print '\t', i, infile, posfile, line

        posline = poslines[i]
        pos_tokens, tagged, pos_sents= common.process_posfile(posline)

        tagged_together = []
        for item in tagged:
            tagged_together+=item

        # tokens and pos_tokens should match
        correct, linex, poslinex = common.posfile_sents_check(line, pos_sents)
        if not correct:
            print 'ERROR. Mismatch with line:'
            print '\tlinex', linex
            print '\tposlines', poslines

        """
        tokens_txt = ''.join(tokens)
        tokens_txt = re.sub(r'(``|\'\')','"', tokens_txt)
        tokens_txt = re.sub('<','{', tokens_txt)
        tokens_txt = re.sub('>','}', tokens_txt)

        pos_tokens_txt = ''.join(pos_tokens)
        pos_tokens_txt = re.sub(r'(``|\'\')','"', pos_tokens_txt)

        if tokens_txt!=pos_tokens_txt:
            mismatch+=1
            print 'NOTE. NLTK and POS file tokens do not match:', infile
            print '\tNLTK:', tokens_txt
            print '\tPOS:', pos_tokens_txt
        """

        # find ngrams
        [ngram_list.append(p) for p in nltk.ngrams(tagged_together, NGRAM)]
        #print tokens
        #print pos_tokens, tagged

        #sents = nltk.sent_tokenize(line)
        #for s in sents:
        #    tagged = nltk.pos_tag(nltk.word_tokenize(s))
        #    [ngram_list.append(p) for p in nltk.ngrams(tagged, NGRAM)]
        #    print tagged

    #if mismatch>3:
    #    print 'ERROR. Found some mismatches in original and POS tokens:', mismatch

    fdist = nltk.FreqDist([p for p in ngram_list])
    
    OUTF.write(str(fdist)+"\n")
    OUTF.write(str(len(ngram_list))+"\n")
    
    # get top 10 (TODO: get rid of stop words)
    for b in fdist.keys()[:10]:
        OUTF.write(str(b)+"\t"+str(fdist[b])+"\t"+str(fdist[b]/len(ngram_list))+"\n")
        
    OUTF.close()
    
    return outfile_path
    

def run():
    """ @note: run
    """
    input_info = pickle.load(open(INDIR))
    pos_info = pickle.load(open(POSDIR))

    data_info = {}

    for infile in sorted(input_info):
        #print "current file is: " + infile
        print "processing: " + infile

        if infile[0]=='.' or infile[-4:]!='.txt':
            continue     # avoids hidden files (on Mac)

        infile_lines = input_info[infile].split('\n')
        
        posfile = re.sub('.txt', '.pos.txt', infile)
        posfile_lines = pos_info[posfile].split('\n')
        #print 'num pos lines:', len(posfile_lines)

        outfile_name = infile+'.out'
        outfile_path = get_pos_ngram(infile_lines, posfile_lines, outfile_name)

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+str(NGRAM)+'.pickle','w'))


if __name__ == '__main__':
    run()



#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()