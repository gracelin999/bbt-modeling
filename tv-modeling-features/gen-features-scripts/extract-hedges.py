from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Extract hedges
---------------------------------------------------------------
"""

#import nltk, re, pprint, os, sys, time, random, codecs, string, difflib
#from operator import itemgetter
import sys, time, os
import re, difflib, pickle

from collections import defaultdict
from itertools import chain

from nltk.util import ngrams
import nltk

#///////////////////////////////////////////////////////////////////// 
THISFILE = os.path.basename(__file__)
THISHEADER = re.sub('.py', '', THISFILE)
print 'this file:', THISFILE
print 'this header:', THISHEADER

#change this to your location of tools
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common

####
#### User can pass in directories names.  Otherwise use pre-defined
#### directories internally.
####
if len(sys.argv) < 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print 'No or not enough arguments passed in.'
    exit()

if len(sys.argv) > 4:
    print 'Usage:', sys.argv[0], '<input pickle>', '<pos pickle>' '<output dir>'
    print "You've provided > 3 arguments. Only the first three will be used:", \
            sys.argv[1], sys.argv[2]

INDIR = sys.argv[1]
POSDIR = sys.argv[2]
OUTDIR = sys.argv[3]


logfilename = '-'.join(OUTDIR.split(os.sep))
LOGFILE = THISHEADER+'-'+logfilename+'.log'

print "----> Your input pickle:", INDIR
print "----> Your pos pickle:", POSDIR
print "----> Your output directory:", OUTDIR
print "----> Your log file:", LOGFILE

# Lakoff's hedges
LAKOFF_HEDGES = open(os.path.join(THISDIR,'lakoff-hedges.txt')).read().split('\n')

#///////////////////////////////////////////////////////////////////// 

#### start a log file
saveout = sys.stdout
fsock = open(LOGFILE, 'w')
sys.stdout = fsock


#### keep track of elapsed time
START_TIME = time.time()

sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

def fuzzy_substring(needle, haystack):
    """ @note: Calculates the fuzzy match of needle in haystack,
        using a modified version of the Levenshtein distance algorithm.
        The function is modified from the levenshtein function in the bktree
        module by Adam Hupp.
        Website: http://ginstrom.com/scribbles/2007/12/01/fuzzy-substring-matching-with-levenshtein-distance-in-python/
    """
    m, n = len(needle), len(haystack)

    # base cases
    if m == 1:
        return not needle in haystack
    if not n:
        return m

    row1 = [0] * (n+1)
    for i in range(0,m):
        row2 = [i+1]
        for j in range(0,n):
            cost = ( needle[i] != haystack[j] )

            row2.append( min(row1[j+1]+1, # deletion
                               row2[j]+1, #insertion
                               row1[j]+cost) #substitution
                           )
        row1 = row2
    return min(row1), row1



def hedges_exact(sent, hedges):
    """ @note: exact comparison of hedges
        @return: hedge count and hedges found
    """
    hcnt = 0
    hlist = []
    for h in hedges:
        if h in sent:
            hcnt += 1
            hlist.append(h)
    return hcnt, hlist


def hedges_fuzzy(sent, hedges, threshold, outf):
    """ @note: fuzzy comparison of hedges
        @return: hedge count and hedges found
        @TODO: what should the threshold be?
    """
    hcnt = 0
    hlist = []
    for h in hedges:
        minval, row1 = fuzzy_substring(h, sent)
        if minval <= threshold:
            hcnt += 1
            hlist.append(h)
            #outf.write('fuzzy min '+str(minval)+' | hedge '+h+ ' | '+ sent+'\n')
        #print '[',h,']',minval, row1, sent
    return hcnt, hlist


def hedge_special(h, sent, sent_tagged, outf):
    """ @note: process special hedges

        'pretty': an adverb that modifies adj or adverb that follows it

        Example Stanford CoreNLP token item:
            {'OriginalText': 'that', 'Lemma': 'that', 'Text': 'that',
            'After': ' ', 'CharacterOffsetEnd': 4, 'PartOfSpeech': 'DT',
            'CharacterOffsetBegin': 0, 'Before': ''}
    """
    found_h = False
    desired_h = False

    if h == 'pretty':
        #tokens_obj = common.get_tokens(sent) # use CoreNLP to get POS
        #tokens_arr = tokens_obj[0]

        for i in range(len(sent_tagged)): #tokens_arr)):
            #token_item = tokens_arr[i]
            word, pos = sent_tagged[i]
            word = word.lower()

            if found_h: # found in previous word
                # JJ = adj; JJR = adj comparative; JJS = adj superlative
                # RB = adv; RBR = adv comparative; RBS = adv superlative
                if re.search('^(JJ|RB)', pos): # token_item['PartOfSpeech']):
                    desired_h = True
                #print '**** special hedge(', h, ') followed by', token_item
                outf.write('**** special hedge ('+h+') followed by '+ \
                    str(sent_tagged[i])+'\n') #token_item)+'\n')
                break

            #if token_item['OriginalText'] == h:
            if word == h:
                found_h = True
    else: # not handled
        print '**** special hedge (', h, ') NOT HANDLED in function'

    return desired_h


def hedges_ngram(sent, sent_tokens, sent_tagged, hedges, outf):
    """ @note:  use ngrams and difflib.SequenceMatcher(None, needle, hay) to
                find similar sequence
        @return: hedge count and hedges found
    """
    #sent_tokens = sent.split() #get_tokens(sent)

    hcnt = 0
    hlist = []

    for h in hedges:
        # if hedge is 'pretty', then check for adjective or adverb that follows
        if h == 'pretty':
            if hedge_special(h, sent, sent_tagged, outf):
                # True if it's the desired hedge
                hcnt += 1
                hlist.append(h)
            continue

        # take care of wildcard
        # e.g., on the * side
        if '*' in h:
            hnew = re.sub('\*', '(.*)',h)
            m = re.match('(.*)'+hnew+'(.*)', sent)
            if m and (h not in hlist):
                hcnt+=1
                hlist.append(h)
        else:
            # tokenize h, find len n, look for (n+1) and (n+2) grams
            hlen = len(h.split()) #len(get_tokens(h)) # number of tokens
            hgrams = ngrams(sent_tokens, hlen)
            hgrams_p1 = ngrams(sent_tokens, hlen+1)
            hgrams_p2 = ngrams(sent_tokens, hlen+2)
            #print hgrams

            for hg in hgrams: # same number of tokens - high threshold
                hg_str = ' '.join(hg)
                r = difflib.SequenceMatcher(None, h, hg_str).ratio()
                if r > 0.9:
                    hcnt += 1
                    hlist.append(h)

            # chain two generators
            for hg in chain(hgrams_p1,hgrams_p2):
                hg_str = ' '.join(hg)
                r = difflib.SequenceMatcher(None, h, hg_str).ratio()
                if r > 0.85:
                    if h not in hlist:
                        hcnt += 1
                        hlist.append(h)
    return hcnt, hlist



def get_hedges(lines, poslines, outfile):
    """ @note: get hedges
        @return: output file path
    """
    outfile_path = os.path.join(OUTDIR, outfile)
    outf = open(outfile_path, 'w')

    #print "---->", outfile
    #outf.write("----> "+outfile+"\n")

    total_hcnt = 0      # total num of hedges
    total_hlist = defaultdict(int)  # all hedges found
    total_hsents = 0    # total number of sentences with hedges
    total_hutts = 0     # total number of utterances with hedges
    total_sents = 0     # total number of sentences
    #total_utts = 0      # total number of utterances (same as len(lines))

    threshold = 1   # for fuzzy threshold
    outf.write('FUZZY threshold: '+ str(threshold)+'\n')

    for i in range(len(lines)):
        
        utt = lines[i].strip()
        
        #sents_array = common.split_sents(utt)
        #sents_nltk = sent_tokenizer.tokenize(utt)

        posline = poslines[i]
        (posfile_tokens, posfile_tagged, posfile_sents) = \
            common.process_posfile(posline)

        hutts_found = False     # see if this utterance has hedges

        for j in range(len(posfile_sents)): #sents_array)):

            #sent_obj = sents_array[j]
            #if not sent_obj: continue
            #sent = sent_obj.toString()
            sent = posfile_sents[j]
            if not sent: continue

            #print '****', sent, type(sent)
            total_sents += 1    # num of sentences

            #if j>=(len(sents_nltk)-1):
            #    print 'ERROR. NLTK reached end.', j
            #else:
            #sent_nltk = sents_nltk[j]
            #sent_posfile = posfile_tokens[j]

            # sanity check
            #if re.sub('\s', '', sent) != ''.join(sent_posfile):
            #    print 'ERROR. Sentence mismatch:'
            #    #print '\tnltk:', sent_nltk
            #    print '\tposfile:', sent_posfile
            #    print '\tcurrent:', sent

            sent_tokens = posfile_tokens[j]
            sent_tagged = posfile_tagged[j]

            # check exact and +1, +2 hedges
            hcnt, hlist = hedges_ngram(sent, sent_tokens, sent_tagged, \
                            LAKOFF_HEDGES, outf)

            if hcnt != 0:
                total_hcnt += hcnt                  # num of hedges
                for h in hlist: total_hlist[h]+=1   # list of hedges
                total_hsents += 1                   # num sents with hedges
                hutts_found = True                  # this utterance has hedges
                outf.write('NGRAM '+str(hcnt)+str(hlist)+sent+'\n')
                #print 'NGRAM', hcnt, hlist, sent

            # check fuzzy hedges to see if there's any additional hedges
            # TODO: what should the threshold be?
            #       this doesn't really work... skip for now
            """
            hsents_found = False
            hcnt2, hlist2 = hedges_fuzzy(sent, LAKOFF_HEDGES, threshold, outf)
            if hcnt2 != 0:
                for h in hlist2:
                    if h not in total_hlist.keys():
                        total_hlist[h]+=1
                        total_hcnt += 1
                        hsents_found = True
                        hutts_found = True
                        # take note of additional ones
                        outf.write('FUZZY '+str(hcnt2)+str(hlist2)+sent+'\n')
                if hsents_found:        # update sents with hedges
                    total_hsents += 1
                #outf.write('FUZZY '+str(hcnt2)+str(hlist2)+sent+'\n')
            """

        # utterances with hedges
        if hutts_found: total_hutts += 1

    # sort hedge frequency count
    total_hlist_sorted = \
        sorted(total_hlist.iteritems(), key=lambda (k,v): (v,k), reverse=True)

    outf.write('\n')
    outf.write('Number of sents: '+str(total_sents)+'\n')
    outf.write('Number of utts: '+str(len(lines))+'\n')
    outf.write('Number of sents with hedges: '+str(total_hsents)+'\n')
    outf.write('Number of utts with hedges: '+str(total_hutts)+'\n')
    outf.write('Number of hedges found: '+str(total_hcnt)+'\n')
    outf.write('Hedges found: '+str(total_hlist_sorted)+'\n')
    outf.close()

    #print 'sents', total_sents, 'utts', len(lines), 'h-sents', total_hsents, \
    #    'h-utts', total_hutts, 'hedges', total_hcnt, total_hlist_sorted
    return outfile_path


def run():
    """ @note: run
    """
    input_info = pickle.load(open(INDIR))
    pos_info = pickle.load(open(POSDIR))

    data_info = {}

    for infile in sorted(input_info):

        # test specific file
        #if infile != 'amy.txt': continue

        print "processing: " + infile

        if infile[0]=='.' or infile[-4:]!='.txt':
            print 'Not text file. Skip.'
            continue     # avoids hidden files (on Mac)

        infile_lines = input_info[infile].split('\n')
        #print 'num input lines:', len(infile_lines)

        posfile = re.sub('.txt', '.pos.txt', infile)
        posfile_lines = pos_info[posfile].split('\n')
        #print 'num pos lines:', len(posfile_lines)

        outfile_name = infile+'.out'
        outfile_path = get_hedges(infile_lines, posfile_lines, outfile_name)

        # store to dict for saving to pickle file later
        data_info[outfile_name]=open(outfile_path).read()

    # save to pickle file
    pickle.dump(data_info, open(THISHEADER+'.pickle','w'))


if __name__ == '__main__':
    run()


#### calculate elapsed time
elapsed_time = time.time() - START_TIME
print '\n\nScript elapsed time (seconds)', elapsed_time


#### save log file
sys.stdout = saveout
fsock.close()

