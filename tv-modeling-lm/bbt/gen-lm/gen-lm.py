from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Build language model with SRILM
---------------------------------------------------------------
"""

import sys, os, re, subprocess
import nltk
from collections import defaultdict

RSCDIR = os.path.join('..','..','..','resources')
sys.path.append(RSCDIR)
import word_category_counter_new.word_category_counter as wc
from common import create_directory, get_tokens_original_text

SRILMDIR = '/Volumes/hhd/Dialogue/srilm171/bin/macosx/'

COMMON_DIR = os.path.join('..','..','..','tv-modeling','bbt')

#### original sentences
DEFAULT_SRCDIR = os.path.join(COMMON_DIR, 'gen-conv', 'conv-input-main-pairs')
SERIES_SRCDIR = os.path.join(COMMON_DIR, 'gen-conv', 'conv-input-main-pairs-series')
EPISODE_SRCDIR = os.path.join(COMMON_DIR, 'gen-conv','conv-input-main-pairs-episode')
SCENE_SRCDIR = os.path.join(COMMON_DIR, 'gen-conv','conv-input-main-pairs-scene')

CHARS_DEFAULT_SRCDIR = os.path.join(COMMON_DIR, 'gen-features','chars-input')
CHARS_SERIES_SRCDIR = os.path.join(COMMON_DIR, 'gen-features','chars-input-series')
CHARS_EPISODE_SRCDIR = os.path.join(COMMON_DIR, 'gen-features','chars-input-episode')
CHARS_SCENE_SRCDIR = os.path.join(COMMON_DIR, 'gen-features','chars-input-scenes')

#### converted source files to one sentence per line
#### then run LM on these files

# conversation input
DEFAULT_INDIR = os.path.join('conv', 'conv-input-main-pairs')
SERIES_INDIR = os.path.join('conv','conv-input-main-pairs-series')
EPISODE_INDIR = os.path.join('conv','conv-input-main-pairs-episode')
SCENE_INDIR = os.path.join('conv','conv-input-main-pairs-scene')
# conversation output (after running LM); files have ".bo" extension
DEFAULT_OUTDIR = os.path.join('conv','conv-output-lm-main-pairs')
SERIES_OUTDIR = os.path.join('conv','conv-output-lm-main-pairs-series')
EPISODE_OUTDIR = os.path.join('conv','conv-output-lm-main-pairs-episode')
SCENE_OUTDIR = os.path.join('conv','conv-output-lm-main-pairs-scene')

# chars input
CHARS_DEFAULT_INDIR = os.path.join('chars','chars-input')
CHARS_SERIES_INDIR = os.path.join('chars','chars-input-series')
CHARS_EPISODE_INDIR = os.path.join('chars','chars-input-episode')
CHARS_SCENE_INDIR = os.path.join('chars','chars-input-scene')
# chars output (after running LM)
CHARS_DEFAULT_OUTDIR = os.path.join('chars','chars-output-lm')
CHARS_SERIES_OUTDIR = os.path.join('chars','chars-output-lm-series')
CHARS_EPISODE_OUTDIR = os.path.join('chars','chars-output-lm-episode')
CHARS_SCENE_OUTDIR = os.path.join('chars','chars-output-lm-scene')

#### testing version - each file only contains one sentence

# conversation input
INPUT_MAIN_PAIRS = 'input-main-pairs'
OUTPUT_MAIN_PAIRS = 'output-main-pairs'

DEFAULT_TESTDIR = os.path.join('conv-test','conv-input-main-pairs')
SERIES_TESTDIR = os.path.join('conv-test','conv-input-main-pairs-series')
EPISODE_TESTDIR = os.path.join('conv-test','conv-input-main-pairs-episode')
SCENE_TESTDIR = os.path.join('conv-test','conv-input-main-pairs-scene')
# conversation output
DEFAULT_TESTOUTDIR = os.path.join('conv-test','conv-output-main-pairs')
SERIES_TESTOUTDIR = os.path.join('conv-test','conv-output-main-pairs-series')
EPISODE_TESTOUTDIR = os.path.join('conv-test','conv-output-main-pairs-episode')
SCENE_TESTOUTDIR = os.path.join('conv-test','conv-output-main-pairs-scene')
# conversation output (sorted)
DEFAULT_TESTOUTSORTEDDIR = os.path.join('conv-test','conv-output-sorted-main-pairs')
SERIES_TESTOUTSORTEDDIR = os.path.join('conv-test','conv-output-sorted-main-pairs-series')
EPISODE_TESTOUTSORTEDDIR = os.path.join('conv-test','conv-output-sorted-main-pairs-episode')
SCENE_TESTOUTSORTEDDIR = os.path.join('conv-test','conv-output-sorted-main-pairs-scene')

# chars input
CHARS_DEFAULT_TESTDIR = os.path.join('chars-test','chars-input')
CHARS_SERIES_TESTDIR = os.path.join('chars-test','chars-input-series')
CHARS_EPISODE_TESTDIR = os.path.join('chars-test','chars-input-episode')
CHARS_SCENE_TESTDIR = os.path.join('chars-test','chars-input-scene')
# chars output
CHARS_DEFAULT_TESTOUTDIR = os.path.join('chars-test','chars-output')
CHARS_SERIES_TESTOUTDIR = os.path.join('chars-test','chars-output-series')
CHARS_EPISODE_TESTOUTDIR = os.path.join('chars-test','chars-output-episode')
CHARS_SCENE_TESTOUTDIR = os.path.join('chars-test','chars-output-scene')
# chars output (sorted)
CHARS_DEFAULT_TESTOUTSORTEDDIR = os.path.join('chars-test','chars-output-sorted')
CHARS_SERIES_TESTOUTSORTEDDIR = os.path.join('chars-test','chars-output-sorted-series')
CHARS_EPISODE_TESTOUTSORTEDDIR = os.path.join('chars-test','chars-output-sorted-episode')
CHARS_SCENE_TESTOUTSORTEDDIR = os.path.join('chars-test','chars-output-sorted-scene')

# sentence tokenizer
sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')


####
#### Re-write files so it's one sentence per line
####
def get_files(SRCDIR, INDIR, TESTDIR):

	create_directory(TESTDIR)

	# re-write files so it's one sentence per line
	listing = os.listdir(SRCDIR)

	for f in listing:
		if f[0] == '.': continue # hidden file

		temp = re.sub('.txt','.onesentperline.txt', f)
		temp2 = re.sub('\'','', temp)
		temp3 = re.sub('\&','n', temp2)
		foutname = re.sub(' ','-',temp3)
		outfile = open(os.path.join(INDIR,foutname), 'w')

		lines = open(os.path.join(SRCDIR,f)).readlines()

		for line in lines:
			sents = sent_tokenizer.tokenize(line.strip())
			#print arr
			#if len(arr)>1:
			outfile.write('\n'.join(sents)+'\n')
			#else:
			#	outfile.write(line.strip()+'\n')
		outfile.close()

		# write ONE line to files in test directory
		testfile = open(os.path.join(TESTDIR,foutname), 'w')
		testfile.write(sents[0]+'\n')
		testfile.close()


####
#### get LIWC's categories' short names 
####
long2short07 = dict()
long2short01 = dict()

for lname, liwc07num, liwc07short, liwc01num, liwc01short in \
	wc._dictionary._liwc_categories:

	#print 'lname:', lname, 'liwc07short:', liwc07short
	long2short07[lname] = liwc07short
	long2short01[lname] = liwc01short


def get_liwc_tag(word):
	tags = []

	# score word
	c = wc._dictionary.score_word(word)

	for entry in c:
		#print 'entry:', entry, c[entry], 
		if entry in long2short07:
			tags.append(long2short07[entry])
		elif entry in long2short01:
			tags.append(long2short01[entry])
		elif entry=='Dictionary Words':
			tags.append('dict')
		elif entry=='Word Count':
			tags.append('wc')

	return tags
	

####
#### LIWC-tagged words
####
def liwc_tagged(INDIR, OUTDIR):

	create_directory(OUTDIR)
	files = os.listdir(INDIR)

	for f in files:
		if f[0] == '.': continue # hidden file

		# create output file
		temp = re.sub('onesentperline.txt','liwctagged.onesentperline.txt', f)
		temp2 = re.sub('\'','', temp)
		temp3 = re.sub('\&','n', temp2)
		foutname = re.sub(' ','-',temp3)
		outf = open(os.path.join(OUTDIR,foutname), 'w')

		# for each line, tokenize and look up 
		lines = open(os.path.join(INDIR,f)).readlines()
		for line in lines:
			line = line.strip()
			if not line: continue

			tokens = get_tokens_original_text(line)
			for token in tokens:
				tags = get_liwc_tag(token)
				#print '\t', token, tags
				if tags:
					outf.write(token+'_'+'_'.join(tags)+' ')
				else:
					outf.write(token+' ')
			outf.write('\n')




####
#### Ngram count
####
def get_lm(INDIR, OUTDIR):
	create_directory(OUTDIR)

	files = os.listdir(INDIR)

	for f in files:
		# ngram-count -text sheldon-leonard.txt -lm sheldon-leonard.bo
		#f2 = re.sub(' ','-',f)
		outlm = re.sub('.txt', '.bo', f)
		#outlm = re.sub(' ','-',temp)
		# default is up to 3 ngrams
		cmd = SRILMDIR+'ngram-count -text '+os.path.join(INDIR,f)+ \
				' -lm '+os.path.join(OUTDIR,outlm)
		p=subprocess.Popen(cmd, shell=True, \
			stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		p.wait()
		#if 'all' in f:
		#	print cmd


def parse_result(results, outf):
	# results look like:
	# <lm file name>
	# file conv/test-input/amy-leonard-others.onesentperline.txt: 1 sentences, 7 words, 7 OOVs
	# 0 zeroprobs, logprob= -0.748188 ppl= 5.6 ppl1= undefined
	ppldict = defaultdict(list)
	for key in results.keys():
		arr = results[key].split('\n')
		line2 = arr[2].strip()
		m = re.match(r'.+ logprob=(.+) ppl=(.+) ppl1', line2)
		logprob = float(m.group(1))	# log prob
		ppl = float(m.group(2))	# perplexity
		ppldict[ppl].append(key)

	# sort	
	f = open(outf, 'w')
	for k in sorted(ppldict.keys()):
		v = ppldict[k]
		for vi in v:
			#print results[vi]
			f.write(results[vi])
	f.close()

	# return the first two entries (least amount of perplexity)
	arr = []
	for i in [0,1]:
		k = sorted(ppldict.keys())[i]
		v = ppldict[k]
		for vi in v:
			r = results[vi] # results
			a = r.split('\n')
			arr.append(a[0])

	return arr	# first line is the filename



def test_lm(OUTDIR, TESTDIR, TESTOUTDIR, TESTOUTSORTEDDIR):

	create_directory(TESTOUTSORTEDDIR)

	err = 0
	listing = os.listdir(TESTDIR)

	for fmain in listing:

		if fmain[0]=='.':
			print 'hidden file (skip):', fmain
			continue

		temp = re.sub('.onesentperline.txt', '', fmain)
		fmainhead = re.sub(' ','-',temp)
		outf = os.path.join(TESTOUTDIR, fmainhead+'.txt.out')
		# clear the file first
		#cmd = 'echo \'\' > '+outf
		#p=subprocess.Popen(cmd, shell=True, \
		#	stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		#p.wait()

		# go through each lm
		results = {}

		for f in listing:
			# ngram -lm sheldon-leonard.bo -ppl test.txt

			if f[0]=='.':
				print 'hidden file (skip):', f
				continue

			temp = re.sub('.onesentperline.txt', '', f)
			fhead = re.sub(' ','-',temp)
			cmd = [SRILMDIR+'ngram',
					'-lm',
					os.path.join(OUTDIR,fhead+'.onesentperline.bo'),
					'-ppl',
					os.path.join(TESTDIR,fmain)
					#'>>', outf
					]
			#print cmd
			p=subprocess.check_output(cmd)
			#print p
			results[fhead]=fhead+'\n'+p

		# sort the result
		create_directory(TESTOUTSORTEDDIR)
		outf2 = os.path.join(TESTOUTSORTEDDIR,fmainhead+'.sorted.txt.out')
		arr = parse_result(results, outf2)

		# returned result should be the same as fmainhead
		if fmainhead not in arr:
			#print 'ERROR.', fmainhead, 'did not match with least perplexity: ', arr
			err+=1

	print 'Mismatch', err, 'out of', len(listing)


def run(dtype, subtype):

	if dtype=='conv':
		if subtype=='default':
			get_files(DEFAULT_SRCDIR, DEFAULT_INDIR, DEFAULT_TESTDIR)
			get_lm(DEFAULT_INDIR, DEFAULT_OUTDIR)
			#test_lm(DEFAULT_OUTDIR, DEFAULT_TESTDIR, DEFAULT_TESTOUTDIR, DEFAULT_TESTOUTSORTEDDIR)
		elif subtype=='series':
			get_files(SERIES_SRCDIR, SERIES_INDIR, SERIES_TESTDIR)
			get_lm(SERIES_INDIR, SERIES_OUTDIR)
			#test_lm(SERIES_OUTDIR, SERIES_TESTDIR, SERIES_TESTOUTDIR, SERIES_TESTOUTSORTEDDIR)
		elif subtype=='episode':
			get_files(EPISODE_SRCDIR, EPISODE_INDIR, EPISODE_TESTDIR)
			get_lm(EPISODE_INDIR, EPISODE_OUTDIR)
			#test_lm(EPISODE_OUTDIR, EPISODE_TESTDIR, EPISODE_TESTOUTDIR, EPISODE_TESTOUTSORTEDDIR)
		elif subtype=='scene':
			get_files(SCENE_SRCDIR, SCENE_INDIR, SCENE_TESTDIR)
			get_lm(SCENE_INDIR, SCENE_OUTDIR)
			#test_lm(SCENE_OUTDIR, SCENE_TESTDIR, SCENE_TESTOUTDIR, SCENE_TESTOUTSORTEDDIR)
	
	elif dtype=='chars':
		if subtype=='default':
			get_files(CHARS_DEFAULT_SRCDIR, CHARS_DEFAULT_INDIR, CHARS_DEFAULT_TESTDIR)
			get_lm(CHARS_DEFAULT_INDIR, CHARS_DEFAULT_OUTDIR)
			#test_lm(CHARS_DEFAULT_OUTDIR, CHARS_DEFAULT_TESTDIR, CHARS_DEFAULT_TESTOUTDIR, CHARS_DEFAULT_TESTOUTSORTEDDIR)
		elif subtype=='series':
			get_files(CHARS_SERIES_SRCDIR, CHARS_SERIES_INDIR, CHARS_SERIES_TESTDIR)
			get_lm(CHARS_SERIES_INDIR, CHARS_SERIES_OUTDIR)
			#test_lm(CHARS_SERIES_OUTDIR, CHARS_SERIES_TESTDIR, CHARS_SERIES_TESTOUTDIR, CHARS_SERIES_TESTOUTSORTEDDIR)
		elif subtype=='episode':
			get_files(CHARS_EPISODE_SRCDIR, CHARS_EPISODE_INDIR, CHARS_EPISODE_TESTDIR)
			get_lm(CHARS_EPISODE_INDIR, CHARS_EPISODE_OUTDIR)
			#test_lm(CHARS_EPISODE_OUTDIR, CHARS_EPISODE_TESTDIR, CHARS_EPISODE_TESTOUTDIR, CHARS_EPISODE_TESTOUTSORTEDDIR)
		elif subtype=='scene':
			get_files(CHARS_SCENE_SRCDIR, CHARS_SCENE_INDIR, CHARS_SCENE_TESTDIR)
			get_lm(CHARS_SCENE_INDIR, CHARS_SCENE_OUTDIR)
			#test_lm(CHARS_SCENE_OUTDIR, CHARS_SCENE_TESTDIR, CHARS_SCENE_TESTOUTDIR, CHARS_SCENE_TESTOUTSORTEDDIR)


def run_liwc_tagged(istest, dtype, subtype):

	txt = 'test-' if istest else ''
	txt2 = '-'+subtype
	main_pairs = ''
	input_dir = ''

	if istest: # testing doesn't care about dtype
		if subtype=='default':
			input_dir = DEFAULT_TESTDIR
			txt2 = ''
		elif subtype=='series':
			input_dir = SERIES_TESTDIR
		elif subtype=='episode':
			input_dir = EPISODE_TESTDIR
		elif subtype=='scene':
			input_dir = SCENE_TESTDIR
	else:

		if dtype=='conv':
			main_pairs = '-main_pairs'
			# regular directories
			if subtype=='default':
				input_dir = DEFAULT_INDIR
			elif subtype=='series':
				input_dir = SERIES_INDIR
			elif subtype=='episode':
				input_dir = EPISODE_INDIR
			elif subtype=='scene':
				input_dir = SCENE_INDIR

		else: # chars
			if subtype=='default':
				input_dir = CHARS_DEFAULT_INDIR
			elif subtype=='series':
				input_dir = CHARS_SERIES_INDIR
			elif subtype=='episode':
				input_dir = CHARS_EPISODE_INDIR
			elif subtype=='scene':
				input_dir = CHARS_SCENE_INDIR			

	if not input_dir:
		print 'ERROR. Unknown pref:', itest, dtype, subtype
		return

	DEFAULT_INDIR_LIWC_TAGGED = os.path.join(dtype+'-'+txt+'liwc-tagged', \
		dtype+'-input-liwc-tagged'+main_pairs+txt2)
	DEFAULT_LM_LIWC_TAGGED = os.path.join(dtype+'-'+txt+'liwc-tagged', \
		dtype+'-lm-liwc-tagged'+main_pairs+txt2)
	DEFAULT_OUTDIR_LIWC_TAGGED = os.path.join(dtype+'-'+txt+'liwc-tagged', \
		dtype+'-output-liwc-tagged'+main_pairs+txt2)
	DEFAULT_OUTDIR_LIWC_TAGGED_SORTED = os.path.join(dtype+'-'+txt+'liwc-tagged', \
		dtype+'-output-liwc-tagged'+main_pairs+'-sorted'+txt2)

	# run
	liwc_tagged(input_dir, DEFAULT_INDIR_LIWC_TAGGED)
	get_lm(DEFAULT_INDIR_LIWC_TAGGED, DEFAULT_LM_LIWC_TAGGED)
	#test_lm(DEFAULT_LM_LIWC_TAGGED, DEFAULT_INDIR_LIWC_TAGGED, \
	#		DEFAULT_OUTDIR_LIWC_TAGGED, DEFAULT_OUTDIR_LIWC_TAGGED_SORTED)


if __name__ == "__main__":

	#### regular, non-LIWC-tagged files

	#run('chars', 'default')
	#run('chars', 'series')
	#run('chars', 'episode')
	#run('chars', 'scene')

	#run('conv', 'default')
	#run('conv', 'series')
	#run('conv', 'episode')
	#run('conv', 'scene')

	#### convert conv input text files into LIWC tagged ones, then generate LM
	# Make sure the original input text files had already being created by
	# run('conv', 'X')'s get_files() above.
	
	#run_liwc_tagged(istest=False, dtype='conv', subtype='default')
	#run_liwc_tagged(istest=False, dtype='conv', subtype='series')
	#run_liwc_tagged(istest=False, dtype='conv', subtype='episode')
	#run_liwc_tagged(istest=False, dtype='conv', subtype='scene')

	#run_liwc_tagged(istest=False, dtype='chars', subtype='default')
	run_liwc_tagged(istest=False, dtype='chars', subtype='series')



