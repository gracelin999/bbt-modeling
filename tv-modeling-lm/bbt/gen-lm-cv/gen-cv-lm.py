from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Build language model with SRILM
---------------------------------------------------------------
"""

import sys, os, re, subprocess
import nltk
from collections import defaultdict

#///////////////////////////////////////////////////////////////////// 

RSCDIR = os.path.join('..','..','..','resources')
sys.path.append(RSCDIR)
import word_category_counter_new.word_category_counter as wc
from common import create_directory, get_tokens_original_text

SRILMDIR = '/Volumes/hhd/Dialogue/srilm171/bin/macosx/'

#### original sentences
CONV_SRCDIR = os.path.join('..','..','..','tv-modeling-conv-multi','bbt', \
				'gen-conv-multi-cv', 'conv-input-all-pair-main-cv')

CHARS_SRCDIR = os.path.join('..','..','..','tv-modeling-features','bbt', \
				'gen-features-cv', 'chars-input-cv')

#### converted source files to one sentence per line
#### then run LM on these files

# conversation input
CONV_INDIR = os.path.join('conv', 'conv-input-main-pairs')
# conversation output (after running LM); files have ".bo" extension
CONV_OUTDIR = os.path.join('conv','conv-output-lm-main-pairs')

# chars input
CHARS_INDIR = os.path.join('chars','chars-input')
# chars output (after running LM)
CHARS_OUTDIR = os.path.join('chars','chars-output-lm')

FOLDS = 5

#///////////////////////////////////////////////////////////////////// 


sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')


####
#### Re-write files so it's one sentence per line
####
def get_files(SRCDIR, INDIR):
	# re-save files so it's one sentence per line
	listing = os.listdir(SRCDIR)

	for f in listing:
		if f[0] == '.': continue # hidden file

		temp = re.sub('.txt','.onesentperline.txt', f)
		temp2 = re.sub('\'','', temp)
		temp3 = re.sub('\&','n', temp2)
		foutname = re.sub(' ','-',temp3)
		outfile = open(os.path.join(INDIR,foutname), 'w')

		lines = open(os.path.join(SRCDIR,f)).readlines()

		for line in lines:
			line = line.strip()
			if not line: continue

			sents = sent_tokenizer.tokenize(line)
			#print arr
			#if len(arr)>1:
			outfile.write('\n'.join(sents)+'\n')
			#else:
			#	outfile.write(line.strip()+'\n')
		outfile.close()



####
#### get LIWC's categories' short names 
####
long2short07 = dict()
long2short01 = dict()

for lname, liwc07num, liwc07short, liwc01num, liwc01short in \
	wc._dictionary._liwc_categories:

	#print 'lname:', lname, 'liwc07short:', liwc07short
	long2short07[lname] = liwc07short
	long2short01[lname] = liwc01short


def get_liwc_tag(word):
	tags = []

	# score word
	c = wc._dictionary.score_word(word)

	for entry in c:
		#print 'entry:', entry, c[entry], 
		if entry in long2short07:
			tags.append(long2short07[entry])
		elif entry in long2short01:
			tags.append(long2short01[entry])
		elif entry=='Dictionary Words':
			tags.append('dict')
		elif entry=='Word Count':
			tags.append('wc')

	return tags
	

def liwc_tagged(INDIR, OUTDIR):
	print 'liwc-tagged indir:', INDIR
	print 'liwc-tagged outdir:', OUTDIR

	create_directory(OUTDIR)
	files = os.listdir(INDIR)

	for f in files:
		if f[0] == '.': continue # hidden file

		# create output file
		temp = re.sub('onesentperline.txt','liwctagged.onesentperline.txt', f)
		temp2 = re.sub('\'','', temp)
		temp3 = re.sub('\&','n', temp2)
		foutname = re.sub(' ','-',temp3)
		outf = open(os.path.join(OUTDIR,foutname), 'w')

		# for each line, tokenize and look up 
		lines = open(os.path.join(INDIR,f)).readlines()
		for line in lines:
			line = line.strip()
			if not line: continue

			tokens = get_tokens_original_text(line)
			for token in tokens:
				tags = get_liwc_tag(token)
				#print '\t', token, tags
				if tags:
					outf.write(token+'_'+'_'.join(tags)+' ')
				else:
					outf.write(token+' ')
			outf.write('\n')



def get_lm(INDIR, OUTDIR):
	create_directory(OUTDIR)

	files = os.listdir(INDIR)

	for f in files:
		# ngram-count -text sheldon-leonard.txt -lm sheldon-leonard.bo
		#f2 = re.sub(' ','-',f)
		outlm = re.sub('.txt', '.bo', f)
		#outlm = re.sub(' ','-',temp)
		# default is up to 3 ngrams
		cmd = SRILMDIR+'ngram-count -text '+os.path.join(INDIR,f)+ \
				' -lm '+os.path.join(OUTDIR,outlm)
		p=subprocess.Popen(cmd, shell=True, \
			stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		p.wait()
		#if 'all' in f:
		#	print cmd




def run_liwc_tagged(indir, fold, dtype, subtype='default'):

	txt = ''
	txt2 = '-'+subtype
	main_pairs = ''

	if dtype=='conv':
		main_pairs = '-main_pairs'

	DEFAULT_INDIR_LIWC_TAGGED = os.path.join(dtype+'-'+txt+'liwc-tagged', \
		str(fold), 'train', dtype+'-input-liwc-tagged'+main_pairs+txt2)
	DEFAULT_LM_LIWC_TAGGED = os.path.join(dtype+'-'+txt+'liwc-tagged', \
		str(fold), 'train', dtype+'-lm-liwc-tagged'+main_pairs+txt2)
	DEFAULT_OUTDIR_LIWC_TAGGED = os.path.join(dtype+'-'+txt+'liwc-tagged', \
		str(fold), 'train', dtype+'-output-liwc-tagged'+main_pairs+txt2)
	DEFAULT_OUTDIR_LIWC_TAGGED_SORTED = os.path.join(dtype+'-'+txt+'liwc-tagged', \
		str(fold), 'train', dtype+'-output-liwc-tagged'+main_pairs+'-sorted'+txt2)

	# run
	liwc_tagged(indir, DEFAULT_INDIR_LIWC_TAGGED)
	get_lm(DEFAULT_INDIR_LIWC_TAGGED, DEFAULT_LM_LIWC_TAGGED)


if __name__ == "__main__":


	for i in range(FOLDS):
		#### chars, non-liwc-tagged
		#srcdir = os.path.join(CHARS_SRCDIR, str(i), 'train')
		chars_indir  = os.path.join(CHARS_INDIR, str(i), 'train')
		"""
		create_directory(indir)
		outdir = os.path.join(CHARS_OUTDIR, str(i), 'train')
		create_directory(outdir)
		# get files (one sent per line) and run LM
		get_files(srcdir, chars_indir)
		get_lm(chars_indir, outdir)
		"""

		#### conv, non-liwc-tagged
		#srcdir = os.path.join(CONV_SRCDIR, str(i), 'train')
		conv_indir  = os.path.join(CONV_INDIR, str(i), 'train')
		"""
		create_directory(indir)
		outdir = os.path.join(CONV_OUTDIR, str(i), 'train')
		create_directory(outdir)
		# get files (one sent per line) and run LM
		get_files(srcdir, conv_indir)
		get_lm(conv_indir, outdir)
		"""

		#### liwc-tagged, both
		run_liwc_tagged(chars_indir, i, dtype='chars', subtype='default')
		run_liwc_tagged(conv_indir, i, dtype='conv', subtype='default')




