
import os, sys
from shutil import copyfile

RESOURCE_DIR = os.path.join('..', '..', '..', 'resources')
sys.path.append(os.path.join(RESOURCE_DIR))
from common import create_directory

"""

Here we basically want to combine files from gen-lm/ and gen-lm-cv/ into
current directory. No re-running is needed.

1. gen-lm-cv-main-and-others/ should have the same directory structure as
	gen-lm-cv/

2. Copy from gen-lm/chars/* and gen-lm/chars-liwc-tagged/* to
	each fold/train directory.

3. Copy from gen-lm-cv/chars/* and gen-lm-cv/chars-liwc-tagged/* to 
	current directory, REPLACING the main characters from above.

"""

FOLDS = 5

# create directory structure as gen-lm-cv

gen_lm_chars_input = os.path.join('..','gen-lm','chars','chars-input')
gen_lm_chars_output = os.path.join('..','gen-lm','chars','chars-output-lm')

gen_lm_cv_chars_input = os.path.join('..','gen-lm-cv','chars','chars-input')
gen_lm_cv_chars_output = os.path.join('..','gen-lm-cv','chars','chars-output-lm')

chars_input = os.path.join('chars','chars-input')
chars_output = os.path.join('chars','chars-output-lm')

for fold in range(FOLDS):

	indir = os.path.join(chars_input,str(fold),'train')
	outdir = os.path.join(chars_output,str(fold),'train')
	create_directory(outdir)
	create_directory(indir)

	# copy from chars-lm
	for f in os.listdir(gen_lm_chars_input):
		src = os.path.join(gen_lm_chars_input,f)
		dst = os.path.join(indir,f)
		copyfile(src,dst)


	for f in os.listdir(gen_lm_chars_output):
		src = os.path.join(gen_lm_chars_output,f)
		dst = os.path.join(outdir,f)
		copyfile(src,dst)

	# copy from chars-lm-cv
	cv_indir = os.path.join(gen_lm_cv_chars_input,str(fold),'train')
	for f in os.listdir(cv_indir):
		src = os.path.join(cv_indir,f)
		dst = os.path.join(indir,f)
		copyfile(src,dst)

	cv_outdir = os.path.join(gen_lm_cv_chars_output,str(fold),'train')
	for f in os.listdir(cv_outdir):
		src = os.path.join(cv_outdir,f)
		dst = os.path.join(outdir,f)
		copyfile(src,dst)


# liwc-tagged
gen_lm_chars_liwc_input = os.path.join('..','gen-lm','chars-liwc-tagged','chars-input-liwc-tagged-default')
gen_lm_chars_liwc_output = os.path.join('..','gen-lm','chars-liwc-tagged','chars-lm-liwc-tagged-default')

gen_lm_cv_dir = os.path.join('..','gen-lm-cv','chars-liwc-tagged')

chars_liwc_dir = os.path.join('chars-liwc-tagged')

for fold in range(FOLDS):

	indir = os.path.join(chars_liwc_dir,str(fold),'chars-input-liwc-tagged-default')
	outdir = os.path.join(chars_liwc_dir,str(fold),'chars-lm-liwc-tagged-default')
	create_directory(outdir)
	create_directory(indir)

	# copy from chars-lm
	for f in os.listdir(gen_lm_chars_liwc_input):
		src = os.path.join(gen_lm_chars_liwc_input,f)
		dst = os.path.join(indir,f)
		copyfile(src,dst)

	for f in os.listdir(gen_lm_chars_liwc_output):
		src = os.path.join(gen_lm_chars_liwc_output,f)
		dst = os.path.join(outdir,f)
		copyfile(src,dst)

	# copy from chars-lm-cv
	cv_indir = os.path.join(gen_lm_cv_dir,str(fold),'train','chars-input-liwc-tagged-default')
	for f in os.listdir(cv_indir):
		src = os.path.join(cv_indir,f)
		dst = os.path.join(indir,f)
		copyfile(src,dst)

	cv_outdir = os.path.join(gen_lm_cv_dir,str(fold),'train','chars-lm-liwc-tagged-default')
	for f in os.listdir(cv_outdir):
		src = os.path.join(cv_outdir,f)
		dst = os.path.join(outdir,f)
		copyfile(src,dst)




