
Here we basically want to combine files from gen-lm/ and gen-lm-cv/ into
current directory. No re-running is needed.

1. gen-lm-cv-main-and-others/ should have the same directory structure as
	gen-lm-cv/

2. Copy from gen-lm/chars/* and gen-lm/chars-liwc-tagged/* to
	each fold/train directory.

3. Copy from gen-lm-cv/chars/* and gen-lm-cv/chars-liwc-tagged/* to 
	current directory, REPLACING the main characters from above.