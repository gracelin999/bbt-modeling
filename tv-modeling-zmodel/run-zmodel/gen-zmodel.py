from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Generate z-models.

This script assumes the tables are in input-tables/ already.
Modify the main function to run specific table.


To run:
>python gen-zmodel.py > gen-zmodel.log
---------------------------------------------------------------
"""

import sys, time, os, re
from collections import defaultdict

import numpy as np


#RESOURCE_DIR = '/Users/grace/dialogue/repos'
#sys.path.append(os.path.join(RESOURCE_DIR,'resources'))
#import common

TABLE_CHAR_INDIR = 'input-tables'
TABLE_CHAR_OUTDIR = 'output-tables'

TABLE_MULTI_INDIR = 'input-tables-multi'
TABLE_MULTI_OUTDIR = 'output-tables-multi'

####
#### create custom table with specific characters
#### save it back to input directory
#### This is for 'chars' only, not for 'conv'
####
def get_specific_chars(infile, sep, outfile, wanted_chars, TABLE_INDIR):
    
    if '-conv-' in infile:
        print 'NOTE. get_specific_chars is only for chars only.'
        return
    
    # open file and read lines
    f = open(os.path.join(TABLE_INDIR,infile))
    lines = f.readlines()

    # open output file for writing
    OUTF = open(os.path.join(TABLE_INDIR, outfile),'w')
    # write header
    OUTF.write(lines[0])

    # go through each line and pick desired characters
    for line in lines[1:]:
        line_arr = line.split(sep)

        # for series/episodes/scenes, it has additional info
        speaker = line_arr[0].strip() 

        if 'series-trends' in infile:
            m = re.search('series-(.+)', speaker)
            speaker = m.group(1)
        elif 'series' in infile:
            m = re.search('series-(.+)', speaker)
            speaker = m.group(1)
        elif 'episode' in infile:
            m = re.search('-episode-\d+-(.+)', speaker)
            speaker = m.group(1)
        elif 'scene' in infile:
            m = re.search('(.+)-series', speaker)
            speaker = m.group(1)

        # see if speaker is what we wanted
        if speaker in wanted_chars:  
               OUTF.write(line)

    OUTF.close()


####
#### get specific speaker and/or addressee from 'conv' files
#### can be empty
####
def get_specific_speaker_addressee(infile, sep, outfile, want_speaker, 
        want_addressee, TABLE_INDIR):

    if '-chars-' in infile:
        print 'NOTE. get_specific_speaker_addresse is only for conv only.'
        return
    # make sure either want_speaker or want_addressee has something
    if want_speaker=='' and want_addressee=='':
        print 'NOTE. Both desired speaker and addressee are empty.'
        return

    # open file and read lines
    f = open(os.path.join(TABLE_INDIR,infile))
    lines = f.readlines()
    # open output file for writing
    OUTF = open(os.path.join(TABLE_INDIR, outfile),'w')
    # write header
    OUTF.write(lines[0])
    # go through each line and pick desired characters
    for line in lines[1:]:
        line_arr = line.split(sep)
        speaker = line_arr[0]
        # addressee; e.g., amy-series-5-episode-08
        # let it match series/episode if necessary
        addressee = line_arr[1]
        # get match
        # if both entries exist
        if want_speaker and want_addressee:
            if (speaker == want_speaker) and (want_addressee in addressee):
                OUTF.write(line)
        elif (speaker == want_speaker) or (want_addressee in addressee):
            OUTF.write(line)
    OUTF.close()



def get_specific_speaker_addressee_multi_pair(infile, sep, outfile, 
        want_speaker, want_addressee, TABLE_INDIR):

    if '-chars-' in infile:
        print 'NOTE. get_specific_speaker_addresse is for conv only.'
        return

    # open file and read lines
    f = open(os.path.join(TABLE_INDIR,infile))
    lines = f.readlines()
    # open output file for writing
    OUTF = open(os.path.join(TABLE_INDIR, outfile),'w')
    # write header
    OUTF.write(lines[0])
    # go through each line and pick desired characters
    for line in lines[1:]:
        line_arr = line.split(sep)
        speaker = line_arr[0].strip()
        addressee = ''
        desc = ''
        
        if 'series'in line_arr[1]:
            arr = line_arr[1].strip().split('-series')
            addressee = arr[0].strip()
            # trends: get the whole thing
            if 'trends' in infile:
                desc = 'series'+arr[1].strip()
        else:
            temp = line_arr[1].strip()
            if temp[-1]=='-':
                addressee = temp[:-1]

        # get match
        # if both entries exist
        if 'trends' in infile:
            #print want_speaker, desc, want_addressee
            if want_addressee[0] == desc:
                if (want_speaker[0]=='any') or (speaker in want_speaker):
                    OUTF.write(line)
        elif want_speaker and want_addressee:
            if (speaker in want_speaker) and (addressee in want_addressee):
                OUTF.write(line)
        elif (speaker in want_speaker) or (addressee in want_addressee):
            OUTF.write(line)
    OUTF.close()



####
#### get average and z-model
####
def get_model(filename, sep, TABLE_INDIR, TABLE_OUTDIR):
    if '-conv-' in filename:
        type='conv'
    else:
        type='char'

    lines = open(os.path.join(TABLE_INDIR,filename)).readlines()
    #print lines[0]
    
    # get header: speaker followed by features
    header_arr = lines[0].strip().split(sep)
    #print len(header_arr) #,header_arr

    if type=='char':
        num_feats = len(header_arr)-1
    else: # conv has an additional column 'addressee'
        num_feats = len(header_arr)-2
    print 'num feats:', num_feats

    # keep track of data by columns
    columns = defaultdict(list)

    # fill in data
    for line in lines[1:]:
        line_arr = line.split(sep)
        for i in range(len(line_arr)):
            if i==0: # speaker
                d = line_arr[i]
            elif i==1 and type=='conv': # addressee
                d = line_arr[i]
            else: # number
                d = float(line_arr[i])
            columns[header_arr[i]].append(d)

    # find mean and stdev, then calculate z-score
    # store in rows
    columns_mean = defaultdict(float)
    columns_stdev = defaultdict(float)
    rows = defaultdict(list)

    # keep the original column header order
    for key in header_arr: #columns.keys():
        if key=='speaker' or key=='addressee': continue

        #print key, columns[key]
        if sum(columns[key])==0:
            mean = 0
            stdev = 0
        else:
            #print filename, key, len(columns[key])
            #mean, stdev = common.meanstdv(columns[key])
            mean = np.mean(columns[key])
            stdev = np.std(columns[key])

        #print key, mean, stdev
        columns_mean[key] = mean
        columns_stdev[key] = stdev

        # store the mean and stdev in separate rows
        rows['_mean'].append(mean)
        rows['_stdev'].append(stdev)

        # go through each value in column and find z-value
        for i in range(len(columns[key])):
            val = columns[key][i]
            if stdev==0:
                zscore = 0
            else:
                zscore = (val-mean)/stdev
            # make the zscore as string
            if type=='char':
                rows[columns['speaker'][i]].append(zscore)
            else:
                rows[columns['speaker'][i]+'=='+ \
                    columns['addressee'][i]].append(zscore)

    #print columns_mean['category-also-ratio']
    #print columns_stdev['category-also-ratio']
    #print columns_mean['category-concede-ratio']
    #print columns_stdev['category-concede-ratio']
    #print rows['houston'][0:4]
    #print rows['all'][0:4]
    #print rows['kripke'][0:4]
    #print rows['cole'][0:4]

    # save results to output file
    outfile= re.sub('.txt','-zvalues.txt',filename)
    OUTF = open(os.path.join(TABLE_OUTDIR, outfile),'w')
    newheader = re.sub('speaker\|addressee','speaker==addressee',lines[0])
    OUTF.write(newheader) # write header

    # write to zmodel file (look for |zscore| >= 1)
    zoutfile = re.sub('.txt','-zmodel1.txt',filename)
    ZOUTF = open(os.path.join(TABLE_OUTDIR, zoutfile),'w')
    if type=='char':
        ZOUTF.write('speaker|num feats|feats\n')
    else:
        ZOUTF.write('speaker==addressee|num feats|feats\n')
    
    # write to zmodel file (look for |zscore| >= 2)
    zoutfile2 = re.sub('.txt','-zmodel2.txt',filename)
    ZOUTF2 = open(os.path.join(TABLE_OUTDIR, zoutfile2),'w')
    if type=='char':
        ZOUTF2.write('speaker|num feats|feats\n')
    else:
        ZOUTF2.write('speaker==addressee|num feats|feats\n')
    
    # write mean and stdev
    # rows['_mean'], rows['_stdev']
    for item in ['_mean', '_stdev']:
        tostr = [str(x) for x in rows[item]]
        OUTF.write(item+'|'+'|'.join(tostr)+'\n')

    # go through each speaker in original order
    for i in range(len(columns['speaker'])):
        speaker = columns['speaker'][i]
        skey = speaker
        if type=='conv':
            addressee = columns['addressee'][i]
            skey += '=='+addressee
        tostr = [str(x) for x in rows[skey]]
        OUTF.write(skey+'|'+'|'.join(tostr)+'\n')
        #print 'num feats', len(rows[speaker])
        #print 'num header items', len(header_arr)
        # z-model
        ZOUTF.write(skey+'|')
        ZOUTF2.write(skey+'|')

        # feats
        numfeats = 0
        numfeats2 = 0

        # go through each feature
        z1 = defaultdict(list)
        z2 = defaultdict(list)
        for j in range(len(rows[skey])):
            val = rows[skey][j]
            if val>=1 or val<=-1:
                if type=='char':
                    idx = j+1  # first item is speaker
                else:
                    idx = j+2  # second item is addressee
                feat = header_arr[idx]
                z1[val].append(feat)
                if val>=2 or val<=-2:
                    z2[val].append(feat)
        """
        #print skey, len(rows[skey]), len(header_arr)
        for j in range(len(rows[skey])):
            val = rows[skey][j]
            if val>=1 or val<=-1:
                feat = header_arr[j+1] # plus 1 since first item is speaker
                numfeats += 1
                zstr += feat+'('+'%.2f'%val+'),'
                if val>=2 or val<=-2:
                    numfeats2 += 1
                    zstr2 += feat+'('+'%.2f'%val+'),'
        """

        zstr = ''
        zstr2 = ''

        for z1v in sorted(z1.keys(),reverse=True):
            for f in z1[z1v]:
                zstr += f+'('+'%.2f'%z1v+'),'
                numfeats+=1
                #if 'wordsperutt' in f:
                #    print f, z1v

        for z2v in sorted(z2.keys(), reverse=True):
            for f in z2[z2v]:
                zstr2 += f+'('+'%.2f'%z2v+'),'
                numfeats2+=1

        ZOUTF.write(str(numfeats)+'|'+zstr+'\n')
        ZOUTF2.write(str(numfeats2)+'|'+zstr2+'\n')

    OUTF.close()
    ZOUTF.close()
    ZOUTF2.close()


####
#### characters
####
def run_chars():

    #### characters, by file; make sure they are copied over from gen-features
    
    filelist = ['gen-chars-default-table.txt'
                #'gen-chars-series-table.txt',
                #'gen-chars-episode-table.txt',
                #'gen-chars-scenes-table.txt',
                #'gen-chars-series-trends-table.txt'
                ]

    for f in filelist:
        print '----> processing file:', f
        get_model(f, '|', TABLE_CHAR_INDIR, TABLE_CHAR_OUTDIR)
    
    

def run_chars_custom():
    #### create custom table; store back in input-tables/
    # this is for 'chars' only, not for conversation
    # files have '-custom' attached
    
    # main characters only
    wanted_chars = ['sheldon', 'leonard', 'penny', 'howard', 'raj',
                    'amy', 'bernadette']
    ext = '-mainchars'

    #### for series, trends:

    #### all seasons for each character
    """
    wanted_chars = ['1-2-3-4-5-6-sheldon', '1-2-3-4-5-6-leonard', \
                   '1-2-3-4-5-6-penny', '1-2-3-4-5-6-howard', \
                   '1-2-3-4-5-6-raj']
    ext = '-mainchars-by-season'
    """

    #### particular character
    """
    wanted_chars = ['3-bernadette', '4-bernadette', '5-bernadette', \
                    '6-bernadette']
    ext = '-bernadette-separate-season'
    """

    #### trends for a particular character
    """
    wanted_chars = ['1-leonard', '1-2-leonard', '1-2-3-leonard', \
                   '1-2-3-4-leonard', '1-2-3-4-5-leonard', \
                   '1-2-3-4-5-6-leonard']
    ext = '-leonard-increment-season'
    """

    sfilelist = [   'gen-chars-default-table.txt'
                    #'gen-chars-series-table.txt',
                    #'gen-chars-episode-table.txt',
                    #'gen-chars-scenes-table.txt',
                    #'gen-chars-series-trends-table.txt'
                 ]

    for f in sfilelist:
        print '----> processing file:', f
        newf = re.sub('.txt','-custom'+ext+'.txt',f)
        get_specific_chars(f, '|', newf, wanted_chars, TABLE_CHAR_INDIR)
        get_model(newf, '|', TABLE_CHAR_INDIR, TABLE_CHAR_OUTDIR)

        # then run for each individual character
        
        # doesn't apply for default (all) or trends
        if f == 'gen-chars-default-table.txt': continue
        if 'series-trends' in f: continue
        
        for c in wanted_chars:
            newf = re.sub('.txt','-custom-'+c+'.txt',f)
            get_specific_chars(f, '|', newf, [c], TABLE_CHAR_INDIR)
            get_model(newf, '|', TABLE_CHAR_INDIR, TABLE_CHAR_OUTDIR)
        
    

####
#### force-paired conversation (old)
####
def run_conv_pair():   
    # speaker-addressee pairs
    
    cfilelist = [#'gen-conv-main-pairs-table.txt',
                  #  'gen-conv-main-pairs-series-table.txt',
                  #  'gen-conv-main-pairs-episode-table.txt',
                  #  'gen-conv-main-pairs-scene-table.txt',
                    'gen-conv-main-pairs-utt-table.txt']
    
    for f in cfilelist:
        get_model(f, '|', TABLE_CHAR_INDIR, TABLE_CHAR_OUTDIR)
    
    #### create custom table; store back in input-tables/
    # this is for 'conv' only
    # files have '-custom' attached
    # empty means match anything
    # some files might be empty (TODO)
    
    want_speaker = '' #'sheldon'
    want_addressee = ['series-1',
                        'series-1-episode-01',
                        'series-1-episode-01-scene-01'
                        ]
    ccfilelist = [  'gen-conv-main-pairs-table.txt',
                    'gen-conv-main-pairs-series-table.txt',
                    'gen-conv-main-pairs-episode-table.txt',
                    'gen-conv-main-pairs-scene-table.txt'
                    ]

    for f in ccfilelist:
        for waddr in want_addressee:
            newf = re.sub('.txt', '-custom-'+want_speaker+'TO'+waddr+'.txt',f)
            get_specific_speaker_addressee(f, '|', newf, want_speaker, waddr, \
                    TABLE_CHAR_INDIR)
            get_model(newf,'|',TABLE_CHAR_INDIR, TABLE_CHAR_OUTDIR)
    

    
####
#### use ../gen-conv-multi/
####
def run_multi():
    
    filelist = [#'gen-conv-table.txt',
                #'gen-conv-pair-table.txt',
                #'gen-conv-series-table.txt',
                #'gen-conv-series-pair-table.txt',
                #'gen-conv-episode-table.txt',
                #'gen-conv-episode-pair-table.txt',
                #'gen-conv-all-table.txt',
                #'gen-conv-all-pair-table.txt',
                'gen-conv-all-pair-main-table.txt',
                #'gen-conv-series-pair-main-trends-table.txt'
                ]
    for f in filelist:
        print '----> processing file:', f
        get_model(f, '|', TABLE_MULTI_INDIR, TABLE_MULTI_OUTDIR)
    

def run_multi_pair():

    # pairs only
    #want_speaker = ['sheldon'] #['sheldon', 'leonard', 'penny', 'howard', \
    #                           #'raj', 'amy', 'bernadette']
    #want_addressee = ['leonard']
    want_speaker = ['any']
    want_addressee = ['series-1-2-3-4-5-6']

    ccfilelist = [  'gen-conv-pair-table.txt',
                    'gen-conv-series-pair-table.txt',
                    'gen-conv-episode-pair-table.txt',
                    'gen-conv-all-pair-table.txt',
                    'gen-conv-all-pair-main-table.txt',
                    'gen-conv-series-pair-main-trends-table.txt'
                    ]

    for f in ccfilelist:
        print '----> processing file:', f
        #newf = re.sub('.txt', '-custom-'+want_speaker+'TO'+waddr+'.txt',f)
        
        newf = re.sub('.txt', '-custom-'+'-'.join(want_speaker)+\
                '-TO-'+'-'.join(want_addressee)+'.txt',f)
        get_specific_speaker_addressee_multi_pair(f, '|', newf, want_speaker, \
                want_addressee, TABLE_MULTI_INDIR)
        get_model(newf,'|',TABLE_MULTI_INDIR, TABLE_MULTI_OUTDIR)
        
        for c in want_speaker:
            # one character to all other main characters
            newf = re.sub('.txt', '-custom-'+c+'-TO'+'.txt',f)
            get_specific_speaker_addressee_multi_pair(f, '|', newf, [c],\
                want_addressee, TABLE_MULTI_INDIR)
            get_model(newf,'|',TABLE_MULTI_INDIR, TABLE_MULTI_OUTDIR)        
    

####
#### Main
####
if __name__ == "__main__":

    #### by characters
    # Make sure to copy files from tv-modeling-features/, such as:
    #   'gen-chars-default-table.txt', 
    #   'gen-chars-series-table.txt',
    #   'gen-chars-episode-table.txt',
    #   'gen-chars-scenes-table.txt',
    #   'gen-chars-series-trends-table.txt'
    #
    # (currently used)
    run_chars()
    run_chars_custom()

    # force-paired conversations 
    # (not used currently)
    #run_conv_pair() 

    # multi-party conversations (including paired conversations)
    # (currently used)
    run_multi() 
    #run_multi_pair()


