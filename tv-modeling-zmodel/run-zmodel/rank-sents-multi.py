from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Rank sentences.

The difference between this file (rank-sents-multi) and 
rank-sents.py is that this file uses:
	f = open(os.path.join('output-tables-multi', ...

rather than "forced" conversation in rank-sents.py
---------------------------------------------------------------
"""

import sys, os, re, copy, subprocess, pickle
from collections import defaultdict, Counter

from scipy.spatial import distance

from common import plot_and_fit_regular, plot_and_fit_hist

#/////////////////////////////////////////////////////////////////////

SRILMDIR = '/Volumes/hhd/Dialogue/srilm171/bin/macosx/'

# original sents
#GEN_CONV_DIR = os.path.join('..','..','..','tv-modeling','bbt', 'gen-conv')
#SENT_INDIR = os.path.join(GEN_CONV_DIR, 'conv-input-main-pairs-utt')

OUTDIR = 'output-rank-sents-multi'

#/////////////////////////////////////////////////////////////////////


def get_ref_sents_info_multi(match_ref, ref_type):
	#### reference for comparison
	# get the average model and zvalues for series 1, episode 1, scene 1

	if ref_type=='scene':
		# use scene as reference

		# each z-model == one speaker-addressee's utterances from a scene 
		# by series, episode, scene
		# look for lines starting with '_mean', '_stdev'
		#
		# format:
		# speaker==addressee|avg-content-wlen| ...
		# _mean|...
		# _stdev|...
		# amy==amy-series-5-episode-08-scene-11-a-bridal-store|0.222146536513| ...
		# ...
		ref_type = 'conv'

		f = open(os.path.join('output-tables-multi', \
				'gen-conv-pair-table-custom-main-pairs-zvalues.txt'))

		# the following has scene info
		#'gen-conv-pair-table-zvalues.txt'
		#'gen-conv-pair-table-custom-main-pairs-zvalues.txt'

		# use scenes as reference, with more refined results
		#ref_type = 'conv'
		#match_ref = 'series-1-episode-01-scene-01'
		#f = open('output-tables/gen-conv-main-pairs-scene-table-custom-TO'+match_ref+'-zvalues.txt')
		
	elif ref_type=='pair': #(currently used)
		# use default pair table 

		ref_type = 'conv'
		#match_ref = '*'

		f = open(os.path.join('output-tables-multi', \
			'gen-conv-all-pair-main-table-zvalues.txt'))

		# speaker-addressee
		#'gen-conv-all-pair-table-zvalues.txt'
		#'gen-conv-all-pair-table-custom-main-pairs-zvalues.txt'

		# the following has different set of features
		#'gen-conv-all-pair-main-table-zvalues.txt'
		#'gen-conv-all-pair-main-table-custom-main-pairs-zvalues.txt'


	elif ref_type=='episode':
		# use episode as reference

		ref_type = 'conv'

		# this file contains main pairs only
		f = open(os.path.join('output-tables-multi', \
			'gen-conv-episode-pair-table-custom-main-pairs-zvalues.txt'))

		# contains all pairs (not just main pairs)
		#'gen-conv-episode-pair-table-zvalues.txt'


		# custom
		#ref_type = 'conv'
		#match_ref = 'series-1-episode-01'
		#f = open('output-tables/gen-conv-main-pairs-episode-table-custom-sheldonTO'+match_ref+'-zvalues.txt')

	elif ref_type=='series':
		# use series as reference

		ref_type = 'conv'

		# this file contains main pairs only
		f = open(os.path.join('output-tables-multi', \
			'gen-conv-series-pair-table-custom-main-pairs-zvalues.txt'))

		# this file contains all pairs (not just main pairs)
		#'gen-conv-series-pair-table-zvalues.txt'

		#match_ref = 'series-1|' #'*' #series-1|'

	else:
		print 'ERROR. Unknown ref ref_type:', ref_type
		return

	#### read ref
	lines = f.readlines()
	header_arr = lines[0].strip().split('|')

	# use a particular set of z-scores
	# e.g., series 1, episode 1, scene 1
	#		leonard==others-series-1-episode-01-scene-01-a-corridor-at-a-sperm-bank
	#		9	
	#		liwc-Fillers(2.29),liwc-Negations(3.33),liwc-Number(2.11),
	#		liwc-Other-Punctuation(2.64),verb-base(2.43),verb-verb-base-ratio(2.69),
	#		word-whereas-ratio-catw(5.21),word-which(6.30),word-which-ratio-allw(4.85),
	#zmodel2 = ['liwc-Fillers','liwc-Negations','liwc-Number',
	#			'liwc-Other-Punctuation','verb-base','verb-verb-base-ratio',
	#			'word-whereas-ratio-catw','word-which','word-which-ratio-allw']

	sents_zscore_ref = []
	# save average and standard deviation
	avg = {}
	stdev = {}

	for line in lines[1:]:
		#print line[:50], '...'

		if '_mean' in line:
			arr = line.strip().split('|')
			avg = dict(zip(header_arr, arr))
		elif '_stdev' in line:
			arr = line.strip().split('|')
			stdev = dict(zip(header_arr, arr))
		elif (match_ref == '*') or (match_ref in line):
			# e.g., if match_ref== 'series-6-episode-03-scene-09-'
			# then it'll look for lines containing it
			arr = line.strip().split('|')
			entry = dict(zip(header_arr, arr))
			sents_zscore_ref.append(entry)

		#elif len(avg)>0 and len(stdev)>0 and 'series-1-episode-01-scene-01-' not in line:
		#	break

	print 'Num of ref z-models for comparison:', len(sents_zscore_ref)
	for sent in sents_zscore_ref[:4]:
		print '\t',
		if ref_type=='chars':
			print sent['speaker'], len(sent)
		else:
			print sent['speaker==addressee'], len(sent)
	print '\t etc...'

	"""
	print 'Num of features for avg/stdev:', len(avg), len(stdev)
	if ref_type=='chars':
		print '\tavg speaker:', avg['speaker']
		print '\tstdev speaker:', stdev['speaker']
	else:
		print '\tavg speaker==addressee:', avg['speaker==addressee']
		print '\tstdev speaker==addressee:', stdev['speaker==addressee']
	"""

	return avg, stdev, sents_zscore_ref


def get_sents(grep_txt):

	# get individual utterances' features
	# speaker | addressee | feat 1 | feat 2 | ...
	# amy|amy-series-5-episode-08-scene-11-a-bridal-store2| ...
	f = open(os.path.join('input-tables','gen-conv-main-pairs-utt-table.txt'))
	#f = open(os.path.join('input-tables', \
	#		'gen-conv-main-pairs-scene-progression-table.txt'))
	
	"""
	# all addressee presence
	input-tables-multi/gen-conv-all-table.txt // RIDemotions ...
	# all pair-only
	input-tables-multi/gen-conv-all-pair-table.txt // RIDemotions ...
	# all pair-only, main pairs
	input-tables-multi/gen-conv-all-pair-main-table.txt // actionapi-adjs-HeShe-HeShe ...
	
	# all addressee presence, with scene info
	input-tables-multi/gen-conv-table.txt // RIDemotions ... 
	# all pair-only, with scene info
	input-tables-multi/gen-conv-pair-table.txt // RIDemotions ...

	input-tables-multi/gen-conv-episode-pair-table.txt
	input-tables-multi/gen-conv-episode-table.txt

	input-tables-multi/gen-conv-series-pair-main-trends-table.txt
	input-tables-multi/gen-conv-series-pair-table.txt
	input-tables-multi/gen-conv-series-table.txt
	"""

	print 'grep text:', grep_txt
	
	lines = f.readlines()
	header_arr = lines[0].strip().split('|')

	# becomes a list of dict below
	sents = []

	for line in lines[1:]: # avoid header
		# addressee has series/episode info
		# e.g., series 1, scene 1, utterances
		if grep_txt in line:
			arr = line.strip().split('|')
			entry = dict(zip(header_arr, arr))
			sents.append(entry)

	#print 'Number of sents:', len(sents)
	#for sent in sents:
	#	print '\tspk:', sent['speaker'], 'addr:', sent['addressee'], \
	#			'sents:', len(sent)

	return sents


def run(ref_type, ref_grep_txt, test_grep_txt, zmodel_type):

	# get z-scored sentences
	#avg, stdev, sents_zscore_ref = get_ref_sents_info(ref_grep_txt, ref_type)
	avg, stdev, sents_zscore_ref = get_ref_sents_info_multi(ref_grep_txt, ref_type)
	#print avg.keys()
	
	# From origin sents find ones with matching LM sents, 
	#then find z-scores for those sentences
	# start with original set of sentences
	sents = get_sents(test_grep_txt)
	print 'Number of test z-models for comparison:', len(sents)

	print 'avg:', sorted(avg.keys())[:5], '...'
	print 'sent:', sorted(sents[0].keys())[:5], '...'

	sents_zscore = copy.deepcopy(sents)
	sents_not_found = Counter()

	for i, sent in enumerate(sents): 
		# find this sent's zscores
		for feat in sent.keys():
			if 'speaker' in feat or 'addressee' in feat:
				continue

			if feat in avg.keys(): # see if the feature is found in avg model
				if float(stdev[feat])==0:
					zscore = 0
				else:
					zscore = (float(sent[feat])-float(avg[feat]))/float(stdev[feat])
				sents_zscore[i][feat] = zscore
			else:
				sents_zscore[i][feat] = 'N/A'



	#for sent in sents_zscore:
	#	print 'spk:', sent['speaker'], 'addr:', sent['addressee'], \
	#			'feats:', len(sent)
	#exit()

	# go through each sentence's zscore and find cosine similarity with respect to 
	# each reference sentence
	match_list = []
	match_count = Counter()
	match_model = defaultdict(Counter)
	spkr_addr_list = []

	for i, sent in enumerate(sents_zscore):
		sent = sents_zscore[i]

		print '\nsent', i
		print 'spk:',sent['speaker']
		print 'addr:',sent['addressee']

		# try to match speaker and addressee portion
		if ref_type == 'chars':
			match = sent['speaker'] #[0:20]
		else:
			match = sent['speaker']+'=='+sent['addressee'] #[0:20]

		pair = re.sub('==','-', match)
		spkr_addr_list.append(pair)
		
		cosine_rank = defaultdict(list) # use list in case overlap
		feats_not_found_in_ref = Counter()

		# compare each sent with each ref sent
		for j, sent_ref in enumerate(sents_zscore_ref):
			#sent_ref = sents_zscore_ref[j]

			# get the zmodel base on zscores
			zmodel1 = [] # contains zscore >=1
			zmodel2 = [] # contains zscore >=2

			for k in sent_ref.keys():
				if ('speaker' in k) or ('addressee' in k):
					continue
				v = abs(float(sent_ref[k])) 
				if v>=1:
					zmodel1.append(k)
					if v>=2:
						zmodel2.append(k)
			#print zmodel2

			feats_list = []

			sent_zscore_list = []
			sent_zscore_ref_list = []

			for f in sent.keys():
				#print f, sent[f]

				if ('speaker' in f) or ('addressee' in f):
					continue
				if sent[f] == 'N/A':
					continue
				if f not in sent_ref:
					# TODO: is this right?
					feats_not_found_in_ref[f]+=1
					#print '----> feat not found in ref (skip comparison):', f
					continue

				#print len(sent), len(sent_ref)

				# use a particular zmodel
				# TODO: instead of using zmodel2, use ref_list's zscores
				if zmodel_type==0:
					# use all feats
					feats_list.append(f)
					sent_zscore_list.append((float(sent[f])))
					sent_zscore_ref_list.append((float(sent_ref[f])))
				else:
					# use features with zscore >=2 (zmodel2)
					# or zscore >= 1 (zmodel1)
					zmodel_c = zmodel2 if zmodel_type == 2 else zmodel1
					if f in zmodel_c:
						feats_list.append(f)
						sent_zscore_list.append((float(sent[f])))
						sent_zscore_ref_list.append((float(sent_ref[f])))
						#print i, j, f, float(sent[f]), float(sent_ref[f])

			# skip if no feats found
			if not len(feats_list):
				continue

			#print 'num of feats:', len(feats_list)

			# cosine similarity
			#print 'ref sent', j, 'feat size:', len(sent_zscore_list), len(sent_zscore_ref_list)
			dist = distance.cosine(sent_zscore_list, sent_zscore_ref_list)
			#dist = distance.euclidean(sent_zscore_list, sent_zscore_ref_list)
			#dist = distance.correlation(sent_zscore_list, sent_zscore_ref_list)
			dist_round = round(dist, 4)
			#print dist_round

			if True: #dist_round>0:
				# get the actual text
				#base = sent_ref['speaker==addressee']
				#print base
				# turn number is attached at the end, make it separate
				# e.g., "xxx.1.txt"
				# non-greedy before the turn number
				#base2 = re.sub(r'(.+?)(\d+)$', r'\1.\2', base)+'.txt'
				#filename = re.sub('==','-',base2)
				#ff = open('../gen-conv/conv-input-main-pairs-utt/'+filename)
				# print result
				if ref_type=='chars':
					keyidx = 'speaker'
				else:
					keyidx = 'speaker==addressee'

				sp_addr = sent_ref[keyidx].split('-series-')[0]
				if sp_addr in match:
					mstr = 'MATCHED '+sp_addr
				else:
					mstr = 'NOT MATCHED'

				tostr = '\tref:'+' '+sent_ref[keyidx][0:50]+ '... '+ \
						str(dist_round)+' '+mstr #+' ['+ff.readline().strip()[0:20]+' ...'
				
				cosine_rank[dist_round].append(tostr)
				#ff.close()

		print 'feats not found in ref:', len(feats_not_found_in_ref), \
				feats_not_found_in_ref.keys()

		print '\tcosine vals:', cosine_rank.keys()[0:5], '...'
		print '\tneed to match:', match

		match_found = False

		for z, cosine_val in enumerate(sorted(cosine_rank)):
			arr = cosine_rank[cosine_val]
			#print '\tcosine', cosine_val,'has number of items:', len(arr)

			for txt in arr:
				#print '\trank', z, 'cosine', cosine_val, txt

				if 'NOT MATCHED' not in txt:

					if z==0: # smallest val (largest similarity)
						if not match_found:
							print '\trank', z, 'cosine', cosine_val, txt,
							print '* (smallest item) (MATCH)'
							match_count[z]+=1
							match_found=True
							# add matcched model
							spkr_addr = txt.split('MATCHED')[1].strip()	
							match_model[spkr_addr][z]+=1

						if ref_type=='chars':
							match_list.append(sent['speaker'])
						else:
							match_list.append(sent['speaker']+'=='+sent['addressee'])

					# found match
					# count first one only
					if not match_found:
						print '\trank', z, 'cosine', cosine_val, txt
						match_count[z]+=1
						match_found=True
						# add matched model
						spkr_addr = txt.split('MATCHED')[1].strip()	
						match_model[spkr_addr][z]+=1				
		
		# get smallest item
		#k = sorted(cosine_rank.keys())[0]
		#print '\tsmallest:', k, cosine_rank[k]
		"""
		for item in cosine_rank[k]:
			if 'NOT MATCHED' not in item:
				match_count += 1
				if ref_type=='chars':
					match_list.append(sent['speaker'])
				else:
					match_list.append(sent['speaker']+'=='+sent['addressee'])
				break
		"""

	data = []
	rank_list = []
	match_cnt_list = []

	# sentences
	print '\nsents:', len(sents_zscore)
	print 'match list (rank 0):', len(match_list)
	print 'match distr:'
	for rank in sorted(match_count):
		acc = match_count[rank] / len(sents_zscore)
		print '\trank:', rank, 'match count:', match_count[rank], '(',acc,')'
		# keep track for plots later
		rank_list.append(rank)
		match_cnt_list.append(match_count[rank])
		data += [rank]*match_count[rank]

	print '\nLM mismatch', sum(sents_not_found.values()), 'out of', len(sents)
	for sent in sents_not_found:
		print sent, sents_not_found[sent]


	return data, rank_list, match_cnt_list, match_model, spkr_addr_list


####
#### Main
####
if __name__ == "__main__":

	# get referenced z-model
	
	# ref_type and ref_grep_txt need to correspond
	"""
	ref_pairs = [
		# 'trivial': one z-model == one speaker-addressee utterance
		# '*': all z-models (i.e., all sentences)
		# Results should have everything match with rank 0
		('trivial', '*'),
		# z-models contain <speaker>==<addressee>					
		# Only test-zmodels containing <speaker>==<addressee> 
		# 	should have a match.
		# Results should all match as well.
		('trivial', 'leonard==sheldon'), 
		# all z-models from series 1, episode 1, scene 1
		# This means any speaker==addressee pair will be used.
		('trivial', 'series-1-episode-01-scene-01-'),
		# "scene": one z-model == one speaker-addressee utterances from a scene
		# doesn't do as well since the zmodel was built by scenes
		('scene', 'series-1-episode-01-scene-01'),
		('scene', '*', 'rank-sents-z-scene.pickle'),
		('episode', '*', 'rank-sents-z-episode.pickle'),
		('series', '*', 'rank-sents-z-series.pickle'),
		('chars', '*', 'rank-sents-z-chars.pickle'),
		('pair', '*', 'rank-sents-z-pair.pickle'),
		('custom_sheldon_to', '')]
	zmodel_type=0
	"""


	for zmodel_type in range(3):

		ref_pairs2 = [
			#('chars', '*', 'rank-sents-z-chars-zmodel'+str(zmodel_type)+'-multi.pickle'),
			('pair', '*', 'rank-sents-z-pair-zmodel'+str(zmodel_type)+'-multi.pickle'),
			#('series', '*', 'rank-sents-z-series-zmodel'+str(zmodel_type)+'-multi.pickle'),
			#('episode', '*', 'rank-sents-z-episode-zmodel'+str(zmodel_type)+'-multi.pickle'),
			#('scene', '*', 'rank-sents-z-scene-zmodel'+str(zmodel_type)+'-multi.pickle')
		]

		for (curr_ref_type, curr_ref_grep_txt, pickle_fname) in ref_pairs2:

			# data to be stored as pickle file
			data_store = defaultdict(dict)

			# these are the sentences (with mix speaker/addressee) we'll be testing on	
			grep_txt_list = ['series-1-episode-01-']

			for curr_test_grep_txt in grep_txt_list:

				print '==== ref_type:', curr_ref_type
				print '==== ref grep text:', curr_ref_grep_txt
				print '==== test grep text:', curr_test_grep_txt

				data, rank_list, match_cnt_list, match_model, spkr_addr_list = \
					run(curr_ref_type, curr_ref_grep_txt, \
						curr_test_grep_txt, zmodel_type)

				# store data
				data_store[curr_test_grep_txt]['data']=data
				data_store[curr_test_grep_txt]['rank_list']=rank_list
				data_store[curr_test_grep_txt]['match_cnt_list']=match_cnt_list
				data_store[curr_test_grep_txt]['match_model_distr']=match_model
				data_store[curr_test_grep_txt]['spkr_addr_list']=spkr_addr_list
			# store data as picke file
			pickle.dump(data_store, open(os.path.join(OUTDIR, pickle_fname), 'w'))
			
			"""
			# load data from pickle file
			data_store = pickle.load(open(os.path.join(OUTDIR, pickle_fname)))

			# plot
			for curr_test_grep_txt in data_store:

				data = data_store[curr_test_grep_txt]['data']
				rank_list = data_store[curr_test_grep_txt]['rank_list']
				match_cnt_list = data_store[curr_test_grep_txt]['match_cnt_list']

				num_sents = len(data)

				fname = os.path.join(OUTDIR, \
					'rank-sents-cnt-'+str(num_sents)+ \
					'-zmodel'+str(zmodel_type)+ '-'+curr_test_grep_txt+'.png')

				plot_and_fit_regular(rank_list, match_cnt_list, fname)
				plot_and_fit_hist(data, re.sub('-cnt-', '-hist-', fname))
			"""


