Z-models and rank sentences
Last update: 10/11/15


-----------------
Create z-models
-----------------

- Create standard scores for each feature for each char and speaker-addressee
- Standard score: Number of standard deviations outside the population;
	significant ones are far away from the population (stdev >1, >2, etc.)
- Average model: average each features across all all characters

Files/directories:

- gen-zmodel.py: script generate z-model and store in output-tables/

- input-tables/
	- characters; a copy of the tables ran from ../gen-features
		(or from tv-modeling-features/bbt/gen-features/)
    - also contains other tables with specific characters
    	(see gen-zmodel.py for this)
    - goes with output-tables/

- output-tables/	 
	- files "*-zvalues.txt" : raw z-values
	- files "*-zmodel1.txt" : >= 1 standard deviation
	- files "*-zmodel2.txt" : >= 2 standard deviation
	- see *** below

- input-tables-multi/
	- conversations version of input-tables/; a copy of tables from ../gen-conv
	- goes with output-tables-multi/

***
	Files "*-custom-*" : custom tables - some file are empty.
	E.g., gen-conv-main-pairs-series-table-custom-sheldonTOseries-1-episode-01
	This is empty because the table is by series/season only, 
	and there's no episode info. TODO: Probably needs to fix this at some point.


-------------------------------------------------
Use z-model and LM to rank a group of sentences
-------------------------------------------------

- rank-sents.py
	- use zmodel to rank sentences

- rank-sents-lm.py  
	- use trained language model (LM) to rank sentences, as comparison to 
		zmodel ranked
	- LM results directory: 
		GEN_LM_DIR = os.path.join('..','..','..','tv-modeling-lm','bbt','gen-lm') 
	- TODO: this probably should be somewhere else? But it's easier for comparison






