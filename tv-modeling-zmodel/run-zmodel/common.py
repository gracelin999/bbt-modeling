
from scipy.stats import norm
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np
from scipy.interpolate import interp1d

#/////////////////////////////////////////////////////////////////////

def plot_and_fit_regular(x, y, fname, title=None):
	plt.clf()
	plt.cla()

	xnew = np.linspace(min(x), max(x), num=len(x)*10, endpoint=True)

	# linear and cubuc fit
	f = interp1d(x, y)
	f2 = interp1d(x, y, kind='cubic')

	plt.plot(x, y, 'o', xnew, f(xnew), '-')
	
	plt.ylim([0,max(y)+1])
	plt.legend(['data', 'linear'], loc='best')
	plt.ylabel('Count')
	plt.xlabel('Rank')

	title = fname if not title else title
	plt.title(title)

	plt.grid()

	plt.savefig(fname)


def plot_and_fit_hist(data, bins, fname, title=None):
	plt.clf()
	plt.cla()

	n, bins, patches = plt.hist(data, bins=bins, normed=0, facecolor='green', alpha=0.75)

	# norm fit
	#(mu, sigma) = norm.fit(data)
	#print fname, ':', 'mu =', mu, 'sigma = ', sigma
	#ygauss = mlab.normpdf(bins, mu, sigma)
	#plt.plot(bins, ygauss, 'r--')

	plt.ylabel('Count')
	plt.xlabel('Dist from ideal (0)')
	
	title = fname if not title else title
	plt.title(title)
	plt.ylim([0,max(n)])
	plt.grid()

	plt.savefig(fname)


def plot_and_fit_hist2(data_z, data_lm, bins, fname, title=None, labels=['Z', 'LM']):
	plt.clf()
	plt.cla()

	colors = ['green', 'blue']


	n1, bins1, patches1 = plt.hist(data_z, bins=bins, \
							facecolor=colors[0], normed=1, alpha=0.75, label=labels[0])
	n2, bins2, patches2 = plt.hist(data_lm, bins=bins, \
							facecolor=colors[1], normed=1, alpha=0.75, label=labels[1])

	max_n1_n2 = max(max(n1), max(n2))
	print 'max n1, n2, both', max(n1), max(n2), max_n1_n2

	# norm fit
	for data, bins, color in [(data_z, bins1, colors[0]), (data_lm, bins2, colors[1])]:
		(mu, sigma) = norm.fit(data)
		print fname, ':', 'mu =', mu, 'sigma = ', sigma
		ygauss = mlab.normpdf(bins, mu, sigma)
		plt.plot(bins, ygauss, color[0]+'--')

	plt.ylabel('Count (normalized)')
	plt.xlabel('Dist from ideal (0)')
	
	title = fname if not title else title
	plt.title(title)
	plt.ylim([0,max_n1_n2])
	plt.legend(loc='upper right')

	plt.grid()

	plt.savefig(fname)	


