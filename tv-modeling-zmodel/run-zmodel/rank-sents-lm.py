from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Rank sentences.


To run:
>python rank-sent.py
---------------------------------------------------------------
"""

import sys, os, re, copy, subprocess, timeit, pickle
from collections import defaultdict, Counter
from scipy.spatial import distance
import nltk

#from common import plot_and_fit_regular, plot_and_fit_hist

#/////////////////////////////////////////////////////////////////////

SRILMDIR = '/Volumes/hhd/Dialogue/srilm171/bin/macosx/'

GEN_CONV_DIR = os.path.join('..','..','..','tv-modeling','bbt', 'gen-conv')
SENT_INDIR = os.path.join(GEN_CONV_DIR, 'conv-input-main-pairs-utt')

# trained LM models (files with extension ".bo")
GEN_LM_DIR = os.path.join('..','..','..','tv-modeling-lm','bbt','gen-lm')
LM_DEFAULT_INDIR = os.path.join(GEN_LM_DIR,'conv','conv-output-lm-main-pairs')
LM_SERIES_INDIR = os.path.join(GEN_LM_DIR,'conv','conv-output-lm-main-pairs-series')
LM_EPISODE_INDIR = os.path.join(GEN_LM_DIR,'conv', 'conv-output-lm-main-pairs-episode')
LM_SCENE_INDIR = os.path.join(GEN_LM_DIR,'conv', 'conv-output-lm-main-pairs-scene')

LM_LIWC_TAGGED_DEFAULT_INDIR = os.path.join(GEN_LM_DIR, 'conv-liwc-tagged', \
	'conv-lm-liwc-tagged-main-pairs')
LM_LIWC_TAGGED_SERIES_INDIR = os.path.join(GEN_LM_DIR, 'conv-liwc-tagged', \
	'conv-lm-liwc-tagged-main-pairs-series')
LM_LIWC_TAGGED_EPISODE_INDIR = os.path.join(GEN_LM_DIR, 'conv-liwc-tagged', \
	'conv-lm-liwc-tagged-main-pairs-episode')
LM_LIWC_TAGGED_SCENE_INDIR = os.path.join(GEN_LM_DIR, 'conv-liwc-tagged', \
	'conv-lm-liwc-tagged-main-pairs-scene')

CHAR_LM_DEFAULT_INDIR = os.path.join(GEN_LM_DIR, 'chars', 'chars-output-lm')
CHAR_LM_SERIES_INDIR = os.path.join(GEN_LM_DIR, 'chars', 'chars-output-lm-series')
CHAR_LM_EPISODE_INDIR = os.path.join(GEN_LM_DIR, 'chars', 'chars-output-lm-episode')
CHAR_LM_SCENE_INDIR = os.path.join(GEN_LM_DIR, 'chars', 'chars-output-lm-scene')

CHAR_LM_LIWC_TAGGED_DEFAULT_INDIR = os.path.join(GEN_LM_DIR, 'chars-liwc-tagged', \
	'chars-lm-liwc-tagged-default')


LM_OUTDIR = 'output-lm'
OUTDIR = 'output-rank-sents-lm'

#/////////////////////////////////////////////////////////////////////

# sentence tokenizer
sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')


def parse_and_sort_results(results, outf, pplidx=0):
	# parse the results and sort and returns least amount of perplexity items
	# pplidx indicates which entry to retrieve, rather than, say, always 
	# retireve the first two entries

	# Results as three-line tuple:
	# 	<lm filename (.bo extension)>
	#	<actual sents file>
	#	<lm result stats>
	# For example:
	# 	howard-penny-series-4-episode-03-scene-02-the-cheesecake-factor.onesentperline.bo
	# 	file conv/test-input/amy-leonard-others.onesentperline.txt: 1 sentences, 7 words, 7 OOVs
	# 	0 zeroprobs, logprob= -0.748188 ppl= 5.6 ppl1= undefined
	#

	# look at perpelexity
	ppldict = defaultdict(list)

	for key in results.keys():
		arr = results[key].split('\n')

		line2 = arr[2].strip() # 3rd line contains the result stats

		m = re.match(r'.+ logprob=(.+) ppl=(.+) ppl1', line2)
		logprob = float(m.group(1))	# log prob
		ppl = float(m.group(2))	# perplexity

		ppldict[ppl].append(key)


	# sort and write to output
	min_perplexity = []

	if pplidx>=len(ppldict):
		return min_perplexity

	f = open(outf, 'w')
	for i, k in enumerate(sorted(ppldict.keys())):
		v = ppldict[k]
		# write to file
		for vi in v:
			#print results[vi]
			f.write(results[vi])
			# get some entries from least amount of perplexity
			if i==pplidx:
				r = results[vi]
				a = r.split('\n')
				min_perplexity.append(a[0])

	f.close()

	# return the first two entries (least amount of perplexity)
	"""
	arr = []
	for i in [0,1]:
		k = sorted(ppldict.keys())[i]
		v = ppldict[k]
		for vi in v:
			r = results[vi] # results
			a = r.split('\n')
			arr.append(a[0])
	"""

	return min_perplexity	



def get_scene_info(desc):
	#print desc
	season = 0; episode = 0; scene = 0

	if 'scene' in desc:
		m = re.match('^(\d)-episode-(\d\d)-scene-(\d\d)-', desc)
		season = m.group(1)
		episode = m.group(2)
		scene = m.group(3)
	elif 'episode' in desc:
		m = re.match('^(\d)-episode-(\d\d)', desc)
		season = m.group(1)
		episode = m.group(2)
	else:
		m = re.match('^(\d)-', desc)
		if m:
			season = m.group(1)

	return int(season), int(episode), int(scene)
		


#### 
#### Compare with pre-built langauge model.
#### e.g., this assumes gen-lm/conv/conv-output-lm-*/ exist.
####
def test_lm(pair, LM_INDIR, debug=False):

	if 'conv' in LM_INDIR:
		isconv = True
	else:
		isconv = False

	# get sents
	m = re.match('(.+?)(\d+)$', pair)
	fhead = m.group(1)
	utt = m.group(2)

	# hack.. # TODO
	if 'leonards-apartment' in fhead:
		fhead = re.sub('leonards-apartment', "leonard's-apartment", fhead)
	elif 'leonards-car' in fhead:
		fhead = re.sub('leonards-car', "leonard's-car", fhead)
	elif 'pennys-old-apartment' in fhead:
		fhead = re.sub('pennys-old-apartment', "penny's-old-apartment", fhead)
	elif 'pennys-ex-boyfriends-apartment' in fhead:
		fhead = re.sub('pennys-ex-boyfriends-apartment', \
			"penny's-ex-boyfriend's-apartment", fhead)
	elif 'sheldon--inside-leonards-ca' in fhead:
		fhead = re.sub('sheldon--inside-leonards-ca', \
			"sheldon,-inside-leonard's-ca", fhead)
	elif 'leonards-building' in fhead:
		fhead = re.sub('leonards-building', "leonard's-building", fhead)
	elif 'leonards-living-room' in fhead:
		fhead = re.sub('leonards-living-room', "leonard's-living-room". fhead)

	lines = open(os.path.join(SENT_INDIR,fhead+'.'+utt+'.txt')).readlines()

	# write sents to a file (with .sent.txt extension)
	sents_fpath = os.path.join(LM_OUTDIR,pair+'.sent.txt')
	fout = open(sents_fpath, 'w')

	for line in lines:
		sents = sent_tokenizer.tokenize(line.strip())
		fout.write('\n'.join(sents)+'\n')

	fout.close()
	
	# store results of lm results
	lm_results_fpath = os.path.join(LM_OUTDIR,pair+'.out.txt')
	# clear it first as we need to append later
	fout = open(lm_results_fpath, 'w')
	fout.write('')
	fout.close()

	# computes the perplexity on test data according to prebuild LM
	lm_prebuilt = os.listdir(LM_INDIR)

	lm_results = {}

	# go through each prebuild LM and test on test data
	for lmp in lm_prebuilt:
		# command example:
		# ngram -lm sheldon-leonard.bo -ppl test.txt
		cmd = [SRILMDIR+'ngram', '-lm', os.path.join(LM_INDIR,lmp), 
				'-ppl', sents_fpath]
		#print cmd
		p=subprocess.check_output(cmd)
		#print p
		lm_results[lmp]=lmp+'\n'+p 	# use name of the lm model as key
		
		# append results to output file
		fout = open(lm_results_fpath, 'a')
		fout.write(lm_results[lmp])
		fout.close()


	#### sort the result by log prob (perplexity)
	lm_results_fpath_sorted = re.sub('.txt', '.sorted.txt', lm_results_fpath)

	# find results that match with desired conversants ('pair') 
	arr = pair.split('-series-')

	if isconv:
		name_compare = arr[0]
	else:
		name_compare = arr[0].split('-')[0]

	desc = arr[1]
	season, episode, scene = get_scene_info(desc)
	#print 'season', season, 'episode', episode, 'scene', scene



	#if debug: print '\n\tlm name to compare:', name_compare

	chars_found = [] # keep track of spaker-addressee matches
	chars_rank = []
	chars_match_found = False

	items_found = [] # keep track of speaker-addressee, and season/episode/scene match
	items_rank = []
	items_match_found = False

	pplidx = 0 # retrieve by rank; 0 would be the smallest perplexity

	while not items_found:
		min_perplexity = parse_and_sort_results(lm_results, \
							lm_results_fpath_sorted, pplidx)

		#if debug:
		#	print '\tidx:', pplidx, '; items in min_perplexity:', \
		#			len(min_perplexity)

		# no more items...
		if not min_perplexity:
			break
		
		# there could be more than one LM producing the same perplexity with 
		# this sent
		for item in min_perplexity:
			# check if speaker-addressee match; more specifically at the beginning
			if isconv:
				name_compare2 = item[:len(name_compare)]
			else:
				# characters only (speaker only)
				name_compare2 = item.split('.onesentperline.')[0]
				# if it is in form of <char name>.liwctagged.onesentperline.bo
				# then remove ".liwctagged" as well
				name_compare2 = re.sub('.liwctagged','',name_compare2)

			#print 'item; compare (needs to match):', item, name_compare, name_compare2

			if name_compare != name_compare2:
				continue

			if isconv and '-others' in item:
				continue
				
			# found speaker-addressee match; keep first one only
			if not chars_match_found:
				chars_found.append(item)
				chars_rank.append(pplidx)
				chars_match_found = True
				if debug: 
					print '\tidx:', pplidx, 'chars match found:', item, \
						'(first found is stored)'
			#elif debug:
			#	print '\tidx:', pplidx, 'chars match found:', item

			# check if season/episode/scene info is there
			matched = False

			remaining = item[len(name_compare):]
			if '-series-' not in remaining:
				# here the trained model only has chars info
				matched = True
			else:
				desc2 = remaining.split('-series-')[1]
				season2, episode2, scene2 = get_scene_info(desc2)
				#print desc2
				#print 'season', season2, 'episode', episode2, 'scene', scene2

				if scene:
					if season==season2 and episode==episode2 and scene==scene2:
						matched = True
				elif episode:
					if season==season2 and episode==episode2:
						matched = True
				elif season:
					if season==season2:
						matched = True

			if matched:
				if not items_match_found:
					items_found.append(item)
					items_rank.append(pplidx)
					items_match_found = True
					if debug: 
						print '\tidx:', pplidx, 'chars/scene match found:', item, \
							'(first found is stored)'
				elif debug:
					print '\tidx:', pplidx, 'chars/scene match found:', item

		pplidx+=1

	#if not found:
	#	print 'ERROR.', name_compare, 'did not match with least perplexity: ', ar
	#print 'name to compare:', name_compare, 'items found:', len(items_found)

	return items_found, items_rank, chars_found, chars_rank



def get_sents(grep_txt):

	# get individual utterances' features
	# speaker | addressee | feat 1 | feat 2 | ...
	# amy|amy-series-5-episode-08-scene-11-a-bridal-store2| ...
	f = open(os.path.join('input-tables','gen-conv-main-pairs-utt-table.txt'))
	#f = open(os.path.join('input-tables', \
	#		'gen-conv-main-pairs-scene-progression-table.txt'))
	
	lines = f.readlines()
	header_arr = lines[0].strip().split('|')

	# becomes a list of dict below
	entries = []

	for line in lines[1:]: # avoid header
		# addressee has series/episode info
		# e.g., series 1, scene 1, utterances
		if grep_txt in line:
			arr = line.strip().split('|')
			entry = dict(zip(header_arr, arr))
			if grep_txt in entry['addressee']:
				entries.append(entry)

	return entries


def run(lm_dir, grep_txt):

	# From origin sents find ones with matching LM sents, 
	#then find z-scores for those sentences
	#print 'Find z-scores for each sent with matched LM'
	# start with original set of sentences
	entries = get_sents(grep_txt)
	print 'Number of sents:', len(entries)
	spkr_addr_list = []

	chars_entries_not_found = Counter()
	chars_match_count = Counter()
	chars_match_model_distr = defaultdict(Counter)

	entries_not_found = Counter()
	match_count = Counter()
	match_model_distr = defaultdict(Counter)

	for i, entry in enumerate(entries): 
		#sent = sents[i]
		print '\n\tspk:', entry['speaker']
		print '\taddr:', entry['addressee'], \
				'feats:', len(entry)-2

		# Apply LMs to this sent and get the LM that gives least perplexity value.
		# Ideally the LM that gives least perplexcity should be the 1st one,
		# but in reality it could be the 2nd least or 3rd least, etc.

		pair = entry['speaker']+'-'+entry['addressee']
		spkr_addr_list.append(pair)

		lm_list_sorted, lm_rank_sorted, \
		chars_lm_list_sorted, chars_lm_rank_sorted = \
			test_lm(pair, lm_dir, debug=True)

		# if no LM found then go to next sentence
		if not lm_list_sorted: 
			entries_not_found[pair]+=1
		else:
			for ii, rank in enumerate(lm_rank_sorted):
				match_count[rank]+=1
				# store by model name
				match_model_name = lm_list_sorted[ii]
				match_model_distr[match_model_name][rank]+=1


			"""
			print '\nSent', i, ':calculate features\' z-scores for matched LM'
			print '\tSent:', pair
			print '\tLM rank (lower is better):', lm_rank
			for lm in lm_list_sorted:
				print '\tLM found:',lm
			"""

		# do the same for characters only
		if not chars_lm_list_sorted: 
			chars_entries_not_found[pair]+=1
		else:
			for ii, rank in enumerate(chars_lm_rank_sorted):
				chars_match_count[rank]+=1
				# store by model name
				match_model_name = chars_lm_list_sorted[ii]
				chars_match_model_distr[match_model_name][rank]+=1

	data = []
	rank_list = []
	match_cnt_list = []

	# stats
	print 
	#print 'match list:', len(match_list)
	print '---- match distr ----'
	for rank in sorted(match_count):
		acc = match_count[rank] / sum(match_count.values())
		print '\trank:', rank, 'match count:', match_count[rank], '(',acc,')'
		# keep track for plots later
		rank_list.append(rank)
		match_cnt_list.append(match_count[rank])
		data += [rank]*match_count[rank]

	print '\nLM mismatch', sum(entries_not_found.values()), 'out of', len(entries)
	for entry in entries_not_found:
		print entry, entries_not_found[entry]

	print '\nMatched model distr:', match_model_distr


	print 
	print '---- chars only match distr ----'
	for rank in sorted(chars_match_count):
		acc = chars_match_count[rank] / sum(chars_match_count.values())
		print '\trank:', rank, 'match count:', chars_match_count[rank], '(',acc,')'

	print '\nLM mismatch', sum(chars_entries_not_found.values()), 'out of', len(entries)
	for entry in chars_entries_not_found:
		print entry, chars_entries_not_found[entry]

	print '\nMatched char model distr:', chars_match_model_distr

	return data, rank_list, match_cnt_list, match_model_distr, spkr_addr_list

####
#### Main
####
#if __name__ == "__main__":
def main_run():

	############## chars lm 
	# most currently used
	#curr_lm_dir = CHAR_LM_DEFAULT_INDIR
	#curr_pickle_name = 'rank-sents-lm-chars.pickle'

	############## chars lm-liwc tagged
	# most currently used
	#curr_lm_dir = CHAR_LM_LIWC_TAGGED_DEFAULT_INDIR
	#curr_pickle_name = 'rank-sents-lmliwc-chars-default.pickle'


	############## conv lm
	# most currently used
	#curr_lm_dir = LM_DEFAULT_INDIR
	#curr_pickle_name = 'rank-sents-lm-default.pickle'

	#curr_lm_dir = LM_SERIES_INDIR
	#curr_pickle_name = 'rank-sents-lm-series.pickle'

	#curr_lm_dir = LM_EPISODE_INDIR
	#curr_pickle_name = 'rank-sents-lm-episode.pickle'

	#curr_lm_dir = LM_SCENE_INDIR
	#curr_pickle_name = 'rank-sents-lm-scene.pickle'

	############# conv lm-liwc tagged
	# most currently used
	curr_lm_dir = LM_LIWC_TAGGED_DEFAULT_INDIR
	curr_pickle_name = 'rank-sents-lmliwc-default.pickle'

	#curr_lm_dir = LM_LIWC_TAGGED_SERIES_INDIR
	#curr_pickle_name = 'rank-sents-lmliwc-series.pickle'

	#curr_lm_dir = LM_LIWC_TAGGED_EPISODE_INDIR
	#curr_lm_dir = LM_LIWC_TAGGED_SCENE_INDIR


	
	#print 'ref_type:', ref_type
	print 'lm_dir:', curr_lm_dir
	print

	# data to be stored as pickle file
	data_store = defaultdict(dict)

	grep_txt_list = ['series-1-episode-01-']

	for curr_grep_txt in grep_txt_list:

		print '==== Processing sents from:', curr_grep_txt

		time_start = timeit.default_timer()
		data, rank_list, match_cnt_list, match_model_distr, spkr_addr_list = \
			run(curr_lm_dir, curr_grep_txt)
		time_stop = timeit.default_timer()

		print '\n==== Finished processing sents from:', curr_grep_txt
		print 'Runtime:', time_stop - time_start, 'secs'

		# store data
		data_store[curr_grep_txt]['data']=data
		data_store[curr_grep_txt]['rank_list']=rank_list
		data_store[curr_grep_txt]['match_cnt_list']=match_cnt_list
		data_store[curr_grep_txt]['match_model_distr']=match_model_distr
		data_store[curr_grep_txt]['spkr_addr_list']=spkr_addr_list


	# store data as pickle file
	pickle.dump(data_store, open(os.path.join(OUTDIR,curr_pickle_name), 'w'))
	



if __name__ == "__main__":
	main_run()
