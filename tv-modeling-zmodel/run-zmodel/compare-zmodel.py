
import os, re
from collections import defaultdict


def conv(z):
	f = open(os.path.join('output-tables-multi', \
						'gen-conv-all-pair-main-table-zmodel'+str(z)+'.txt'),'rb')
	lines = f.read().split('\n')
	header = lines[0].split('|')

	chars = ['sheldon','penny','leonard','howard','raj','bernadette','amy']
	char_inter = {}
	for c in chars:
		char_inter[c] = None

	char_conv = {}

	for line in lines[1:]:
		if not line: continue
		items = line.split('|')
		d = dict(zip(header, items))
		
		m = re.match('(^.+)==', d['speaker==addressee'])
		char = m.group(1)
		if char not in chars:
			print 'ERROR.', char, 'not in char'
		
		print char, d['speaker==addressee'], d['num feats'],

		arr = d['feats'].split(',')
		feats = []
		vals = []
		for f in arr:
			if not f: continue
			# pos-ngram2-UH-PERIOD(2.27)
			m = re.match('(.+)\((.+)\)', f)
			if m:
				feats.append(m.group(1))
				vals.append(m.group(2))

		char_conv[d['speaker==addressee']]=dict(zip(feats,vals))

		print len(feats), len(feats)==int(d['num feats'])

		if char_inter[char] == None:
			char_inter[char] = set(feats)
		else:
			inter = char_inter[char].intersection(set(feats))
			char_inter[char] = inter


		print len(char_inter[char]), list(char_inter[char])[:4], '...'


	print '-----------------'
	print 'convs:', len(char_conv.keys())

	print '-----------------'
	print 'common among all:'
	#print '-----------------'
	for c in char_inter:
		print c, len(char_inter[c]) #, list(char_inter[c])
		for f in char_inter[c]:
			print '\t', f
			# go through the conv pairs
			for pair in char_conv:
				#print c, pair
				m = re.search(c,pair)
				if not m: continue
				if m.start() != 0: continue
				#print pair, len(char_conv[pair])
				if f not in char_conv[pair]: continue
				print '\t\t', pair, '\t', char_conv[pair][f]
	print '-----------------'
	return char_conv




def compare_char_to_conv(char_conv, z):

	# compare character model to conversation models
	fname = 'gen-chars-default-table-custom-mainchars-zmodel'+str(z)+'.txt'

	f = open(os.path.join('output-tables', fname),'rb')
	lines = f.read().split('\n')
	header = lines[0].split('|')

	chars = {}

	for line in lines[1:]:
		if not line: continue
		items = line.split('|')
		d = dict(zip(header, items))
		
		char = d['speaker']
		
		print char, d['num feats'],

		arr = d['feats'].split(',')
		feats = []
		vals = []
		for f in arr:
			if not f: continue
			# pos-ngram2-UH-PERIOD(2.27)
			m = re.match('(.+)\((.+)\)', f)
			if m:
				feats.append(m.group(1))
				vals.append(m.group(2))

		chars[char]=dict(zip(feats,vals))

		print len(feats), len(feats)==int(d['num feats'])


	print '-----------------'
	char_inter = {}

	for c in chars:
		print c, len(chars[c]) #, list(char_inter[c])
		char_feats = chars[c]

		for pair in char_conv:
			m = re.search(c,pair)
			if not m: continue
			if m.start() != 0: continue

			print '\t', pair, len(char_conv[pair]),
			
			pair_feats = char_conv[pair]
			# compare features
			a = set(char_feats.keys())
			b = set(pair_feats.keys())
			inter = a.intersection(b)

			print '\t', len(inter),
			
			if c not in char_inter:
				char_inter[c] = inter
			else:
				char_inter[c] = char_inter[c].intersection(inter)
			
			print '\t', len(char_inter[c])

	print '----------'
	for c in char_inter:
		print c, list(char_inter[c])


if __name__ == "__main__":
	# use z=1 model
	char_conv = conv(2)
	#compare_char_to_conv(char_conv, 2)

	# find features that's only specific for certain conversants

	for pair_main in sorted(char_conv):
		feats_sa = set(char_conv[pair_main].keys())
		feats = feats_sa
		print pair_main, len(feats)
		# get first speaker from e.g., 'leonard==howard-'
		c = pair_main.split('==')[0] #'leonard'
		for pair in char_conv:
			if pair == pair_main: continue #'leonard==howard-': continue
			m = re.search(c,pair)
			if not m: continue
			if m.start() != 0: continue
			# find overlapping
			overlap = feats & set(char_conv[pair].keys())
			feats = feats-overlap
			print '\t', pair, len(feats)#, char_conv[pair].keys()
		print '\t', 'Final count', len(feats)
		for f in list(feats): #[:5]:
			print '\t\t', f, char_conv[pair_main][f]
		#print '\t\t...'

		# features common with addressee-speaker
		a = pair_main.split('==')[1][:-1] # e.g., sheldon==penny-
		pair_main_inv = a+'=='+c+'-'
		feats_as = set(char_conv[pair_main_inv].keys())
		overlap = feats_sa & feats_as
		print '\t** common with', pair_main_inv, len(overlap)
		for f in list(overlap):
			print '\t\t',f, char_conv[pair_main][f], char_conv[pair_main_inv][f]
		
		# features common with other pairs
		for pair in char_conv:
			if pair in [pair_main, pair_main_inv]: continue #'leonard==howard-': continue
			m = re.search(c,pair)
			if not m: continue
			if m.start() != 0: continue
			# find overlapping
			overlap = feats_sa & set(char_conv[pair].keys())
			print '\tcommon with', pair, len(overlap)
			for f in list(overlap):
				print '\t\t',f, char_conv[pair_main][f], char_conv[pair][f]
		



