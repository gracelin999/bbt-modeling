
import sys, os, re, pickle

import numpy as np
import matplotlib.pyplot as plt

from collections import defaultdict

from scipy.stats import pearsonr, chisquare, norm, ks_2samp, entropy
from scipy.spatial import distance as dist # euclidean, cityblock, chebyshev

sys.path.append(os.path.join('..','run-zmodel'))
from common import plot_and_fit_regular, plot_and_fit_hist, plot_and_fit_hist2

#/////////////////////////////////////////////////////////////////////

RANK_Z_DIR = 'output-rank-sents'
RANK_LM_DIR = 'output-rank-sents-lm'

DIR = 'output-compare'
OUTDIR_CHARS_LM = os.path.join(DIR,'output-compare-chars-lm') # chars -compare with- LM
OUTDIR_CONV_LM = os.path.join(DIR,'output-compare-conv-lm') # conv pair -compare with- LM
OUTDIR_CHARS_LMLIWC = os.path.join(DIR,'output-compare-chars-lmliwc') # chars -compare with- LIWC-tagged LM
OUTDIR_CONV_LMLIWC = os.path.join(DIR,'output-compare-conv-lmliwc') # conv pair -compare with- LIWC-tagged LM
OUTDIR_CHARS_MAIN_OTHERS_LM = os.path.join(DIR,'output-compare-chars-main-and-others-lm')
OUTDIR_CHARS_MAIN_OTHERS_LMLIWC = os.path.join(DIR,'output-compare-chars-main-and-others-lmliwc')

# used to compare utt zmoodel created with other characters' utt
OUTDIR_CHARS_MAIN_OTHERS2_LM = os.path.join(DIR,'output-compare-chars-main-and-others2-lm')
OUTDIR_CHARS_MAIN_OTHERS2_LMLIWC = os.path.join(DIR,'output-compare-chars-main-and-others2-lmliwc')


FOLDS = 5

#/////////////////////////////////////////////////////////////////////

def compare(n_z, n_lm, n_ideal):
	# compare 
	print 'corr n_z  to n_lm:\t', pearsonr(n_z, n_lm)
	print 'corr n_z  to ideal:\t', pearsonr(n_z, n_ideal)
	print 'corr n_lm to ideal:\t', pearsonr(n_lm, n_ideal)
	print 'kl   n_z  to n_lm:\t', entropy(n_z, n_lm) # larger val == larger divergence
	print 'kl   n_z  to ideal:\t', entropy(n_z, n_ideal) # larger val == larger divergence
	print 'kl   n_lm to ideal:\t', entropy(n_lm, n_ideal) # larger val == larger divergence

	# distance: correlation, euclidean, manhattan, chebyshev
	print 'cos  n_z  to ideal:\t', dist.cosine(n_z, n_ideal)
	print 'cos  n_lm to ideal:\t', dist.cosine(n_lm, n_ideal)
	print 'cor  n_z  to ideal:\t', dist.correlation(n_z, n_ideal)
	print 'cor  n_lm to ideal:\t', dist.correlation(n_lm, n_ideal)
	print 'euc  n_z  to ideal:\t', dist.euclidean(n_z, n_ideal)
	print 'euc  n_lm to ideal:\t', dist.euclidean(n_lm, n_ideal)
	print 'che  n_z  to ideal:\t', dist.chebyshev(n_z, n_ideal)
	print 'che  n_lm to ideal:\t', dist.chebyshev(n_lm, n_ideal)
	print 'man  n_z  to ideal:\t', dist.cityblock(n_z, n_ideal)
	print 'man  n_lm to ideal:\t', dist.cityblock(n_lm, n_ideal)


def compare_chisquare(n_z, n_lm, n_ideal):

	n_lm_fill = []
	for v in n_lm:
		if v: n_lm_fill.append(v)
		else: n_lm_fill.append(1)

	n_z_fill = []
	for v in n_z:
		if v: n_z_fill.append(v)
		else: n_z_fill.append(1)

	n_ideal_fill = []
	for v in n_ideal:
		if v: n_ideal_fill.append(v)
		else: n_ideal_fill.append(1)

	print
	print 'n_lm:     \t', n_lm
	print 'n_lm_fill:\t', n_lm_fill
	print 'n_z:      \t', n_z
	print 'n_z_fill: \t', n_z_fill
	print 'n_ideal:  \t', n_ideal
	print 'n_ideal_fill:', n_ideal_fill

	print 'chisquare n_ideal_fill to n_ideal_fill:', chisquare(n_ideal_fill, n_ideal_fill)
	print 'chisquare n_z to uniform:', chisquare(n_z)
	print 'chisquare n_lm to uniform:', chisquare(n_lm)
	print 'chisquare n_z to ideal_fill:', chisquare(n_z, n_ideal_fill)
	print 'chisquare n_lm to ideal_fill:', chisquare(n_lm, n_ideal_fill)
	print 'chisquare n_z_fill to n_lm_fill:', chisquare(n_z_fill, n_lm_fill)
	print 'chisquare n_lm_fill to n_z_fill:', chisquare(n_lm_fill, n_z_fill)
	print


def plot(OUTDIR, bins, lm_or_z, mtype, ranked_sents):
	data = ranked_sents['data']
	rank_list = ranked_sents['rank_list']
	match_cnt_list = ranked_sents['match_cnt_list']

	num_sents = len(data)
	title = lm_or_z+'-'+mtype+'-cnt-'+str(num_sents)
	f = os.path.join(OUTDIR, 'fig-'+title+'.png')

	plot_and_fit_regular(rank_list, match_cnt_list, f, title)
	plot_and_fit_hist(data, bins, re.sub('-cnt-', '-hist-', f), title)


def plot2(OUTDIR, bins, mtype, ranked_sents, ranked_sents_lm):
	data_z = ranked_sents['data']
	data_lm = ranked_sents_lm['data']
	print 'len data_z, data_lm:', len(data_z), len(data_lm)

	#rank_list_z = ranked_sents['rank_list']
	#rank_list_lm = ranked_sents_lm['rank_list']

	#match_cnt_list_z = ranked_sents['match_cnt_list']
	#match_cnt_list_lm = ranked_sents_lm['match_cnt_list']

	num_sents_z = len(data_z)
	num_sents_lm = len(data_lm)
	title = 'both-'+mtype+'-hist-'+str(num_sents_z)+'-'+str(num_sents_lm)
	f = os.path.join(OUTDIR, 'fig-'+title+'.png')
	plot_and_fit_hist2(data_z, data_lm, bins, f, title)


def get_bins(max_rank, num_bins):
	# use same bins distributions
	print '\nmax rank:', max_rank
	if max_rank < num_bins:
		return max_rank

	bins = range(0,max_rank)[::max_rank/num_bins]
	#bins = bins[1:]
	dif = bins[-1]-bins[-2]
	bins += [bins[-1]+dif] # add dif to last entry
	print 'bins:', len(bins), bins
	return bins


def get_info(OUTDIR, mtype, num_bins, mm_distr, mm_distr_lm, \
		spkr_addr_list, spkr_addr_list_lm):

	# find bins
	max_rank = 0
	for c in mm_distr_lm:
		max_rank = max(max_rank, max(mm_distr_lm[c].keys()))
	for c in mm_distr:
		#print c, mm_distr[c].keys()
		max_rank = max(max_rank, max(mm_distr[c].keys()))

	bins = get_bins(max_rank, num_bins)

	char_vec = defaultdict(dict)

	print '\nLM model match distr:', mm_distr_lm.keys()
	total = 0 
	for c in mm_distr_lm:
		data = mm_distr_lm[c]
		total += sum(data.values())
		# convert counter to vector
		vec = []
		for r in data:
			vec += [r]*data[r]

		# clean file name
		if 'liwctagged' in c:
			c_name = c.split('.liwctagged')[0].strip()
			if c_name[-1]=='.': c_name = c_name[:-1]
		else:
			c_name = c.split('.onesentperline')[0].strip()

		# e.g., -char-sheldon-inpresenceof-leonard
		m = re.match('-char-(.+)-inpresenceof-(.+)',c_name)
		if m: 
			c_name = m.group(1).strip()+'-'+m.group(2).strip()

		print c_name, sum(data.values())

		title = 'lm-'+c_name+'-'+mtype+'-'+str(sum(data.values()))
		f = os.path.join(OUTDIR, 'mmatch-distr-'+title+'.png')
		plot_and_fit_hist(vec, bins, f, title)	
		# store vec
		char_vec[c_name]['lm']=vec
		char_vec[c_name]['lm-num-utt']=sum(data.values())
	print '\tTOTAL:', total

	
	print '\nZ model match distr:', mm_distr.keys()
	total = 0
	for c in mm_distr:
		data = mm_distr[c]
		total += sum(data.values())
		# convert counter to vector
		vec = []
		for r in data:
			vec += [r]*data[r]

		# clean up file name
		# e.g., leonard==sheldon-
		c_name = re.sub('==','-',c)
		if c_name[-1]=='-': c_name = c_name[:-1]
		
		print c_name, sum(data.values())

		title = 'z-'+c_name+'-'+mtype+'-'+str(sum(data.values()))
		f = os.path.join(OUTDIR, 'mmatch-distr-'+title+'.png')
		plot_and_fit_hist(vec, bins, f, title)	
		# store vec
		char_vec[c_name]['z2']=vec
		char_vec[c_name]['z2-num-utt']=sum(data.values())
	print '\tTOTAL:', total
	print

	# plot both LM and Z model for each character
	print 'all chars:', char_vec.keys()
	print
	for c in char_vec:

		print '\nplotting:', c

		if 'lm' not in char_vec[c]:
			print 'ERROR. LM not found for char:', c
			continue

		lm_vec = char_vec[c]['lm']
		lm_num_utt = char_vec[c]['lm-num-utt']

		if 'z2' not in char_vec[c]:
			print '\nNOTE. z2 not found for', c, ': no plot\n'
			continue
		
		z_vec = char_vec[c]['z2']
		z_num_utt = char_vec[c]['z2-num-utt']

		title = 'both-'+c+'-'+mtype+'-'+str(z_num_utt)+'-'+str(lm_num_utt)
		f = os.path.join(OUTDIR, 'mmatch-distr-'+title+'.png')
		plot_and_fit_hist2(z_vec, lm_vec, bins, f, title)



def run_compare(OUTDIR, num_bins, mtype, ranks_sents, ranks_sents_lm):
	#print ranks_sents.keys()
	#print ranks_sents_lm.keys()

	print '\n----> processing:', mtype

	# before plotting, look at the distr of test set and predicted results
	print '----> debug:', ranks_sents.keys()
	print '----> debug:', ranks_sents_lm.keys()

	if 'match_model_distr' in ranks_sents and \
		'match_model_distr' in ranks_sents_lm:

		mm_distr = ranks_sents['match_model_distr']
		mm_distr_lm = ranks_sents_lm['match_model_distr']
		spkr_addr_list = ranks_sents['spkr_addr_list']
		spkr_addr_list_lm = ranks_sents_lm['spkr_addr_list']
		get_info(OUTDIR, mtype, num_bins, mm_distr, mm_distr_lm, \
					spkr_addr_list, spkr_addr_list_lm)

	plt.clf()
	plt.cla()

	# combine ranks
	rank_list = ranks_sents['rank_list']
	rank_list_lm = ranks_sents_lm['rank_list']
	rank_list_combined = list(set(rank_list + rank_list_lm))

	if not rank_list_combined:
		print 'No data; no comparison.'
		return

	if not rank_list:
		print 'n_z: no data; no comparison'
		return

	if not rank_list_lm:
		print 'n_lm: no data; no comparison'
		return

	# use same bins distributions
	max_rank = max(rank_list_combined)
	bins = get_bins(max_rank, num_bins)
	"""
	print 'max rank:', max_rank
	bins = range(0,max_rank)[::max_rank/num_bins]
	#bins = bins[1:]
	dif = bins[-1]-bins[-2]
	bins += [bins[-1]+dif] # add dif to last entry
	print 'bins:', len(bins), bins
	"""

	# hist for n_z
	data_z = ranks_sents['data']
	match_cnt_list = ranks_sents['match_cnt_list']
	dict_z = dict(zip(rank_list, match_cnt_list))
	n_z, bins_t, patches = plt.hist(data_z, bins=bins, normed=False)
	print 'n_z:', len(n_z), n_z, sum(n_z)
	plot(OUTDIR, bins, 'z', mtype, ranks_sents)

	# hist for n_lm
	data_lm = ranks_sents_lm['data']
	match_cnt_list_lm = ranks_sents_lm['match_cnt_list']	
	dict_lm = dict(zip(rank_list_lm, match_cnt_list_lm))
	n_lm, bins_t, patches = plt.hist(data_lm, bins=bins, normed=False)
	print 'n_lm:', len(n_lm), n_lm, sum(n_lm)
	plot(OUTDIR, bins, 'lm', mtype, ranks_sents_lm)
	#print patches
	#plt.show()

	# hist for both n_z and n_lm
	plot2(OUTDIR, bins, mtype, ranks_sents, ranks_sents_lm)

	# ideal histogram: all 0's (i.e, all in first slot)
	n_ideal = [sum(n_z)] + [0]*(len(n_z)-1)
	#n_ideal = [0] + [sum(n_z)] + [0]*(len(n_z)-2)
	#n_ideal = [sum(n_z)/len(n_z)] * len(n_z)
	print 'n_ideal:', len(n_ideal), n_ideal, sum(n_ideal)

	# compare 
	#print '\nchisquare:', chisquare(n_z[1:-1], n_lm[1:-1])
	compare_chisquare(n_z, n_lm, n_ideal)
	compare(n_z, n_lm, n_ideal)

	# compare normal distr fit of data
	(mu_z, sigma_z) = norm.fit(data_z)
	(mu_lm, sigma_lm) = norm.fit(data_lm)
	print '\nmu_z, sigma_z:', mu_z, sigma_z
	print 'mu_lm, sigma_lm:', mu_lm, sigma_lm
	mu_ideal = 1
	sigma_ideal = 1
	print 'mu_idea, sigma_ideal:', mu_ideal, sigma_ideal

	x_z = np.random.normal(mu_z,sigma_z,5000)
	x_lm = np.random.normal(mu_lm, sigma_lm,5000)
	x_ideal = np.random.normal(mu_ideal, sigma_ideal,5000)
	print 'ks_2samp x_z to x_lm:', ks_2samp(x_z, x_lm)
	print 'ks_2samp x_z to x_ideal:', ks_2samp(x_z, x_ideal)
	print 'ks_2samp x_lm to x_ideal:', ks_2samp(x_lm, x_ideal)

	"""
	print 'processing:', mtype
	print len(rank_list), rank_list
	print len(rank_list_lm), rank_list_lm
	print len(rank_list_combined), rank_list_combined
	"""

	cnt_z = []
	cnt_lm = []

	#for rank in rank_list_combined:
	for rank in range(max_rank+1):
		v_z = float(dict_z[rank]) if rank in dict_z else 0.
		cnt_z.append(v_z)
		
		v_lm = float(dict_lm[rank]) if rank in dict_lm else 0.
		cnt_lm.append(v_lm)

	print '\ncnt_z:', len(cnt_z), cnt_z[:5], '...', sum(cnt_z)
	print 'cnt_lm:', len(cnt_lm), cnt_lm[:5], '...', sum(cnt_lm)

	cnt_ideal = [sum(cnt_z)]+[0]*(len(cnt_z)-1)
	print 'cnt_ideal:', len(cnt_ideal), cnt_ideal[:5], '...', sum(cnt_ideal)

	# compare 
	compare(cnt_z, cnt_lm, cnt_ideal)


def run(OUTDIR, run_list, num_bins):
	
	for (mtype, zpickle, lmpickle) in run_list:

		if not os.path.isfile(zpickle) or not os.path.isfile(lmpickle):
			print mtype,': one or more pickle files do not exist. Skip.'
			print '\t',os.path.isfile(zpickle), ':', zpickle
			print '\t',os.path.isfile(lmpickle), ':', lmpickle
			continue

		ranks_sents = pickle.load(open(zpickle))
		ranks_sents_lm = pickle.load(open(lmpickle))
		# redirect output
		sys.stdout = open(os.path.join(OUTDIR,'log.compare.'+mtype+'.txt'), 'w') # redirect
		run_compare(OUTDIR, num_bins, mtype, ranks_sents, ranks_sents_lm)
		# reset output
		sys.stdout = sys.__stdout__



####
#### Main
####
if __name__ == "__main__":
	
	num_bins = 20

	#### compare char zmodel with lm
	"""
	run_list = []
	for a in ['chars']:
		for f in range(FOLDS):
			for z in range(3): # zmodel
				run_list.append((
					a+'-zmodel'+str(z)+'-'+str(f),
					os.path.join(RANK_Z_DIR, \
						'rank-sents-z-'+a+'-zmodel'+str(z)+'-'+str(f)+'.pickle'),
					os.path.join(RANK_LM_DIR, \
						'rank-sents-lm-'+a+'-'+str(f)+'.pickle')
					)
				)
	run(OUTDIR_CHARS_LM, run_list, num_bins)
	"""
	
	#### compare conv zmodel with lm
	
	run_list2 = []
	for a in ['conv']:
		for f in range(FOLDS):
			for z in range(3): # zmodel
				run_list2.append((
					a+'-zmodel'+str(z)+'-'+str(f),
					os.path.join(RANK_Z_DIR, \
						'rank-sents-z-'+a+'-zmodel'+str(z)+'-'+str(f)+'.pickle'),
					os.path.join(RANK_LM_DIR, \
						'rank-sents-lm-'+a+'-'+str(f)+'.pickle')
					)
				)
	run(OUTDIR_CONV_LM, run_list2, num_bins)
	
	

	
	### compare char zmodel with lmliwc
	"""
	run_list3 = []
	for a in ['chars']:
		for f in range(FOLDS):
			for z in range(3): # zmodel
				run_list3.append((
					'lmliwc-'+a+'-zmodel'+str(z)+'-'+str(f),
					os.path.join(RANK_Z_DIR, \
						'rank-sents-z-'+a+'-zmodel'+str(z)+'-'+str(f)+'.pickle'),
					os.path.join(RANK_LM_DIR, \
						'rank-sents-lmliwc-'+a+'-'+str(f)+'.pickle')
					)
				)
	run(OUTDIR_CHARS_LMLIWC, run_list3, num_bins)
	"""
	

	#### compare conv zmodel with lmliwc
	
	run_list4 = []
	for a in ['conv']:
		for f in range(FOLDS):
			for z in range(3): # zmodel
				run_list4.append((
					'lmliwc-'+a+'-zmodel'+str(z)+'-'+str(f),
					os.path.join(RANK_Z_DIR, \
						'rank-sents-z-'+a+'-zmodel'+str(z)+'-'+str(f)+'.pickle'),
					os.path.join(RANK_LM_DIR, \
						'rank-sents-lmliwc-'+a+'-'+str(f)+'.pickle')
					)
				)
	run(OUTDIR_CONV_LMLIWC, run_list4, num_bins)
	

	#### compare chars main and others with lm
	"""
	run_list5 = []
	for a in ['chars']:
		for f in range(FOLDS):
			for z in range(3): # zmodel
				run_list5.append((
					a+'-main-and-others-zmodel'+str(z)+'-'+str(f),
					os.path.join(RANK_Z_DIR, \
						'rank-sents-z-'+a+'-main-and-others-zmodel'+str(z)+'-'+str(f)+'.pickle'),
					os.path.join(RANK_LM_DIR, \
						'rank-sents-lm-'+a+'-main-and-others-'+str(f)+'.pickle')
					)
				)
	run(OUTDIR_CHARS_MAIN_OTHERS_LM, run_list5, num_bins)
	"""

	#### compare chars main and others with lmliwc
	"""
	run_list6 = []
	for a in ['chars']:
		for f in range(FOLDS):
			for z in range(3): # zmodel
				run_list6.append((
					'lmliwc-'+a+'-main-and-others-zmodel'+str(z)+'-'+str(f),
					os.path.join(RANK_Z_DIR, \
						'rank-sents-z-'+a+'-main-and-others-zmodel'+str(z)+'-'+str(f)+'.pickle'),
					os.path.join(RANK_LM_DIR, \
						'rank-sents-lmliwc-'+a+'-main-and-others-'+str(f)+'.pickle')
					)
				)
	run(OUTDIR_CHARS_MAIN_OTHERS_LMLIWC, run_list6, num_bins)
	"""

	#### compare chars main and others 2 
	#### (utt z model created by including others' utt) with lm
	"""
	run_list7 = []
	for a in ['chars']:
		for f in range(FOLDS):
			for z in range(3): # zmodel
				run_list7.append((
					a+'-main-and-others2-zmodel'+str(z)+'-'+str(f),
					os.path.join(RANK_Z_DIR, \
						'rank-sents-z-'+a+'-main-and-others2-zmodel'+str(z)+'-'+str(f)+'.pickle'),
					os.path.join(RANK_LM_DIR, \
						'rank-sents-lm-'+a+'-main-and-others-'+str(f)+'.pickle')
					)
				)
	run(OUTDIR_CHARS_MAIN_OTHERS2_LM, run_list7, num_bins)
	"""

	#### compare chars main and others 2 
	#### (utt z model created by including others' utt) with lmliwc
	"""
	run_list8 = []
	for a in ['chars']:
		for f in range(FOLDS):
			for z in range(3): # zmodel
				run_list8.append((
					'lmliwc-'+a+'-main-and-others2-zmodel'+str(z)+'-'+str(f),
					os.path.join(RANK_Z_DIR, \
						'rank-sents-z-'+a+'-main-and-others2-zmodel'+str(z)+'-'+str(f)+'.pickle'),
					os.path.join(RANK_LM_DIR, \
						'rank-sents-lmliwc-'+a+'-main-and-others-'+str(f)+'.pickle')
					)
				)
	run(OUTDIR_CHARS_MAIN_OTHERS2_LMLIWC, run_list8, num_bins)
	"""


	