from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Rank sentences.
---------------------------------------------------------------
"""

import sys, os, re, copy, subprocess, pickle
from collections import defaultdict, Counter
from scipy.spatial import distance
import math

#/////////////////////////////////////////////////////////////////////

SRILMDIR = '/Volumes/hhd/Dialogue/srilm171/bin/macosx/'

RSCDIR = os.path.join('..','..','..','resources')
sys.path.append(RSCDIR)
from common import create_directory

CHAR_TRAIN_TEST_ZMODEL_DIR = 'output-tables'
CHAR_TEST_SENTS_ZMODEL_DIR = 'output-tables-utt-test'
# utt zmodel contains other chars' utt; look at main only
CHAR_TEST_SENTS2_ZMODEL_DIR = 'output-tables-utt-test-and-others'

CHAR_TRAIN_MAIN_AND_OTHERS_ZMODEL_DIR = 'output-tables-main-and-others'

CONV_TRAIN_TEST_ZMODEL_DIR = 'output-tables-multi'
CONV_TEST_SENTS_ZMODEL_DIR = 'output-tables-multi-utt-test'


OUTDIR = 'output-rank-sents'
create_directory(OUTDIR)

FOLDS = 5

#/////////////////////////////////////////////////////////////////////


def remove_entries(entries_dict, remove=['ngram2','rep-vb-','rep-pvb-']):
	# original removal list: remove=['ngram2','word-','rep-vb-','rep-pvb-']
	keys = entries_dict.keys()
	for key in keys:
		for rm in remove:
			if rm in key:
				del entries_dict[key]

def fill_entries_with_zero(entries_dict, desired_keys):
	for k in desired_keys:
		if k not in entries_dict:
			entries_dict[k]=0


def get_zmodel(f):
	lines = f.read().split('\n')
	header_arr = lines[0].strip().split('|')

	zmodel_zscore_train = []
	# save average and standard deviation
	avg = {}
	stdev = {}

	for line in lines[1:]:
		#print line[:50], '...'
		if '_mean' in line:
			arr = line.strip().split('|')
			avg = dict(zip(header_arr, arr))
			remove_entries(avg)
		elif '_stdev' in line:
			arr = line.strip().split('|')
			stdev = dict(zip(header_arr, arr))
			remove_entries(stdev)
		else:
			arr = line.strip().split('|')
			entry = dict(zip(header_arr, arr))
			# TODO: found an entry with 'speaker' as key?
			if len(entry)==1: continue
			remove_entries(entry)
			zmodel_zscore_train.append(entry)

	return avg, stdev, zmodel_zscore_train



def get_train_info(train_type, fold):
	if train_type=='chars':
		f = open(os.path.join(CHAR_TRAIN_TEST_ZMODEL_DIR, \
				'gen-cv-chars-default-table-'+str(fold)+'-zvalues.txt'))
	elif train_type=='chars-main-and-others' or train_type=='chars-main-and-others2':
		f = open(os.path.join(CHAR_TRAIN_MAIN_AND_OTHERS_ZMODEL_DIR, \
				'gen-cv-chars-main-and-others-default-table-'+str(fold)+'-zvalues.txt'))
	elif train_type=='conv':
		f = open(os.path.join(CONV_TRAIN_TEST_ZMODEL_DIR, \
				'gen-cv-conv-multi-all-pair-main-table-'+str(fold)+'-zvalues.txt'))
	else:
		print 'ERROR. Unknown train type:', train_type
		return

	# get zmodel
	avg, stdev, zmodel_zscore_train = get_zmodel(f)
	"""
	lines = f.read().split('\n')
	header_arr = lines[0].strip().split('|')

	zmodel_zscore_train = []
	# save average and standard deviation
	avg = {}
	stdev = {}

	for line in lines[1:]:
		#print line[:50], '...'
		if '_mean' in line:
			arr = line.strip().split('|')
			avg = dict(zip(header_arr, arr))
			remove_entries(avg)
		elif '_stdev' in line:
			arr = line.strip().split('|')
			stdev = dict(zip(header_arr, arr))
			remove_entries(stdev)
		else:
			arr = line.strip().split('|')
			entry = dict(zip(header_arr, arr))
			# TODO: found an entry with 'speaker' as key?
			if len(entry)==1: continue
			remove_entries(entry)
			zmodel_zscore_train.append(entry)
	"""

	print 'Num of trained z-models:', len(zmodel_zscore_train)

	for sent in zmodel_zscore_train[:4]:
		print '\t', sent['speaker'], len(sent)
	print '\t ...'
	for sent in zmodel_zscore_train[-4:]:
		print '\t', sent['speaker'], len(sent)

	return avg, stdev, zmodel_zscore_train



def get_sents(fname, desired_feats):
	lines = open(fname).read().split('\n')
	header_arr = lines[0].strip().split('|')

	# becomes a list of dict below
	sents = []

	for line in lines[1:]: # avoid header
		# addressee has series/episode info
		# e.g., series 1, scene 1, utterances
		arr = line.strip().split('|')
		entry = dict(zip(header_arr, arr))
		# keep only the desired features
		if desired_feats:
			entry_keys = entry.keys()
			for k in entry_keys:
				if 'speaker' in k or 'addressee' in k: continue
				if k not in desired_feats:
					del entry[k]
			# fill entries
			fill_entries_with_zero(entry,desired_feats)
		#print len(entry), len(desired_feats)
		sents.append(entry)

	return sents


def get_test_sents(test_type, fold, desired_feats=None):

	if test_type=='chars' or test_type=='chars-main-and-others':
		fname = os.path.join(CHAR_TEST_SENTS_ZMODEL_DIR, \
				'gen-utt-chars-default-table'+str(fold)+'-zvalues.txt')
	elif test_type=='chars-main-and-others2':
		fname = os.path.join(CHAR_TEST_SENTS2_ZMODEL_DIR, \
				'gen-utt-chars-test-and-others-default-table'+str(fold)+'-zvalues-main-only.txt')
	elif test_type=='conv':
		fname = os.path.join(CONV_TEST_SENTS_ZMODEL_DIR, \
				'gen-utt-conv-multi-all-pair-main-table'+str(fold)+'-zvalues.txt')
	else:
		print 'ERROR. Unknown train type:', test_type
		return False

	if not os.path.isfile(fname):
		print 'ERROR. File not found:',fname
		return False

	sents = get_sents(fname, desired_feats)
	return sents


def run(ref_type, fold, zmodel_type):

	# get trained sets z-scored results
	avg, stdev, trained_zmodel = get_train_info(ref_type, fold)
	print 'trained models:', len(trained_zmodel)
	
	# get test sents
	sents = get_test_sents(ref_type, fold, avg.keys())
	if not sents:
		print 'ERROR. No test sents.'
		return False, False, False, False, False

	print 'test sents:', len(sents)
	print
	print 'avg feats:', len(avg.keys()), sorted(avg.keys())[:5], '...'
	print 'sent feats:', len(sents[0].keys()), sorted(sents[0].keys())[:5], '...'

	sents_zscore = copy.deepcopy(sents)

	# go through each test sent
	for i, sent in enumerate(sents): 
		# find this sent's zscores
		for feat in sent.keys():
			if 'speaker' in feat or 'addressee' in feat:
				continue

			if feat in avg.keys(): # see if the feature is found in avg model
				# TODO: polarity-overall has 'nan'?
				if float(stdev[feat])==0 or math.isnan(float(sent[feat])):
					zscore = 0
				else:
					zscore = (float(sent[feat])-float(avg[feat]))/float(stdev[feat])
				sents_zscore[i][feat] = zscore
			else:
				sents_zscore[i][feat] = 'N/A'

	#print
	#for sent in sents_zscore:
	#	print 'spk/addr:', sent['speaker'], '\t', 'feats:', len(sent)

	#exit()

	# go through each sent's zscore and find cosine similarity with respect to 
	# each zmodel
	match_list = []
	match_count = Counter()
	match_model = defaultdict(Counter)
	spkr_addr_list = []

	for i, sent in enumerate(sents_zscore):

		if sent['speaker'] in ['_mean','_stdev']: continue

		print '\nsent', i
		print 'spk:',sent['speaker']
		#print 'addr:',sent['addressee']

		# what to match
		# e.g., sheldon==penny-140
		match = sent['speaker']

		pair = match #re.sub('==','-', match)
		spkr_addr_list.append(pair)

		cosine_rank = defaultdict(list) # use list in case overlap
		feats_not_found_in_ref = Counter()

		# compare each sent with each trained zmodel
		# trained_zmodel is a list of entries
		for j, zmodel in enumerate(trained_zmodel):
			#print 'type, feats:', type(zmodel), len(zmodel)

			# zmodel 
			zmodel1 = [] # contains zscore >=1
			zmodel2 = [] # contains zscore >=2

			for k in zmodel.keys():
				if 'speaker' in k: continue
				v = abs(float(zmodel[k])) 
				if v>=1:
					zmodel1.append(k)
					if v>=2:
						zmodel2.append(k)

			feats_list = []
			sent_zscore_list = []
			zmodel_zscore_list = []
			feats_not_found_in_zmodel = Counter()

			for f in sent:
				#print f, sent[f]
				if 'speaker' in f: continue
				if sent[f] == 'N/A': continue

				if f not in zmodel.keys():
					# TODO: is this right?
					feats_not_found_in_zmodel[f]+=1
					print '----> feat not found in zmodel:', f
					print zmodel.keys()
					continue

				#print len(sent), len(zmodel)

				# use a particular zmodel
				# TODO: instead of using zmodel2, use ref_list's zscores
				if zmodel_type==0:
					# use all feats
					feats_list.append(f)
					sent_zscore_list.append((float(sent[f])))
					zmodel_zscore_list.append((float(zmodel[f])))
				else:
					# use features with zscore >=2 (zmodel2)
					# or zscore >= 1 (zmodel1)
					if zmodel_type==2:
						zmodel_c = zmodel2
					else:
						zmodel_c = zmodel1
					if f in zmodel_c:
						feats_list.append(f)
						sent_zscore_list.append((float(sent[f])))
						zmodel_zscore_list.append((float(zmodel[f])))
						#print i, j, f, float(sent[f]), float(zmodel[f])

			# skip if no feats found
			if not len(feats_list):
				continue

			#print 'num of feats:', len(feats_list)

			# cosine similarity
			#print 'ref sent', j, 'feat size:', len(sent_zscore_list), len(sent_zscore_ref_list)
			dist = distance.cosine(sent_zscore_list, zmodel_zscore_list)
			#dist = distance.euclidean(sent_zscore_list, sent_zscore_ref_list)
			#dist = distance.correlation(sent_zscore_list, sent_zscore_ref_list)
			dist_round = round(dist, 4)
			#print dist_round

			keyidx = 'speaker'
			sp_addr = zmodel[keyidx]

			if sp_addr in match:
				mstr = 'MATCHED '+sp_addr
			else:
				mstr = 'NOT MATCHED'

			tostr = '\tref:'+' '+zmodel[keyidx][0:50]+ '... '+ \
					str(dist_round)+' '+mstr #+' ['+ff.readline().strip()[0:20]+' ...'
			
			cosine_rank[dist_round].append(tostr)
			#ff.close()

		print 'feats not found in ref:', len(feats_not_found_in_ref), \
				feats_not_found_in_ref.keys()

		print '\tcosine vals:', sorted(cosine_rank.keys())[0:5], '...'
		print '\tneed to match:', match

		match_found = False

		for z, cosine_val in enumerate(sorted(cosine_rank)):
			arr = cosine_rank[cosine_val]
			#print '\tcosine', cosine_val,'has number of items:', len(arr)

			for txt in arr:
				#print '\trank', z, 'cosine', cosine_val, txt

				if 'NOT MATCHED' not in txt:

					if z==0: # smallest val (largest similarity)
						if not match_found:
							print '\trank', z, 'cosine', cosine_val, txt,
							print '* (smallest item) (MATCH)'
							match_count[z]+=1
							match_found=True
							# add matched model
							spkr_addr = txt.split('MATCHED')[1].strip()	
							match_model[spkr_addr][z]+=1

						match_list.append(sent['speaker'])
						#if ref_type=='chars':
						#	match_list.append(sent['speaker'])
						#else:
						#	match_list.append(sent['speaker']+'=='+sent['addressee'])

					# found match
					# count first one only
					if not match_found:
						print '\trank', z, 'cosine', cosine_val, txt
						match_count[z]+=1
						match_found=True
						# add matched model
						spkr_addr = txt.split('MATCHED')[1].strip()	
						match_model[spkr_addr][z]+=1		
		
		# get smallest item
		#k = sorted(cosine_rank.keys())[0]
		#print '\tsmallest:', k, cosine_rank[k]
		"""
		for item in cosine_rank[k]:
			if 'NOT MATCHED' not in item:
				match_count += 1
				if ref_type=='chars':
					match_list.append(sent['speaker'])
				else:
					match_list.append(sent['speaker']+'=='+sent['addressee'])
				break
		"""

	data = []
	rank_list = []
	match_cnt_list = []

	# sentences
	print '\nsents:', len(sents_zscore)
	print 'match list (rank 0):', len(match_list)
	print 'match distr:'
	for rank in sorted(match_count):
		acc = match_count[rank] / len(sents_zscore)
		print '\trank:', rank, 'match count:', match_count[rank], '(',acc,')'
		# keep track for plots later
		rank_list.append(rank)
		match_cnt_list.append(match_count[rank])
		data += [rank]*match_count[rank]


	return data, rank_list, match_cnt_list, match_model, spkr_addr_list



def main_run(fold):

	for zmodel_type in range(3):

		ref_pairs = [
			#('chars', 'rank-sents-z-chars-zmodel'+str(zmodel_type)+'-'+str(fold)+'.pickle'),
			#('chars-main-and-others', 'rank-sents-z-chars-main-and-others-zmodel'+str(zmodel_type)+'-'+str(fold)+'.pickle'),
			('chars-main-and-others2', 'rank-sents-z-chars-main-and-others2-zmodel'+str(zmodel_type)+'-'+str(fold)+'.pickle'),
			#('conv',  'rank-sents-z-conv-zmodel'+str(zmodel_type)+'-'+str(fold)+'.pickle'),
		]

		for (curr_ref_type, pickle_fname) in ref_pairs:

			# data to be stored as pickle file
			data_store = {}

			print '\n----> data type', curr_ref_type,
			print 'zmodel', zmodel_type,
			print 'fold', fold

			data, rank_list, match_cnt_list, match_model, spkr_addr_list = \
				run(curr_ref_type, fold, zmodel_type)

			# store data
			data_store['data']=data
			data_store['rank_list']=rank_list
			data_store['match_cnt_list']=match_cnt_list
			data_store['match_model_distr']=match_model
			data_store['spkr_addr_list']=spkr_addr_list

			# store data as picke file
			pickle.dump(data_store, open(os.path.join(OUTDIR,pickle_fname), 'w'))

####
#### Main
####
if __name__ == "__main__":
	for fold in range(FOLDS):
		main_run(fold)



