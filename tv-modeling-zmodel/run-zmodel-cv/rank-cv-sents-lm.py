from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Rank sentences based on LM
---------------------------------------------------------------
"""

import sys, os, re, copy, subprocess, timeit, pickle
from collections import defaultdict, Counter
from scipy.spatial import distance
import nltk

#from common import plot_and_fit_regular, plot_and_fit_hist

#/////////////////////////////////////////////////////////////////////
RSCDIR = os.path.join('..','..','..','resources')
sys.path.append(RSCDIR)
from common import create_directory

SRILMDIR = '/Volumes/hhd/Dialogue/srilm171/bin/macosx/'

# testing sents
CHAR_TEST_SENTS_DIR = os.path.join('..','..','..','tv-modeling-features', \
	'bbt','gen-features-cv', 'chars-input-cv')
CONV_TEST_SENTS_DIR = os.path.join('..','..','..','tv-modeling-conv-multi', \
	'bbt','gen-conv-multi-cv','conv-input-all-pair-main-cv')

# trained LM models (files with extension ".bo")
GEN_LM_DIR = os.path.join('..','..','..','tv-modeling-lm','bbt','gen-lm-cv')
CHAR_LM_DEFAULT_INDIR = os.path.join(GEN_LM_DIR, 'chars', 'chars-output-lm')
CHAR_LM_LIWC_TAGGED_DEFAULT_INDIR = os.path.join(GEN_LM_DIR, 'chars-liwc-tagged')
CONV_LM_DEFAULT_INDIR = os.path.join(GEN_LM_DIR,'conv','conv-output-lm-main-pairs')
CONV_LM_LIWC_TAGGED_DEFAULT_INDIR = os.path.join(GEN_LM_DIR, 'conv-liwc-tagged')

# main and others
GEN_LM_MAIN_OTHERS_DIR = os.path.join('..','..','..','tv-modeling-lm','bbt','gen-lm-cv-main-and-others')
CHAR_LM_MAIN_OTHERS_DEFAULT_INDIR = os.path.join(GEN_LM_MAIN_OTHERS_DIR,'chars','chars-output-lm')
CHAR_LM_LIWC_MAIN_OTHERS_TAGGED_DEFAULT_INDIR = os.path.join(GEN_LM_MAIN_OTHERS_DIR, 'chars-liwc-tagged')

MAIN_CHARS = ['leonard','sheldon','penny', 'howard','raj','amy','bernadette']

LM_OUTDIR = 'output-lm'
LM_LIWC_OUTDIR = 'output-lm-liwc'

LM_MULTI_OUTDIR = 'output-lm-multi'
LM_LIWC_MULTI_OUTDIR = 'output-lm-multi-liwc'

LM_MAIN_OTHERS_OUTDIR = 'output-lm-main-and-others'
LM_LIWC_MAIN_OTHERS_OUTDIR = 'output-lm-liwc-main-and-others'

OUTDIR = 'output-rank-sents-lm'

FOLDS = 5

#/////////////////////////////////////////////////////////////////////

# sentence tokenizer
sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')


def parse_and_sort_results(results, outf, pplidx=0):
	# parse the results and sort and returns least amount of perplexity items
	# pplidx indicates which entry to retrieve, rather than, say, always 
	# retireve the first two entries

	# Results as three-line tuple:
	# 	<lm filename (.bo extension)>
	#	<actual sents file>
	#	<lm result stats>
	# For example:
	# 	howard-penny-series-4-episode-03-scene-02-the-cheesecake-factor.onesentperline.bo
	# 	file conv/test-input/amy-leonard-others.onesentperline.txt: 1 sentences, 7 words, 7 OOVs
	# 	0 zeroprobs, logprob= -0.748188 ppl= 5.6 ppl1= undefined
	#

	# look at perpelexity
	ppldict = defaultdict(list)

	for key in results.keys():
		arr = results[key].split('\n')

		line2 = arr[2].strip() # 3rd line contains the result stats
		#print line2
		
		m = re.match(r'.+ logprob=(.+) ppl=(.+) ppl1', line2)
		logprob = float(m.group(1))	# log prob
		
		# perplexity might be undefined
		if m.group(2).strip()=='undefined': continue

		ppl = float(m.group(2))	# perplexity

		ppldict[ppl].append(key)


	# sort and write to output
	min_perplexity = []

	if pplidx>=len(ppldict):
		return min_perplexity

	f = open(outf, 'w')
	for i, k in enumerate(sorted(ppldict.keys())):
		v = ppldict[k]
		# write to file
		for vi in v:
			#print results[vi]
			f.write(results[vi])
			# get some entries from least amount of perplexity
			if i==pplidx:
				r = results[vi]
				a = r.split('\n')
				min_perplexity.append(a[0])

	f.close()

	# return the first two entries (least amount of perplexity)
	"""
	arr = []
	for i in [0,1]:
		k = sorted(ppldict.keys())[i]
		v = ppldict[k]
		for vi in v:
			r = results[vi] # results
			a = r.split('\n')
			arr.append(a[0])
	"""
	#print min_perplexity
	return min_perplexity	



def get_scene_info(desc):
	#print desc
	season = 0; episode = 0; scene = 0

	if 'scene' in desc:
		m = re.match('^(\d)-episode-(\d\d)-scene-(\d\d)-', desc)
		season = m.group(1)
		episode = m.group(2)
		scene = m.group(3)
	elif 'episode' in desc:
		m = re.match('^(\d)-episode-(\d\d)', desc)
		season = m.group(1)
		episode = m.group(2)
	else:
		m = re.match('^(\d)-', desc)
		if m:
			season = m.group(1)

	return int(season), int(episode), int(scene)
		


#### 
#### Compare with pre-built langauge model.
#### e.g., this assumes gen-lm/conv/conv-output-lm-*/ exist.
####
def test_lm(speaker, utt, utt_num, LM_INDIR, LM_OUTDIR, \
			main_char_lm_only=True, debug=False):

	if 'conv' in LM_INDIR:
		isconv = True
	else:
		isconv = False

	# write sents to a file (with .sent.txt extension)
	sents_fpath = os.path.join(LM_OUTDIR,speaker+'.'+str(utt_num)+'.sent.txt')
	fout = open(sents_fpath, 'w')
	# each utt may have multiple sents
	sents = sent_tokenizer.tokenize(utt)
	#print sents			
	fout.write('\n'.join(sents)+'\n')
	fout.close()
	
	# clear lm results file as we need to append to it later
	lm_results_fpath = re.sub('.sent.txt', '.out.txt', sents_fpath)
	fout = open(lm_results_fpath, 'w')
	fout.write('')
	fout.close()

	# computes the perplexity on test sents according to prebuild LM
	lm_prebuilt = os.listdir(LM_INDIR)
	lm_results = {}

	# go through each prebuild LM and test on test sents
	for lmp in sorted(lm_prebuilt):
		# command example:
		# ngram -lm sheldon-leonard.bo -ppl test.txt
		if main_char_lm_only:
			char_lm = lmp.split('.onesentperline.bo')[0].strip()
			char_lm = re.sub('.liwctagged','',char_lm)
			#print char_lm

			if not isconv: # chars
				if main_char_lm_only and char_lm not in MAIN_CHARS: 
					continue
			else: #conv
				m = re.match('-char-(.+)-inpresenceof-(.+)',char_lm)
				spkr=m.group(1).strip()
				addr=m.group(2).strip()
				if main_char_lm_only and \
					(spkr not in MAIN_CHARS or addr not in MAIN_CHARS):
					continue
			
		cmd = [SRILMDIR+'ngram', '-lm', os.path.join(LM_INDIR,lmp), 
				'-ppl', sents_fpath]
		#print cmd
		p = ''
		p = subprocess.check_output(cmd)
		#print p
		lm_results[lmp]=lmp+'\n'+p 	# use name of the lm model as key
		
		# append results to output file
		fout = open(lm_results_fpath, 'a')
		fout.write(lm_results[lmp])
		fout.close()


	#### sort the result by log prob (perplexity)
	lm_results_fpath_sorted = re.sub('.txt', '.sorted.txt', lm_results_fpath)

	# find results that match with desired conversants ('pair') 
	#arr = pair.split('-series-')

	#if isconv:
	#	name_compare = arr[0]
	#else:
	#	name_compare = arr[0].split('-')[0]
	name_compare_target = speaker
	print '\ttarget:', name_compare_target
	
	#desc = arr[1]
	#season, episode, scene = get_scene_info(desc)
	#print 'season', season, 'episode', episode, 'scene', scene

	#if debug: print '\n\tlm name to compare:', name_compare

	chars_found = [] # keep track of spaker-addressee matches
	chars_rank = []
	chars_match_found = False

	items_found = [] # keep track of speaker-addressee, and season/episode/scene match
	items_rank = []
	items_match_found = False

	pplidx = 0 # retrieve by rank; 0 would be the smallest perplexity

	while not items_found:
		min_perplexity = parse_and_sort_results(lm_results, \
							lm_results_fpath_sorted, pplidx)

		#if debug:
		#	print '\tidx:', pplidx, '; items in min_perplexity:', \
		#			len(min_perplexity)

		# no more items...
		if not min_perplexity:
			break

		#print min_perplexity
		
		# there could be more than one LM producing the same perplexity with 
		# this sent
		for item in min_perplexity:
			# check if speaker-addressee match; 
			if isconv:
				name_compare2 = item[:len(name_compare_target)]
			else:
				# characters only (speaker only)
				name_compare2 = item.split('.onesentperline.')[0]
				# if it is in form of <char name>.liwctagged.onesentperline.bo
				# then remove ".liwctagged" as well
				name_compare2 = re.sub('.liwctagged','',name_compare2)

			#print '\tgot:', name_compare2, \
			#		name_compare_target == name_compare2

			if name_compare_target != name_compare2:
				continue

			if isconv and '-others' in item:
				continue
				
			# found match
			matched = True

			# keep first one only
			if not chars_match_found:
				chars_found.append(item)
				chars_rank.append(pplidx)
				chars_match_found = True
				if debug: 
					print '\tidx:', pplidx, 'chars match found:', item, \
						'(first found is stored)'
			#elif debug:
			#	print '\tidx:', pplidx, 'chars match found:', item

			if matched:
				if not items_match_found:
					items_found.append(item)
					items_rank.append(pplidx)
					items_match_found = True
					if debug: 
						print '\tidx:', pplidx, 'chars/scene match found:', item, \
							'(first found is stored)'
				elif debug:
					print '\tidx:', pplidx, 'chars/scene match found:', item

		pplidx+=1

	#if not found:
	#	print 'ERROR.', name_compare, 'did not match with least perplexity: ', ar
	#print 'name to compare:', name_compare, 'items found:', len(items_found)

	return items_found, items_rank, chars_found, chars_rank



def get_sents(curr_test_sent_dir):
	# dir has one file per character or conv pair
	#	e.g., amy.txt, bernadette.txt, ...
	files = os.listdir(curr_test_sent_dir)
	sents = defaultdict(list)

	for f in files:
		if f[0]=='.': continue

		speaker = f.split('.txt')[0].strip()
		f = open(os.path.join(curr_test_sent_dir,f)).read()
		lines = f.split('\n')
		for line in lines:
			line = line.strip()
			if not line: continue
			sents[speaker].append(line)
	return sents


def run(lm_dir, curr_test_sent_dir, curr_lm_outdir, main_char_lm_only=True):
	# get testing sentences; from 'test' directory
	# sents[speaker]=[sent1, sent2,...]
	utts = get_sents(curr_test_sent_dir)
	total_utts = 0

	print '\nNumber of speakers:', len(utts)
	for s in utts: 
		print '\t',s, 'sents', len(utts[s])
		total_utts += len(utts[s])

	spkr_addr_list = []

	chars_entries_not_found = Counter()
	chars_match_count = Counter()
	chars_match_model_distr = defaultdict(Counter)

	entries_not_found = Counter()
	match_count = Counter()
	match_model_distr = defaultdict(Counter)

	for speaker in utts: 
		
		#if speaker != 'howard': continue
		print '\nprocessing speaker:', speaker

		# Apply LMs to this sent and get the LM that gives least perplexity value.
		# Ideally the LM that gives least perplexcity should be the 1st one,
		# but in reality it could be the 2nd least or 3rd least, etc.

		#pair = entry['speaker']+'-'+entry['addressee']
		pair = speaker
		#spkr_addr_list.append(pair)
		for i, utt in enumerate(utts[speaker]):
			#if i < 593: continue
			print '\n\tutt number:', i

			lm_list_sorted, lm_rank_sorted, \
			chars_lm_list_sorted, chars_lm_rank_sorted = \
				test_lm(speaker, utt, i, lm_dir, curr_lm_outdir, 
					main_char_lm_only=main_char_lm_only, debug=True)

			# if no LM found then go to next sentence
			if not lm_list_sorted: 
				entries_not_found[pair]+=1
			else:
				for ii, rank in enumerate(lm_rank_sorted):
					match_count[rank]+=1
					# store by model name
					match_model_name = lm_list_sorted[ii]
					match_model_distr[match_model_name][rank]+=1

			# do the same for characters only
			if not chars_lm_list_sorted: 
				chars_entries_not_found[pair]+=1
			else:
				for ii, rank in enumerate(chars_lm_rank_sorted):
					chars_match_count[rank]+=1
					# store by model name
					match_model_name = chars_lm_list_sorted[ii]
					chars_match_model_distr[match_model_name][rank]+=1

	data = []
	rank_list = []
	match_cnt_list = []

	# stats
	print 
	#print 'match list:', len(match_list)
	print '---- match distr ----'
	for rank in sorted(match_count):
		acc = match_count[rank] / sum(match_count.values())
		print '\trank:', rank, 'match count:', match_count[rank], '(',acc,')'
		# keep track for plots later
		rank_list.append(rank)
		match_cnt_list.append(match_count[rank])
		data += [rank]*match_count[rank]

	print '\nLM mismatch', sum(entries_not_found.values()), 'out of', total_utts
	for entry in entries_not_found:
		print entry, entries_not_found[entry]

	print '\nMatched model distr:', match_model_distr


	print 
	print '---- chars only match distr ----'
	for rank in sorted(chars_match_count):
		acc = chars_match_count[rank] / sum(chars_match_count.values())
		print '\trank:', rank, 'match count:', chars_match_count[rank], '(',acc,')'

	print '\nLM mismatch', sum(chars_entries_not_found.values()), 'out of', total_utts
	for entry in chars_entries_not_found:
		print entry, chars_entries_not_found[entry]

	print '\nMatched char model distr:', chars_match_model_distr

	return data, rank_list, match_cnt_list, match_model_distr, spkr_addr_list



def main_run(curr_lm_dir, curr_test_sent_dir, curr_pickle_name, \
				curr_lm_outdir, main_char_lm_only=True):

	#print 'ref_type:', ref_type
	print 'lm_dir:', curr_lm_dir
	print

	# data to be stored as pickle file
	data_store = {}

	print '----test sents from:', curr_test_sent_dir

	time_start = timeit.default_timer()
	
	data, rank_list, match_cnt_list, match_model_distr, spkr_addr_list = \
		run(curr_lm_dir, curr_test_sent_dir, curr_lm_outdir, \
			main_char_lm_only=main_char_lm_only)

	time_stop = timeit.default_timer()

	print 'Runtime:', time_stop - time_start, 'secs'
	print 'Done.\n'

	# store data
	data_store['data']=data
	data_store['rank_list']=rank_list
	data_store['match_cnt_list']=match_cnt_list
	data_store['match_model_distr']=match_model_distr
	data_store['spkr_addr_list']=spkr_addr_list


	# store data as pickle file
	pickle.dump(data_store, open(os.path.join(OUTDIR,curr_pickle_name), 'w'))
	

if __name__ == "__main__":

	for fold in range(FOLDS):

		"""
		############## chars lm
		
		curr_lm_dir = os.path.join(CHAR_LM_DEFAULT_INDIR,str(fold),'train')
		curr_test_sent_dir = os.path.join(CHAR_TEST_SENTS_DIR,str(fold),'test')
		curr_pickle_name = 'rank-sents-lm-chars-'+str(fold)+'.pickle'
		curr_lm_outdir = os.path.join(LM_OUTDIR,str(fold))
		create_directory(curr_lm_outdir)
		main_run(curr_lm_dir, curr_test_sent_dir, curr_pickle_name, curr_lm_outdir)

		############## chars lm-liwc tagged (TODO: something is not right with the results)
		
		curr_lm_dir = os.path.join(CHAR_LM_LIWC_TAGGED_DEFAULT_INDIR,str(fold), \
						'train','chars-lm-liwc-tagged-default')
		curr_test_sent_dir = os.path.join(CHAR_TEST_SENTS_DIR,str(fold),'test')
		curr_pickle_name = 'rank-sents-lmliwc-chars-'+str(fold)+'.pickle'
		curr_lm_outdir = os.path.join(LM_LIWC_OUTDIR,str(fold))
		create_directory(curr_lm_outdir)
		main_run(curr_lm_dir, curr_test_sent_dir, curr_pickle_name, curr_lm_outdir)
		
		############## conv lm
		
		curr_lm_dir = os.path.join(CONV_LM_DEFAULT_INDIR,str(fold),'train')
		curr_test_sent_dir = os.path.join(CONV_TEST_SENTS_DIR,str(fold),'test')
		curr_pickle_name = 'rank-sents-lm-conv-'+str(fold)+'.pickle'
		curr_lm_outdir = os.path.join(LM_MULTI_OUTDIR,str(fold))
		create_directory(curr_lm_outdir)
		main_run(curr_lm_dir, curr_test_sent_dir, curr_pickle_name, curr_lm_outdir)
		

		############# conv lm-liwc tagged
		
		curr_lm_dir = os.path.join(CONV_LM_LIWC_TAGGED_DEFAULT_INDIR,str(fold), \
						'train', 'conv-lm-liwc-tagged-main_pairs-default')
		curr_test_sent_dir = os.path.join(CONV_TEST_SENTS_DIR,str(fold),'test')
		curr_pickle_name = 'rank-sents-lmliwc-conv-'+str(fold)+'.pickle'
		curr_lm_outdir = os.path.join(LM_LIWC_MULTI_OUTDIR,str(fold))
		create_directory(curr_lm_outdir)
		main_run(curr_lm_dir, curr_test_sent_dir, curr_pickle_name, curr_lm_outdir)
		"""

		############# chars main and others lm
		"""
		curr_lm_dir = os.path.join(CHAR_LM_MAIN_OTHERS_DEFAULT_INDIR,str(fold),'train')
		curr_test_sent_dir = os.path.join(CHAR_TEST_SENTS_DIR,str(fold),'test')
		curr_pickle_name = 'rank-sents-lm-chars-main-and-others-'+str(fold)+'.pickle'
		curr_lm_outdir = os.path.join(LM_MAIN_OTHERS_OUTDIR,str(fold))
		create_directory(curr_lm_outdir)
		main_run(curr_lm_dir, curr_test_sent_dir, curr_pickle_name, \
					curr_lm_outdir, main_char_lm_only=False)
		"""
		############# chars main and others lm-liwc tagged
		
		curr_lm_dir = os.path.join(CHAR_LM_LIWC_MAIN_OTHERS_TAGGED_DEFAULT_INDIR, \
						str(fold), 'chars-lm-liwc-tagged-default')
		curr_test_sent_dir = os.path.join(CHAR_TEST_SENTS_DIR,str(fold),'test')
		curr_pickle_name = 'rank-sents-lmliwc-chars-main-and-others-'+str(fold)+'.pickle'
		curr_lm_outdir = os.path.join(LM_LIWC_MAIN_OTHERS_OUTDIR,str(fold))
		create_directory(curr_lm_outdir)
		main_run(curr_lm_dir, curr_test_sent_dir, curr_pickle_name, \
					curr_lm_outdir, main_char_lm_only=False)

