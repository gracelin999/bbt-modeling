
import os, zipfile

# make sure to add directories for the ones you want to zip/repo up
dirs1 = ['input-tables',
		'input-tables-main-and-others',
		'input-tables-multi',
		'input-tables-multi-utt-test',
		'input-tables-others',
		'input-tables-utt-others',
		'input-tables-utt-test',
		'input-tables-utt-test-and-others',
		]

dirs2 = ['output-compare']

dirs3 = ['output-tables',
     	'output-tables-main-and-others',
     	'output-tables-multi',
     	'output-tables-multi-utt-test',
    	'output-tables-utt-others',
		]

# these directories are too large; skip zipping/repo them
#dirs4 = ['output-tables-utt-test',
#		'output-tables-utt-test-and-others']

dirs = dirs2+dirs3 #+dirs1

def zipdir(path,ziph):
	# ziph is zipfile handle
	for root, dirs, files in os.walk(path):
		for file in files:
			ziph.write(os.path.join(root,file))


for d in dirs:
	zipf=zipfile.ZipFile(d+'.zip','w',zipfile.ZIP_DEFLATED,allowZip64=True)
	zipdir(d,zipf)
	zipf.close()



