from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Generate z-models.
---------------------------------------------------------------
"""

import sys, time, os, re
from collections import defaultdict

import numpy as np

#/////////////////////////////////////////////////////////////////////

RSCDIR = os.path.join('..','..','..','resources')
sys.path.append(RSCDIR)
from common import create_directory

TABLE_CHAR_INDIR = 'input-tables'   # chars; train set by cv; grouped; copied
TABLE_CHAR_OUTDIR = 'output-tables' # zmodel results

TABLE_CHAR_UTT_TEST_INDIR = 'input-tables-utt-test' # chars; utt test set
TABLE_CHAR_UTT_TEST_OUTDIR = 'output-tables-utt-test'

TABLE_CHAR_UTT_OTHERS_INDIR = 'input-tables-utt-others' # chars; non-main chars utts
TABLE_CHAR_UTT_OTHERS_OUTDIR = 'output-tables-utt-others'

TABLE_CHARS_UTT_TEST_OTHERS_INDIR = 'input-tables-utt-test-and-others'
TABLE_CHARS_UTT_TEST_OTHERS_OUTDIR = 'output-tables-utt-test-and-others'

TABLE_CHAR_OTHERS_INDIR = 'input-tables-others'
# this combines "input-tables" and "input-tables-others"
TABLE_CHAR_MAIN_AND_OTHERS_INDIR = 'input-tables-main-and-others'
TABLE_CHAR_MAIN_AND_OTHERS_OUTDIR = 'output-tables-main-and-others'

TABLE_MULTI_INDIR = 'input-tables-multi'    # conv pairs; train set by cv; grouped; copied
TABLE_MULTI_OUTDIR = 'output-tables-multi'  # zmodel results

TABLE_MULTI_UTT_TEST_INDIR = 'input-tables-multi-utt-test' # conv pairs; test set
TABLE_MULTI_UTT_TEST_OUTDIR = 'output-tables-multi-utt-test'


FOLDS = 5

#/////////////////////////////////////////////////////////////////////

####
#### create custom table with specific characters
#### save it back to input directory
#### This is for 'chars' only, not for 'conv'
####
def get_specific_chars(infile, sep, outfile, wanted_chars, TABLE_INDIR):
    
    if '-conv-' in infile:
        print 'NOTE. get_specific_chars is only for chars only.'
        return
    
    # open file and read lines
    f = open(os.path.join(TABLE_INDIR,infile))
    lines = f.readlines()

    # open output file for writing
    OUTF = open(os.path.join(TABLE_INDIR, outfile),'wb')
    # write header
    OUTF.write(lines[0])

    # go through each line and pick desired characters
    for line in lines[1:]:
        line_arr = line.split(sep)

        # for series/episodes/scenes, it has additional info
        speaker = line_arr[0].strip() 

        if 'series-trends' in infile:
            m = re.search('series-(.+)', speaker)
            speaker = m.group(1)
        elif 'series' in infile:
            m = re.search('series-(.+)', speaker)
            speaker = m.group(1)
        elif 'episode' in infile:
            m = re.search('-episode-\d+-(.+)', speaker)
            speaker = m.group(1)
        elif 'scene' in infile:
            m = re.search('(.+)-series', speaker)
            speaker = m.group(1)

        # see if speaker is what we wanted
        if speaker in wanted_chars:  
               OUTF.write(line)

    OUTF.close()


####
#### get specific speaker and/or addressee from 'conv' files
#### can be empty
####
def get_specific_speaker_addressee(infile, sep, outfile, want_speaker, 
        want_addressee, TABLE_INDIR):

    if '-chars-' in infile:
        print 'NOTE. get_specific_speaker_addresse is only for conv only.'
        return
    # make sure either want_speaker or want_addressee has something
    if want_speaker=='' and want_addressee=='':
        print 'NOTE. Both desired speaker and addressee are empty.'
        return

    # open file and read lines
    f = open(os.path.join(TABLE_INDIR,infile))
    lines = f.readlines()
    # open output file for writing
    OUTF = open(os.path.join(TABLE_INDIR, outfile),'wb')
    # write header
    OUTF.write(lines[0])
    # go through each line and pick desired characters
    for line in lines[1:]:
        line_arr = line.split(sep)
        speaker = line_arr[0]
        # addressee; e.g., amy-series-5-episode-08
        # let it match series/episode if necessary
        addressee = line_arr[1]
        # get match
        # if both entries exist
        if want_speaker and want_addressee:
            if (speaker == want_speaker) and (want_addressee in addressee):
                OUTF.write(line)
        elif (speaker == want_speaker) or (want_addressee in addressee):
            OUTF.write(line)
    OUTF.close()



def get_specific_speaker_addressee_multi_pair(infile, sep, outfile, 
        want_speaker, want_addressee, TABLE_INDIR):

    if '-chars-' in infile:
        print 'NOTE. get_specific_speaker_addresse is for conv only.'
        return

    # open file and read lines
    f = open(os.path.join(TABLE_INDIR,infile))
    lines = f.readlines()
    # open output file for writing
    OUTF = open(os.path.join(TABLE_INDIR, outfile),'wb')
    # write header
    OUTF.write(lines[0])
    # go through each line and pick desired characters
    for line in lines[1:]:
        line_arr = line.split(sep)
        speaker = line_arr[0].strip()
        addressee = ''
        desc = ''
        
        if 'series'in line_arr[1]:
            arr = line_arr[1].strip().split('-series')
            addressee = arr[0].strip()
            # trends: get the whole thing
            if 'trends' in infile:
                desc = 'series'+arr[1].strip()
        else:
            temp = line_arr[1].strip()
            if temp[-1]=='-':
                addressee = temp[:-1]

        # get match
        # if both entries exist
        if 'trends' in infile:
            #print want_speaker, desc, want_addressee
            if want_addressee[0] == desc:
                if (want_speaker[0]=='any') or (speaker in want_speaker):
                    OUTF.write(line)
        elif want_speaker and want_addressee:
            if (speaker in want_speaker) and (addressee in want_addressee):
                OUTF.write(line)
        elif (speaker in want_speaker) or (addressee in want_addressee):
            OUTF.write(line)
    OUTF.close()



def get_model_columns(sep, f):
    lines = open(f).read().split('\n')
    
    # get header: speaker followed by features
    header_arr = lines[0].split(sep)
    #print len(header_arr) #,header_arr

    num_feats = len(header_arr)-1 # skips "speaker" column
    print 'num feats:', num_feats

    # keep track of data by columns
    columns = defaultdict(list)

    # fill in data
    for line in lines[1:]:
        line = line.strip()
        if not line: continue

        line_arr = line.split(sep)
        for i, v in enumerate(line_arr):
            if i!=0: # first column is speaker (and addr)
                v = float(v)
            columns[header_arr[i]].append(v)

    return header_arr, columns


####
#### get average and z-model
####
def get_model(filename, sep, TABLE_INDIR, TABLE_OUTDIR):

    print '----> processing file:', filename
    print 'table_indir:', TABLE_INDIR
    print 'table_outdir:', TABLE_OUTDIR

    if '-conv-' in filename:
        type='conv'
    else:
        type='char'

    f = os.path.join(TABLE_INDIR,filename)
    header_arr, columns = get_model_columns(sep, f)
    """
    lines = open(os.path.join(TABLE_INDIR,filename)).read().split('\n')
    
    # get header: speaker followed by features
    header_arr = lines[0].split(sep)
    #print len(header_arr) #,header_arr

    num_feats = len(header_arr)-1 # skips "speaker" column
    print 'num feats:', num_feats

    # keep track of data by columns
    columns = defaultdict(list)

    # fill in data
    for line in lines[1:]:
        line = line.strip()
        if not line: continue

        line_arr = line.split(sep)
        for i, v in enumerate(line_arr):
            if i!=0: # first column is speaker (and addr)
                v = float(v)
            columns[header_arr[i]].append(v)
    """

    # find mean and stdev, then calculate z-score
    # store in rows
    columns_mean = defaultdict(float)
    columns_stdev = defaultdict(float)
    rows = defaultdict(list)

    # keep the original column header order
    for key in header_arr: #columns.keys():
        if key=='speaker': continue

        #print key, columns[key]
        if sum(columns[key])==0:
            mean = 0
            stdev = 0
        else:
            #print filename, key, len(columns[key])
            #mean, stdev = common.meanstdv(columns[key])
            mean = np.mean(columns[key])
            stdev = np.std(columns[key])

        #print key, mean, stdev
        columns_mean[key] = mean
        columns_stdev[key] = stdev

        # store the mean and stdev in separate rows
        rows['_mean'].append(mean)
        rows['_stdev'].append(stdev)

        # go through each value in column and find z-value
        for i in range(len(columns[key])):
            val = columns[key][i]
            if stdev==0:
                zscore = 0
            else:
                zscore = (val-mean)/stdev
            # store zscore
            rows[columns['speaker'][i]].append(zscore)

            #print key, i, val

    #print columns_mean['category-also-ratio']
    #print columns_stdev['category-also-ratio']
    #print columns_mean['category-concede-ratio']
    #print columns_stdev['category-concede-ratio']
    #print rows['houston'][0:4]
    #print rows['all'][0:4]
    #print rows['kripke'][0:4]
    #print rows['cole'][0:4]

    # save results to output file
    outfile= re.sub('.txt','-zvalues.txt',filename)
    OUTF = open(os.path.join(TABLE_OUTDIR, outfile),'wb')
    #newheader = re.sub('speaker\|addressee','speaker==addressee',lines[0])
    #OUTF.write(newheader) # write header
    OUTF.write(sep.join(header_arr)+'\n')

    # write to zmodel file (look for |zscore| >= 1)
    zoutfile = re.sub('.txt','-zmodel1.txt',filename)
    ZOUTF = open(os.path.join(TABLE_OUTDIR, zoutfile),'wb')
    if type=='char':
        ZOUTF.write('speaker|num feats|feats\n')
    else:
        ZOUTF.write('speaker==addressee|num feats|feats\n')
    
    # write to zmodel file (look for |zscore| >= 2)
    zoutfile2 = re.sub('.txt','-zmodel2.txt',filename)
    ZOUTF2 = open(os.path.join(TABLE_OUTDIR, zoutfile2),'wb')
    if type=='char':
        ZOUTF2.write('speaker|num feats|feats\n')
    else:
        ZOUTF2.write('speaker==addressee|num feats|feats\n')
    
    # write mean and stdev
    # rows['_mean'], rows['_stdev']
    for item in ['_mean', '_stdev']:
        tostr = [str(x) for x in rows[item]]
        OUTF.write(item+'|'+'|'.join(tostr)+'\n')

    # go through each speaker in original order
    for i in range(len(columns['speaker'])):
        skey = columns['speaker'][i]

        if type=='char':
            skey_str = skey
        else: 
            # conv "speaker":
            # e.g., -char-bernadette-inpresenceof-howard
            m = re.match('-char-(.+)-inpresenceof-(.+)', skey)
            spkr = m.group(1).strip()
            addr = m.group(2).strip()
            skey_str = spkr+'=='+addr


        tostr = [str(x) for x in rows[skey]]
        OUTF.write(skey_str+'|'+'|'.join(tostr)+'\n')
        #print 'num feats', len(rows[speaker])
        #print 'num header items', len(header_arr)
        # z-model
        ZOUTF.write(skey+'|')
        ZOUTF2.write(skey+'|')

        # feats
        numfeats = 0
        numfeats2 = 0
        zstr = ''
        zstr2 = ''

        # go through each feature
        z1 = defaultdict(list)
        z2 = defaultdict(list)
        for j in range(len(rows[skey])):
            val = rows[skey][j]
            if val>=1 or val<=-1:
                idx = j+1  # first item is speaker
                feat = header_arr[idx]
                z1[val].append(feat)
                if val>=2 or val<=-2:
                    z2[val].append(feat)
        """
        #print skey, len(rows[skey]), len(header_arr)
        for j in range(len(rows[skey])):
            val = rows[skey][j]
            if val>=1 or val<=-1:
                feat = header_arr[j+1] # plus 1 since first item is speaker
                numfeats += 1
                zstr += feat+'('+'%.2f'%val+'),'
                if val>=2 or val<=-2:
                    numfeats2 += 1
                    zstr2 += feat+'('+'%.2f'%val+'),'
        """

        for z1v in sorted(z1.keys(),reverse=True):
            for f in z1[z1v]:
                zstr += f+'('+'%.2f'%z1v+'),'
                numfeats+=1
        for z2v in sorted(z2.keys(), reverse=True):
            for f in z2[z2v]:
                zstr2 += f+'('+'%.2f'%z2v+'),'
                numfeats2+=1

        ZOUTF.write(str(numfeats)+'|'+zstr+'\n')
        ZOUTF2.write(str(numfeats2)+'|'+zstr2+'\n')

    OUTF.close()
    ZOUTF.close()
    ZOUTF2.close()



def combine_files(sep, f_main, f_others, f_combine):
    # get each file's content
    header_arr_main, columns_main = get_model_columns(sep, f_main)
    header_arr_others, columns_others = get_model_columns(sep, f_others)
    num_chars_main = len(columns_main['speaker'])
    num_chars_others = len(columns_others['speaker'])
    print 'main feats:', len(header_arr_main), 'chars:', num_chars_main
    print 'main feats:', len(header_arr_others), 'chars:', num_chars_others

    both = set(header_arr_main + header_arr_others)
    main_fill = list(both - set(header_arr_main))
    others_fill = list(both - set(header_arr_others))
    print 'total:', len(both), 'main need:', len(main_fill)
    print 'total:', len(both), 'others need:', len(others_fill)


    for item in main_fill:
        columns_main[item] = [0]*num_chars_main
        header_arr_main.append(item)
    header_arr_main = sorted(header_arr_main)
    header_arr_main.remove('speaker')
    header_arr_main = ['speaker']+header_arr_main

    for item in others_fill:
        columns_others[item] = [0]*num_chars_others
        header_arr_others.append(item)
    header_arr_others = sorted(header_arr_others)
    header_arr_others.remove('speaker')
    header_arr_others = ['speaker']+header_arr_others

    print header_arr_main == header_arr_others
    #print header_arr_main[:5], '...'

    fout = open(f_combine,'wb')
    fout.write(sep.join(header_arr_main)+'\n')

    # write out main character's feats
    for c in range(num_chars_main):
        # get all the feats
        feats = []
        for item in header_arr_main:
            vstr = str(columns_main[item][c])
            feats.append(vstr)
            #
            if vstr=='nan': 
                print 'NOTE. NAN in main', columns_main['speaker'][c], item 
        # write it out
        fout.write(sep.join(feats)+'\n')

    # write out other characters' feats
    for c in range(num_chars_others):
        feats = []
        for item in header_arr_others:
            vstr = str(columns_others[item][c])
            feats.append(vstr)
            #
            if vstr=='nan': 
                print 'NOTE. NAN in others', columns_others['speaker'][c], item 
        fout.write(sep.join(feats)+'\n')

    fout.close()


####
#### Main
####
if __name__ == "__main__":

    # grouped
    """
    for t in ['train', 'test']:
        for fold in range(FOLDS):

            #### by characters
            f='gen-cv-chars-default-table-'+str(fold)+'.txt'
            if t=='test':
                f=re.sub('-table-','-table-test-',f)
            print '----> processing file:', f
            create_directory(TABLE_CHAR_OUTDIR)
            get_model(f, '|', TABLE_CHAR_INDIR, TABLE_CHAR_OUTDIR)

            #### by conv pair (from multi-party conversations)
            f='gen-cv-conv-multi-all-pair-main-table-'+str(fold)+'.txt'
            if t=='test':
                f=re.sub('-table-','-table-test-',f)
            print '----> processing file:', f
            create_directory(TABLE_MULTI_OUTDIR)
            get_model(f, '|', TABLE_MULTI_INDIR, TABLE_MULTI_OUTDIR)
    """
    
    # utt for test set
    """
    create_directory(TABLE_CHAR_UTT_TEST_OUTDIR)
    create_directory(TABLE_MULTI_UTT_TEST_OUTDIR)

    for fold in range(FOLDS):
        f='gen-utt-chars-default-table'+str(fold)+'.txt'
        if os.path.isfile(os.path.join(TABLE_CHAR_UTT_TEST_INDIR,f)):
            get_model(f,'|',TABLE_CHAR_UTT_TEST_INDIR, TABLE_CHAR_UTT_TEST_OUTDIR)

        # by conv pair
        f='gen-utt-conv-multi-all-pair-main-table'+str(fold)+'.txt'
        if os.path.isfile(os.path.join(TABLE_MULTI_UTT_TEST_INDIR,f)):
            get_model(f,'|',TABLE_MULTI_UTT_TEST_INDIR,TABLE_MULTI_UTT_TEST_OUTDIR)
    """

    # utt + other chars utt for test set
    create_directory(TABLE_CHARS_UTT_TEST_OTHERS_INDIR)
    f_others = os.path.join(TABLE_CHAR_UTT_OTHERS_INDIR, \
                    'gen-utt-others-chars-default-table.txt')

    # create combined files
    for fold in range(FOLDS):
        f_main = os.path.join(TABLE_CHAR_UTT_TEST_INDIR, \
                    'gen-utt-chars-default-table'+str(fold)+'.txt')
        f ='gen-utt-chars-test-and-others-default-table'+str(fold)+'.txt'
        fpath = os.path.join(TABLE_CHARS_UTT_TEST_OTHERS_INDIR, f)
        combine_files('|', f_main, f_others, fpath)

    create_directory(TABLE_CHARS_UTT_TEST_OTHERS_OUTDIR)

    # get zmodel
    for fold in range(FOLDS):
        f ='gen-utt-chars-test-and-others-default-table'+str(fold)+'.txt'
        get_model(f,'|', TABLE_CHARS_UTT_TEST_OTHERS_INDIR, \
                         TABLE_CHARS_UTT_TEST_OTHERS_OUTDIR)

    # main + other chars zmodels
    # main has 5 folds; we'll add "others" to each of the folds
    """
    f_others = os.path.join(TABLE_CHAR_OTHERS_INDIR, 
                    'gen-others-chars-default-table.txt')

    # create combined files
    for fold in range(FOLDS):
        f_main = os.path.join(TABLE_CHAR_INDIR,
                'gen-cv-chars-default-table-'+str(fold)+'.txt')
        f = 'gen-cv-chars-main-and-others-default-table-'+str(fold)+'.txt'
        fpath = os.path.join(TABLE_CHAR_MAIN_AND_OTHERS_INDIR, f)
        combine_files('|', f_main, f_others, fpath)

    create_directory(TABLE_CHAR_MAIN_AND_OTHERS_OUTDIR)

    # get zmodel
    for fold in range(FOLDS):
        f = 'gen-cv-chars-main-and-others-default-table-'+str(fold)+'.txt'
        get_model(f,'|',TABLE_CHAR_MAIN_AND_OTHERS_INDIR, \
                        TABLE_CHAR_MAIN_AND_OTHERS_OUTDIR)
    """
