
Many directories are zipped to save space.

Some output files do not have in zipped format because it's too large.
They can be recreated by using gen-cv-zmodel.py.

Tables:

input-tables/		chars; train and test set by cv; grouped; copied
output-tables/		zmodel results

input-tables-utt-test/	chars; test set by utt by cv; copied
output-tables-utt-test/	zmodel results

input-tables-multi/		conv pairs; train and test set by cv; grouped; copied
output-tables-multi/	zmodel results

input-tables-multi-utt-test/	conv pairs; test set by utt by cv; utt; copied
output-tables-multi-utt-test/	zmodel results

input-tables-utt-test-and-others/	chars; utt zmodel created by main chars and other chars' utt
output-tables-utt-test-and-others/	zmodel results
