
import os, sys
import re, pickle

#///////////////////////////////////////////////////////////////////// 

THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
from common import create_directory

CHAR_INDIR = 'conv-input-all-pair-main-utt'
CHAR_INDIR_ORIG = 'conv-input-all-pair-main'
POS_DIR = 'pos-utt'
POS_DIR_ORIG = os.path.join(POS_DIR,'pos')
WN_DIR = 'wn-utt'
WN_DIR_ORIG = os.path.join(WN_DIR,'wn')


#///////////////////////////////////////////////////////////////////// 


for f in os.listdir(CHAR_INDIR_ORIG):
	if f[0]=='.': continue # skip hidden files

	print '---- splitting content to one utt per file:', f

	lines = open(os.path.join(CHAR_INDIR_ORIG,f)).read()
	lines = lines.split('\n')

	for i, line in enumerate(lines):
		line = line.strip()
		if not line:
			print 'ERROR? Empty line:',i,'Skip.' 
			continue

		fname = os.path.join(CHAR_INDIR, re.sub('.txt','-'+str(i)+'.txt',f))
		outf = open(fname,'wb')
		outf.write(line)
		outf.close()


# write to pickle files

for maindir in [CHAR_INDIR, POS_DIR, WN_DIR]:
	content = {}
	if not os.path.isdir(maindir): continue

	for f in os.listdir(maindir):
		c = open(os.path.join(maindir,f)).read()
		content[f]=c
	# write to pickle
	pname = maindir+'.pickle'
	pickle.dump(content, open(pname,'wb'))




