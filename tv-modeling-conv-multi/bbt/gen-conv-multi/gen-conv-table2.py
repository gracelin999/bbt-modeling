from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Generate tables for conversations

To run:
>python gen-conv-table.py > gen-conv-table.log
---------------------------------------------------------------
"""

import sys, time, os, glob, copy
import re, sets, pickle
from collections import defaultdict

#### change this to your location of tools
#### use it to call Stanford's Core NLP
#sys.path.append(os.path.join('..','..','..','..','resources'))
RESOURCE_DIR = os.path.join('..', '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common
#import word_category_counter_new.word_category_counter as liwc

# get features' information
SCRIPTS_DIR = os.path.join('..','..','gen-features-scripts')
SCRIPTS_DIR = os.path.join('..','..','..', \
    'tv-modeling-features', 'gen-features-scripts')
sys.path.append(SCRIPTS_DIR)
import getfeatsinfo

# conversation directories (input)
CONV_INDIR = 'conv-input' # test-input
CONV_PAIR_INDIR = 'conv-input-pair'
CONV_CAT_INDIR = 'conv-input-category'

CONV_EPISODE_INDIR = 'conv-input-episode'
CONV_EPISODE_PAIR_INDIR = 'conv-input-episode-pair'
CONV_EPISODE_PAIR_MAIN_INDIR = 'conv-input-episode-pair-main'
CONV_SERIES_INDIR = 'conv-input-series'
CONV_SERIES_PAIR_INDIR = 'conv-input-series-pair'
CONV_SERIES_PAIR_MAIN_INDIR = 'conv-input-series-pair-main'
CONV_SERIES_PAIR_MAIN_TRENDS_INDIR = 'conv-input-series-pair-main-trends'
CONV_ALL_INDIR = 'conv-input-all'
CONV_ALL_PAIR_INDIR = 'conv-input-all-pair'
CONV_ALL_PAIR_MAIN_INDIR = 'conv-input-all-pair-main'

CONV_OUTDIR = 'conv-output' # test-output
CONV_PAIR_OUTDIR = 'conv-output-pair'
CONV_CAT_OUTDIR = 'conv-output-category'

CONV_EPISODE_OUTDIR = 'conv-output-episode'
CONV_SERIES_OUTDIR = 'conv-output-series'
CONV_SERIES_TRENDS_OUTDIR = 'conv-output-series-trends'
CONV_ALL_OUTDIR = 'conv-output-all'

# arff directories
ARFF_OUTDIR = 'conv-output-arff'
ARFF_PAIR_OUTDIR = 'conv-output-arff-pair'
ARFF_CAT_OUTDIR = 'conv-output-arff-category'

ARFF_EPISODE_OUTDIR = 'conv-output-arff-episode'
ARFF_EPISODE_PAIR_OUTDIR = 'conv-output-arff-episode-pair'
ARFF_EPISODE_PAIR_MAIN_OUTDIR = 'conv-output-arff-episode-pair-main'
ARFF_SERIES_OUTDIR = 'conv-output-arff-series'
ARFF_SERIES_PAIR_OUTDIR = 'conv-output-arff-series-pair'
ARFF_SERIES_PAIR_MAIN_OUTDIR = 'conv-output-arff-series-pair-main'
ARFF_SERIES_PAIR_MAIN_TRENDS_OUTDIR = 'conv-output-arff-series-pair-main-trends'
ARFF_ALL_OUTDIR = 'conv-output-arff-all'
ARFF_ALL_PAIR_OUTDIR = 'conv-output-arff-all-pair'
ARFF_ALL_PAIR_MAIN_OUTDIR = 'conv-output-arff-all-pair-main'


# create tables
TABLE_FILENAME = 'gen-conv-table.txt'
TABLE_PAIR_FILENAME = 'gen-conv-pair-table.txt'
TABLE_CAT_FILENAME = 'gen-conv-category-'

TABLE_SERIES_FILENAME = 'gen-conv-series-table.txt'
TABLE_SERIES_PAIR_FILENAME = 'gen-conv-series-pair-table.txt'
TABLE_SERIES_PAIR_MAIN_FILENAME = 'gen-conv-series-pair-main-table.txt'
TABLE_SERIES_PAIR_MAIN_TRENDS_FILENAME = 'gen-conv-series-pair-main-trends-table.txt'
TABLE_EPISODE_FILENAME = 'gen-conv-episode-table.txt'
TABLE_EPISODE_PAIR_FILENAME = 'gen-conv-episode-pair-table.txt'
TABLE_EPISODE_PAIR_MAIN_FILENAME = 'gen-conv-episode-pair-main-table.txt'
TABLE_ALL_FILENAME = 'gen-conv-all-table.txt'
TABLE_ALL_PAIR_FILENAME = 'gen-conv-all-pair-table.txt'
TABLE_ALL_PAIR_MAIN_FILENAME = 'gen-conv-all-pair-main-table.txt'

#### script names as features
FEATURES = ['extract-liwc', # do this first so others can use LIWC
            'extract-concess-words-pol',
            'extract-tag',
            'extract-content-words',
            'extract-verb-strength',
            'extract-hedges',
            'extract-verbs', 
            'extract-merge',
            'extract-words-personage',
            'extract-pol-sentiword', 
            'extract-dialogue-act',  
            'extract-basic',
            #'extract-info'     # probably don't want to use this
            #'extract-actions',
            #'extract-actionapi'
            'extract-words',
            'extract-ngram2',
            'extract-pos-ngram2'
            ]

MAIN_CHARS = ['sheldon', 'leonard', 'penny', 'howard', 'raj', 'amy', 'bernadette']

#SCENE_CATEGORIES = ['apartment', 'cafeteria', 'cheesecake', 'comic', 'office', 'stair']
#SCENE_CATEGORIES = ['sheldon', 'leonard', 'penny', 'howard', 'raj',
#                    'amy', 'bernadette',]
"""
SCENE_CATEGORIES = ['sheldon', 'leonard', 'penny', 'howard', 'raj',
                    'amy', 'bernadette', 'apartment', 'living-room', 'cafeteria',
                    'cheesecake', 'comic', 'office', 'stair', 'bowling', 'laundry',
                    'lobby', 'hallway', 'restaurant', 'kitchen', 'hotel', 'bookstore',
                    'hospital', 'gift-shop', 'gymnasium', 'supermarket', 'bathroom',
                    'cinema', 'university', 'bookshop', 'train', 'conference', 'park',
                    'goth-club', 'tattoo', 'camp', 'coffee-shop', '-bar', 'airport',
                    'party', '-lab', 'paint', 'taxi', 'movie', 'corridor',
                    'physics', 'store', 'dmv', '-car', 'elevator', 'police', 'church']
"""
SCENE_CATEGORIES = ['sheldon', 'leonard', 'penny', 'howard', 'raj',
                    'amy', 'bernadette', 'living-room', 'bedroom', 'cafeteria', 'corridor',
                    '-lab', '-car', 'university', 'kitchen', 'restaurant',
                    'hallway', 'lobby', 'laundry']

def categorize_episode(num):
    if num<10:
        return 'e1'
    elif num>=10 and num<20:
        return 'e2'
    else:
        return 'e3'

def categorize_scene(num):
    if num<5:
        return 'sc1'
    elif num>=5 and num<10:
        return 'sc2'
    else:
        return 'sc3'

def categorize_series(num):
    return 's'+str(num)


def process_filename(txt):
    m = re.match('(.*)-char-(.+)-inpresenceof-(.*)',txt)
    #print txt
    header = m.group(1)
    speaker = m.group(2)
    addressee = m.group(3)
    if not addressee:
        addressee='noone'
    return speaker, addressee, header


####
#### write to arff file for Weka (by speaker)
####
def write_table_and_arff(inst_all, feats_all, tablefile, arffdir, type):
    OUTF = open(tablefile,'w')
    OUTF.write('speaker|addressee')
    for f in sorted(feats_all):
        feat = common.proc_str(f)      # generic process of string
        #if 'rhs_sun' in feat:
        #    print '---->', f
        OUTF.write('|'+feat)
    OUTF.write('\n')

    pair_by_speaker = defaultdict(list)
    for conv_pair in sorted(inst_all.keys()):
        # get speaker/addressee
        speaker, addressee, header = process_filename(conv_pair)
        # group by main character or 'others'
        found = False
        for c in MAIN_CHARS:
            if c in addressee:
                found = True
                break
        if found:
            #addressee = 'others'
            new_inst = common.process_attributes(inst_all[conv_pair], feats_all)
            pair_by_speaker[speaker].append([addressee+'-'+header, new_inst])
        #if 'sheldon-server' in conv_pair or 'sheldon-stan lee' in conv_pair:
        #    print conv_pair, inst_all[conv_pair]

    # write to arff file by each speaker
    for key in sorted(pair_by_speaker.keys()):
        # skip if the type is the same as the speaker
        if type==key:
            continue
        print '====> write to arff file for speaker('+key+')'
        # open arff file for writing
        if type=='default':
            arff = open(os.path.join(arffdir,key+'.arff'), 'w')
        else:
            arff = open(os.path.join(arffdir,key+'-class-'+type+'.arff'), 'w')
        # talking to sheldon vs. everyone else
        #arffSheldonVsOthers = open(os.path.join(arffdir,key+'.SheldonVsOthers.arff'), 'w')
        # write to both files
        arffstr = '%'+key+"\n\n"; 
        arff.write(arffstr); #arffSheldonVsOthers.write(arffstr)
        arffstr = '@relation '+key+"\n\n"
        arff.write(arffstr); #arffSheldonVsOthers.write(arffstr)
        # write out attributes
        for f in sorted(feats_all):
            feat = common.proc_str(f)

            # class is the presence of particular addressee
            # skip it here since it is entered as class later
            if (type!='default') and (feat=='info-addressee-'+type):
                continue

            if feat=='info-series':
                arffstr = '@attribute\t'+feat+'\t{s1,s2,s3,s4,s5,s6}\n'
            elif feat=='info-episode':
                arffstr = "@attribute\t"+feat+"\t{e1,e2,e3}\n"
            elif feat =='info-scene':
                arffstr = "@attribute\t"+feat+"\t{sc1,sc2,sc3}\n"
            elif feat =='info-category':
                arffstr = "@attribute\t"+feat+'\t{'+','.join(SCENE_CATEGORIES+['other'])+'}\n'
            elif 'info-addressee' in feat:
                arffstr = "@attribute\t"+feat+"\t{0,1}\n"
            else:
                arffstr = "@attribute\t"+feat+"\treal\n"
            arff.write(arffstr);
            #print '$$$$', f, feat, arffstr

        # write out class
        addressee_list = []
        for item in pair_by_speaker[key]:
            # take the person's name only
            itemarr = item[0].split('-series')
            #print 'itemarr:', itemarr
            addr = itemarr[0]
            if '"'+addr+'"' not in addressee_list:
                addressee_list.append('"'+addr+'"')

        if type!='default':
            arff.write('@attribute\tclass\t{0,1}\n\n')
        else:
            arff.write('@attribute\tclass\t{'+','.join(addressee_list)+'}\n\n')

        # write out instances for each addressee
        arffstr = '@data\n'
        arff.write(arffstr); #arffSheldonVsOthers.write(arffstr)
        for item in pair_by_speaker[key]:
            itemarr = item[0].split('-series')
            addr_arff = itemarr[0] # take person's name only for arff file
            addr = item[0]
            attrs = item[1]
            inst = []
            # go through each attribute
            for f in sorted(attrs.keys()):
                if (type!='default') and (f=='info-addressee-'+type):
                    continue

                if f=='info-episode':
                    s = categorize_episode(int(attrs[f]))
                elif f=='info-scene':
                    s = categorize_scene(int(attrs[f]))
                elif f=='info-series':
                    s = categorize_series(int(attrs[f]))
                elif 'info-addressee' in f:
                    s = str(int(attrs[f]))
                else:
                    s = str(attrs[f])
                inst.append(s)

            # write to arff file
            if type!='default':
                arff.write(','.join(inst)+','+str(int(attrs['info-addressee-'+type]))+'\n')
            else:
                arff.write(','.join(inst)+','+'"'+addr_arff+'"'+'\n')

            # also write to a regular table
            OUTF.write(key+'|'+addr+'|'+'|'.join(inst)+'\n')

        arff.close()
        #arffSheldonVsOthers.close()
        print 'speaker:', key, len(pair_by_speaker[key])
        for item in pair_by_speaker[key]:
            print '\taddressee:', item[0], len(item[1]), 'feats'

    OUTF.close()



####
#### collect all the features and write to a table
####
def get_table(indir, outdir, features, tablefile, arffdir):
#if __name__ == "__main__":
    # features to get
    # table file to write
    #OUTF = open(TABLE_FILENAME,'w')
    #OUTF.write('pair|'+'|'.join(features)+'\n')
    print 'get_table indir:', indir
    print 'get_table outdir:', outdir

    # go through each pair of conversation
    input_info = pickle.load(open(os.path.join(indir, indir+'.pickle')))  
    conv_pairs = input_info.keys()
    #conv_pairs = os.listdir(indir)
    feats_all = defaultdict(int)    # count features being used
    inst_all = defaultdict(list)    # store all instances for all conv. pairs

    for cp in conv_pairs:
        #if cp[0]=='.': continue  # hidden file
        if cp[-4:]!='.txt': continue

        #if cp[0]!='s': continue # restrict to sheldon-
        p = re.sub('.txt','',cp)
        #OUTF.write(p)
        print outdir, p
        fset_all = []    # collect all feature sets

        for feat in features:
            output_info = pickle.load(open(os.path.join(outdir, feat+'.pickle'))) 
            #listing = os.listdir(outdir)
            #print outdir, feat, p+'.txt.out'
            header = ''
            if feat == 'extract-pol-sentiword':
                ext = '.pos.wn.senti.txt'
            elif feat == 'extract-actions':
                ext = '.action.txt'
            elif feat == 'extract-actionapi':
                ext = '.postproc.txt'
                header = 'output'
            else:
                ext = '.txt.out'
            feat_fname = header+p+ext
            #ffile = open(os.path.join(outdir,feat,feat_name))
            feat_content = output_info[feat_fname]
            #fset = getfeatsinfo.get_feature_info(ffile, feat)
            fset = getfeatsinfo.get_feature_info(feat_content, feat)
            #print p, fset
            if fset:
                fset_all.append(fset)
                for k in fset.keys():
                    feats_all[k] += 1   # get feature name

        #OUTF.write('\n')
        #write_arff(fset_all, p) # write to arff
        inst_all[p]=copy.deepcopy(fset_all)
        #if 'zack-amy' in inst_all.keys():
        #    print p, inst_all[p]
        #    print 'zack-amy', inst_all['zack-amy']

    #print inst_all
    #print feats_all
    #for key in inst_all.keys():
    #    print key, len(inst_all[key]), inst_all[key]
    #print 'sheldon-server', inst_all['sheldon-server']
    write_table_and_arff(inst_all, feats_all.keys(), tablefile, arffdir, 'default')
    if ('extract-info' in FEATURES) and ('-pair' not in arffdir):
        for char in MAIN_CHARS:
            write_table_and_arff(inst_all, feats_all.keys(), tablefile, arffdir, char)

    #OUTF.close()



####
#### Generate table based on type.
#### This assumes features had already be run by run_get_features().
####
def run_get_tables(type):
    print 'Getting table with type', type, '...'
    if type == 'default':
        get_table(CONV_INDIR, \
                    CONV_OUTDIR, \
                    FEATURES, \
                    TABLE_FILENAME, \
                    ARFF_OUTDIR)
    elif type == 'pair':
        get_table(CONV_PAIR_INDIR, \
                    CONV_OUTDIR, #CONV_PAIR_OUTDIR \
                    FEATURES, \
                    TABLE_PAIR_FILENAME, \
                    ARFF_PAIR_OUTDIR)
    elif type == 'category':
        dirs = os.listdir(CONV_CAT_INDIR)
        for d in dirs:
            get_table(CONV_CAT_INDIR+os.sep+d+os.sep, \
                        CONV_OUTDIR,
                        FEATURES, \
                        TABLE_CAT_FILENAME+d+'.txt', \
                        ARFF_CAT_OUTDIR+os.sep+d+os.sep)
    elif type == 'series':
        get_table(CONV_SERIES_INDIR, \
                    CONV_SERIES_OUTDIR, \
                    FEATURES, \
                    TABLE_SERIES_FILENAME, \
                    ARFF_SERIES_OUTDIR)  
    elif type == 'series-pair':
        get_table(CONV_SERIES_PAIR_INDIR, \
                    CONV_SERIES_OUTDIR, \
                    FEATURES, \
                    TABLE_SERIES_PAIR_FILENAME, \
                    ARFF_SERIES_PAIR_OUTDIR) 
    elif type == 'series-pair-main':
        get_table(CONV_SERIES_PAIR_MAIN_INDIR, \
                    CONV_SERIES_OUTDIR, \
                    FEATURES, \
                    TABLE_SERIES_PAIR_MAIN_FILENAME, \
                    ARFF_SERIES_PAIR_MAIN_OUTDIR)   
    elif type == 'series-pair-main-trends':
        get_table(CONV_SERIES_PAIR_MAIN_TRENDS_INDIR, \
                    CONV_SERIES_TRENDS_OUTDIR, \
                    FEATURES, \
                    TABLE_SERIES_PAIR_MAIN_TRENDS_FILENAME, \
                    ARFF_SERIES_PAIR_MAIN_TRENDS_OUTDIR)  
    elif type == 'episode':
        get_table(CONV_EPISODE_INDIR, \
                    CONV_EPISODE_OUTDIR, \
                    FEATURES, \
                    TABLE_EPISODE_FILENAME, \
                    ARFF_EPISODE_OUTDIR)
    elif type == 'episode-pair':
        get_table(CONV_EPISODE_PAIR_INDIR, \
                    CONV_EPISODE_OUTDIR, \
                    FEATURES, \
                    TABLE_EPISODE_PAIR_FILENAME, \
                    ARFF_EPISODE_PAIR_OUTDIR)
    elif type == 'episode-pair-main':
        get_table(CONV_EPISODE_PAIR_MAIN_INDIR, \
                    CONV_EPISODE_OUTDIR, \
                    FEATURES, \
                    TABLE_EPISODE_PAIR_MAIN_FILENAME, \
                    ARFF_EPISODE_PAIR_MAIN_OUTDIR)
    elif type == 'all':
        get_table(CONV_ALL_INDIR, \
                    CONV_ALL_OUTDIR, \
                    FEATURES, \
                    TABLE_ALL_FILENAME, \
                    ARFF_ALL_OUTDIR)
    elif type == 'all-pair':
        get_table(CONV_ALL_PAIR_INDIR, \
                    CONV_ALL_OUTDIR, \
                    FEATURES, \
                    TABLE_ALL_PAIR_FILENAME, \
                    ARFF_ALL_PAIR_OUTDIR)
    elif type == 'all-pair-main':
        get_table(CONV_ALL_PAIR_MAIN_INDIR, \
                    CONV_ALL_OUTDIR, \
                    FEATURES, \
                    TABLE_ALL_PAIR_MAIN_FILENAME, \
                    ARFF_ALL_PAIR_MAIN_OUTDIR)
    else:
        print 'ERROR. Type unknown:', type

    print 'Done.'




####
#### make sure to see global FEATURES contains the features you want
####
if __name__ == "__main__":
    #### generate tables and arff files
    #run_get_tables('default')  # scenes
    #run_get_tables('pair')
    #run_get_tables('category')

    #run_get_tables('series')
    #run_get_tables('series-pair')
    #run_get_tables('series-pair-main')
    #run_get_tables('series-pair-main-trends')
    
    #run_get_tables('episode')
    #run_get_tables('episode-pair')
    #run_get_tables('episode-pair-main')

    #run_get_tables('all')      # combined into characters
    #run_get_tables('all-pair')
    run_get_tables('all-pair-main')





