from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Generate conversation and features

To run:
>python gen-conv.py > gen-conv.log
---------------------------------------------------------------
"""

import sys, time, os, glob, copy
import re, subprocess, sets
from collections import defaultdict

#///////////////////////////////////////////////////////////////////// 

#### change this to your location of tools
#### use it to call Stanford's Core NLP
#sys.path.append(os.path.join('..','..','..','..','resources'))
RESOURCE_DIR = os.path.join('..', '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common
#import word_category_counter_new.word_category_counter as liwc

# conversation directories (input)
CONV_INDIR = 'conv-input'
CONV_PAIR_INDIR = 'conv-input-pair'
CONV_CAT_INDIR = 'conv-input-category'

CONV_EPISODE_INDIR = 'conv-input-episode'
CONV_EPISODE_PAIR_INDIR = 'conv-input-episode-pair'
CONV_SERIES_INDIR = 'conv-input-series'
CONV_SERIES_PAIR_INDIR = 'conv-input-series-pair'
CONV_SERIES_PAIR_MAIN_TRENDS_INDIR = 'conv-input-series-pair-main-trends'
CONV_ALL_INDIR = 'conv-input-all'
CONV_ALL_PAIR_INDIR = 'conv-input-all-pair' 

CONV_OUTDIR = 'conv-output' 
CONV_EPISODE_OUTDIR = 'conv-output-episode'
CONV_SERIES_OUTDIR = 'conv-output-series'
CONV_SERIES_TRENDS_OUTDIR = 'conv-output-series-trends'
CONV_ALL_OUTDIR = 'conv-output-all'

POS = 'pos'
WN = 'wn'

GEN_FEAT_SCRIPT_DIR = os.path.join('..','..','..','..', \
	'tv-modeling-features', 'gen-features-scripts')

SCENE_DIR = os.path.join('..','proc-data','scenes')

SCENE_CATEGORIES = ['apartment', 'cafeteria', 'cheesecake', 'comic', \
	'office', 'stair']

SWN3_FPATH = os.path.join(os.path.realpath(RESOURCE_DIR), \
                'SentiWordNet_3.0.0', 'data', 'SentiWordNet_3.0.0.txt')

#///////////////////////////////////////////////////////////////////// 

#### script names as features

FEATURES = [#'extract-liwc',
			#'extract-concess-words-pol',
			#'extract-tag',
			#'extract-content-words',
			#'extract-verb-strength',
			#'extract-hedges',
			#'extract-verbs', # repeating verbs
			#'extract-merge',
			#'extract-words-personage',
			#'extract-pol-sentiword',   
			#'extract-dialogue-act',   
			#'extract-basic',
			#'extract-passive',
			#'extract-words',    	# wasn't here before but used in chars
			'extract-ngram2',		# same ^
			'extract-pos-ngram2'   # same ^
			]


####
#### parse line with header as dict
####
def parse_line(line, header):
	t = line.strip()
	arr = t.split('|')
	zipped = zip(header, arr)
	t_arr = dict((x,y) for x, y in zipped)
	return t_arr


####
#### get conversation from scenes
####
def get_conv_from_scene():
	files = os.listdir(SCENE_DIR)
	#print files[0:5]

	chars_all = defaultdict(list)
	chars_all_pair = defaultdict(list)

	series_all = defaultdict(dict)
	series_all_pair = defaultdict(dict)

	series_episode = defaultdict(dict)
	series_episode_pair = defaultdict(dict)

	for f in files:
		if f[-4:]!='.txt': continue
		fname = re.sub('.txt', '',f)
		print fname
		arr = fname.split('-')
		# get info on series, episode, scene number, and scene description
		series = arr[1]
		episode = arr[3]
		scene_num = arr[4]
		scene_desc = arr[5]

		# characters present
		chars_dial = defaultdict(list)
		lines = open(os.path.join(SCENE_DIR,)).readlines()
		for line in lines:
			# dialogue separate by ':'
			if ':' not in line: continue
			tmp = line.strip().split(':')
			char = tmp[0].strip().lower()
			if '(' in char:
				char = char.split('(')[0].strip()
			char = re.sub('\'', ' ', char)
			char = re.sub('\. ', ' ', char)
			dial = tmp[1].strip()
			chars_dial[char].append(dial)


		# write to output files for each character (in presence of others)
		all_chars = chars_dial.keys()

		for c in sorted(chars_dial.keys()):
			remain_chars = [item for item in all_chars if item not in [c]]
			#print '\t', c, remain_chars
			cc = '-char-'+c+'-inpresenceof-'+'-'.join(remain_chars)

			ftmp = fname+cc
			ftmp = common.proc_str(ftmp)
			fout = ftmp+'.txt'
			
			outf = open(os.path.join(CONV_INDIR,fout), 'w')
			outf.write('\n'.join(chars_dial[c])+'\n')
			outf.close()

			# track by episodes
			k = 'series-'+series+'-episode-'+episode
			if cc not in series_episode[k].keys():
				series_episode[k][cc] = copy.deepcopy(chars_dial[c])
			else:
				for item in chars_dial[c]:
					series_episode[k][cc].append(item)

			# track by series
			k = 'series-'+series
			if cc not in series_all[k].keys():
				series_all[k][cc] = copy.deepcopy(chars_dial[c])
			else:
				for item in chars_dial[c]:
					series_all[k][cc].append(item)
			
			# track by speaker-addressee
			if cc not in chars_all.keys():
				chars_all[cc] = copy.deepcopy(chars_dial[c])
			else:
				for item in chars_dial[c]:
					chars_all[cc].append(item)
			
			# put into categories as well
			for cat in SCENE_CATEGORIES:
				if cat in fout:
					catdir = os.path.join(CONV_CAT_INDIR,cat)
					outf = open(os.path.join(catdir,fout), 'w')
					outf.write('\n'.join(chars_dial[c])+'\n')
					outf.close()
			

			# if there are only two characters, also write to a separate directory
			if len(all_chars)==2:
				
				outf = open(os.path.join(CONV_PAIR_INDIR,fout),'w')
				outf.write('\n'.join(chars_dial[c])+'\n')
				outf.close()

				# track by episodes
				k = 'series-'+series+'-episode-'+episode
				if cc not in series_episode_pair[k].keys():
					series_episode_pair[k][cc] = copy.deepcopy(chars_dial[c])
				else:
					for item in chars_dial[c]:
						series_episode_pair[k][cc].append(item)
				# track by series
				k = 'series-'+series
				if cc not in series_all_pair[k].keys():
					series_all_pair[k][cc] = copy.deepcopy(chars_dial[c])
				else:
					for item in chars_dial[c]:
						series_all_pair[k][cc].append(item)
				
				# track by speaker-addressee
				if cc not in chars_all_pair.keys():
					chars_all_pair[cc] = copy.deepcopy(chars_dial[c])
				else:
					for item in chars_dial[c]:
						chars_all_pair[cc].append(item)

				# put into categories as well
				
				for cat in SCENE_CATEGORIES:
					if cat in fout:
						catdir = os.path.join(CONV_CAT_INDIR,cat+'-pair')
						outf = open(os.path.join(catdir,fout), 'w')
						outf.write('\n'.join(chars_dial[c])+'\n')
						outf.close()
				

	# write to episode
	for key in series_episode.keys():
		for char in series_episode[key].keys():
			fout = open(os.path.join(CONV_EPISODE_INDIR,key+char+'.txt'), 'w')
			fout.write('\n'.join(series_episode[key][char])+'\n')
			fout.close()

	for key in series_episode_pair.keys():
		for char in series_episode_pair[key].keys():
			fout = open(os.path.join(CONV_EPISODE_PAIR_INDIR,key+char+'.txt'), 'w')
			fout.write('\n'.join(series_episode_pair[key][char])+'\n')
			fout.close()            

	# write to series
	for key in series_all.keys():
		for char in series_all[key].keys():
			fout = open(os.path.join(CONV_SERIES_INDIR,key+char+'.txt'), 'w')
			fout.write('\n'.join(series_all[key][char])+'\n')
			fout.close()

	for key in series_all_pair.keys():
		for char in series_all_pair[key].keys():
			fout = open(os.path.join(CONV_SERIES_PAIR_INDIR,key+char+'.txt'), 'w')
			fout.write('\n'.join(series_all_pair[key][char])+'\n')
			fout.close()
	
	# write to all speaker-addressee
	for key in chars_all.keys():
		fout = open(os.path.join(CONV_ALL_INDIR,key+'.txt'), 'w')
		fout.write('\n'.join(chars_all[key])+'\n')
		fout.close()

	for key in chars_all_pair.keys():
		fout = open(os.path.join(CONV_ALL_PAIR_INDIR,key+'.txt'), 'w')
		fout.write('\n'.join(chars_all_pair[key])+'\n')
		fout.close()



####
#### From conversation files, get features
#### Note: for extract-pol-sentiword, the extra argument is assumed to be
#### a directory with attached '-sent'
####

def get_conv_features(indir, outdir, features):
	print '----> Note: will cd into and out of', outdir
	for feat in features:
		cmd1 = 'cd '+outdir

		if feat in ['extract-pol-sentiword']:
			# takes additional directory (with attached '-sent') as input
			# use wordnet tagged input files
			cmd2 = 'python '+ \
				os.path.join(GEN_FEAT_SCRIPT_DIR, feat+'.py')+' '+\
				SWN3_FPATH+' '+ \
				os.path.join('..',WN,WN,indir)+' '+ \
				os.path.join('.',feat)+' '+ \
				os.path.join('.', feat+'-sent') # sentences

		elif feat in ['extract-basic', 'extract-concess-words-pol', \
						'extract-tag', 'extract-content-words', \
						'extract-verb-strength', 'extract-hedges', \
						'extract-verbs', 'extract-dialogue-act', \
						'extract-words-personage', 'extract-merge']:
			# takes additional directory: pos
			cmd2 = 'python '+ \
				os.path.join(GEN_FEAT_SCRIPT_DIR, feat+'.py')+' '+\
				os.path.join('..',indir,indir+'.pickle')+' '+ \
				os.path.join('..',POS,indir+'.pickle')+' '+ \
				os.path.join('.',feat)

	   	elif feat in ['extract-words']:
			cmd2 = 'python '+ \
				os.path.join(GEN_FEAT_SCRIPT_DIR, feat+'.py')+' '+\
				os.path.join('..',indir, indir+'.pickle')+' '+ \
				os.path.join('.',feat)

		elif 'extract-ngram' in feat:
			# e.g., 'extract-ngram2'
			m = re.match('(.+)(\d+)', feat)
			feat2 = m.group(1).strip()
			ngram = m.group(2).strip()
			cmd2 = 'python '+ \
				os.path.join(GEN_FEAT_SCRIPT_DIR, feat2+'.py')+' '+\
				os.path.join('..',indir, indir+'.pickle')+' '+ \
				os.path.join('.',feat)+' '+ \
				ngram

		elif 'extract-pos-ngram' in feat:
			# e.g., 'extract-ngram2'
			m = re.match('(.+)(\d+)', feat)
			feat2 = m.group(1).strip()
			ngram = m.group(2).strip()
			cmd2 = 'python '+ \
				os.path.join(GEN_FEAT_SCRIPT_DIR, feat2+'.py')+' '+\
				os.path.join('..',indir,indir+'.pickle')+' '+ \
				os.path.join('..',POS,indir+'.pickle')+' '+ \
				os.path.join('.',feat)+' '+ \
				ngram

		elif 'extract-liwc' in feat:
			liwc_file = os.path.join('liwc-2007-output', 'LIWC results.txt')
			cmd2 = 'python '+ \
				os.path.join(GEN_FEAT_SCRIPT_DIR, feat+'.py')+' '+\
				'"'+liwc_file+'" '+ \
				os.path.join('.',feat)

		else:
			cmd2 = 'python '+ \
				os.path.join(GEN_FEAT_SCRIPT_DIR,feat+'.py')+' '+ \
				os.path.join('..',indir, indir)+' '+ \
				os.path.join('.',feat) # each feature has its own dir

		# run commands
		cmd3 = 'cd ..'
		result=subprocess.check_output(cmd1+';'+cmd2+';'+cmd3, shell=True)
		print result




####
#### Get POS and write to output files
#### Directories and corresponding files have "-pos" attached
####
def get_pos(indirs):
	print 'Get POS (PENN) and write to file...'
	#indirs = [CONV_INDIR, \
	#            CONV_MAIN_PAIRS_INDIR, \
	#            CONV_MAIN_PAIRS_EPISODE_INDIR, \
	#            CONV_MAIN_PAIRS_SERIES_INDIR, \
	#            CONV_MAIN_PAIRS_SCENE_INDIR]
	for indir in indirs:
		listing = os.listdir(indir)
		for infile in listing:
			#print "current file is: " + infile
			if infile[0]=='.' or infile[-4:]!='.txt':
				print 'Incorrect file format and skip:', infile
				continue
			outfile = os.path.join(POS,indir, re.sub('.txt','.pos.txt',infile))
			common.get_and_write_pos_doc(os.path.join(indir,infile), outfile)
	print 'Done.'



####
#### convert PENN (after running get_pos()) to WORDNET tags
####
def convert_penn2wn(indirs):
	#convert-PENN-to-WN.pl --input_dir <dir> --output_dir <dir>
	print 'Convert POS (PENN) to WordNet tagged and write to file...'
	#indirs = [CONV_INDIR, \
	#            CONV_MAIN_PAIRS_INDIR, \
	#            CONV_MAIN_PAIRS_EPISODE_INDIR, \
	#            CONV_MAIN_PAIRS_SERIES_INDIR, \
	#            CONV_MAIN_PAIRS_SCENE_INDIR]
	for indir in indirs:
		common.penn2wn_dirs(os.path.join(POS,indir), os.path.join(WN,indir))
	print 'Done.'






####
#### running with main characters' paired conversation
####
def run_get_conversations():
	print 'Getting conversations...'
	get_conv_from_scene()
	print 'Done.'


####
#### Go get/run features for conversations
####
def run_get_features(type):
	print 'Getting features with type', type, '...'
	if type == 'default':
		get_conv_features(CONV_INDIR, \
							CONV_OUTDIR, \
							FEATURES)
	elif type == 'series':
		get_conv_features(CONV_SERIES_INDIR, \
							CONV_SERIES_OUTDIR, \
							FEATURES)
	elif type == 'series-trends':
		get_conv_features(CONV_SERIES_PAIR_MAIN_TRENDS_INDIR, \
							CONV_SERIES_TRENDS_OUTDIR, \
							FEATURES)
	elif type == 'episode':
		get_conv_features(CONV_EPISODE_INDIR, \
							CONV_EPISODE_OUTDIR, \
							FEATURES)
	elif type == 'all':
		get_conv_features(CONV_ALL_INDIR, \
							CONV_ALL_OUTDIR, \
							FEATURES)        
	else:
		print 'ERROR. type not found:', type
	print 'Done.'



####
#### make sure to see global FEATURES contains the features you want
####
if __name__ == "__main__":
	#run_get_conversations() 

	#### get pos and convert penn to wordnet
	# This assumes all conv_input_* are populated
	# (from running previous function calls)
	# Note: this might take a while to run since it runs Stanford's corenlp
	#indirs = [CONV_SERIES_PAIR_TRENDS_INDIR] #[CONV_INDIR] #CONV_ALL_INDIR]  #[CONV_EPISODE_INDIR] #CONV_INDIR
	#get_pos(indirs)
	#convert_penn2wn(indirs)

	#### get features
	#### NOTE: run LIWC software before getting the LIWC features

	#run_get_features('default') # scenes
	#run_get_features('series') 
	#run_get_features('episode')
	run_get_features('all') # combined to characters;
	#run_get_features('series-trends')



