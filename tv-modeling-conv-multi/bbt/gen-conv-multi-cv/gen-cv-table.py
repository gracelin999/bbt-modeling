from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Generate table
---------------------------------------------------------------
"""

import sys, time, os, glob, copy
import re, subprocess, sets, pickle
from collections import defaultdict

#///////////////////////////////////////////////////////////////////// 

#### change this to your location of tools
#### including call to Stanford's Core NLP
#RESOURCE_DIR = '/Users/grace/dialogue/repos'
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common

# get features' information
SCRIPTS_DIR = os.path.join(THISDIR, '..','..', '..', \
                        'tv-modeling-features', 'gen-features-scripts')
sys.path.append(SCRIPTS_DIR)
import getfeatsinfo

FOLDS = 5

CONV_MULTI_INDIR = 'conv-input-all-pair-main-cv'
CONV_MULTI_OUTDIR = 'conv-output-cv'

POS = 'pos-cv'
WN = 'wn-cv'

# create tables
TABLE_FILENAME = 'gen-cv-conv-multi-all-pair-main-table.txt'

#///////////////////////////////////////////////////////////////////// 

#### script names as features
FEATURES = ['extract-liwc', 
            'extract-concess-words-pol',
            'extract-tag',
            'extract-content-words',
            'extract-verb-strength',
            'extract-hedges',
            'extract-verbs',
            'extract-merge',
            'extract-words-personage',
            'extract-pol-sentiword',   
            'extract-dialogue-act',
            'extract-basic',
            'extract-passive',
            'extract-words',
            'extract-ngram2',
            'extract-pos-ngram2'
            ]



def process_attributes(inst, feats_all):
    existing_feats = []
    existing_insts = {}
    for fset in inst:
        existing_feats += fset.keys()
        existing_insts = dict(existing_insts.items()+fset.items())

    s1 = set(existing_feats)
    s2 = set(feats_all)
    add_feats = s2.difference(s1)
    #print add_feats
    new_inst = existing_insts
    for feat in add_feats:  # add these features and give them zeros
        if feat in existing_insts.keys():
            print "ERROR!"
        new_inst[feat] = 0.0
    return new_inst


####
#### write table
####
def write_table(inst_all, feats_all, tablefile):
    """ Example:
        key: zack
        value: (sets of features)
                [{'liwc-Anxiety': 0.242130750605, 'liwc-Time': 6.97674418605},
                {'hedges-per-sent': 1.0},
                {'merge-ratio': 0.0}]
    """
    OUTF = open(tablefile,'w')
    OUTF.write('speaker')
    # sorted features
    for f in sorted(feats_all):
        feat = common.proc_str(f)      # generic process of string
        OUTF.write('|'+feat)
    OUTF.write('\n')

    inst_by_speaker = defaultdict(list)
    speaker_to_shortname = defaultdict(str)

    for char in sorted(inst_all.keys()):
        speaker = common.proc_str(char)
        if ('series' not in char) and ('episode' not in char) and \
            ('scene' not in char):
            # default - characters only
            ctemp = char
        else:
            arr = char.split('-')
            if arr[0]=='series':
                ctemp = arr[-1] # last entry is the name
            else:
                ctemp = arr[0] # 1st entry is the name
        shortname = common.proc_str(ctemp)
        # save the mapping
        speaker_to_shortname[speaker] = shortname
        # create and store instance
        new_inst = process_attributes(inst_all[char], feats_all)
        inst_by_speaker[speaker].append(new_inst)

    main_chars = ['sheldon', 'leonard', 'penny', 'howard', 'raj', 'amy', 'bernadette']

    for char in inst_by_speaker.keys():
        attrs = inst_by_speaker[char][0] # there's only one item: a list of attrs
        inst = []
        # go through each attribute
        #print attrs
        for f in sorted(attrs.keys()):
            inst.append(str(attrs[f]))
        # also write to a regular table
        OUTF.write(char+'|'+'|'.join(inst)+'\n')
        # log
        #print 'speaker:', char, len(inst_by_speaker[char]), len(attrs)
        #print '\tNumber of feats:', len(attrs)

    OUTF.close()



####
#### collect all the features and write to a table
####
def get_table(indir, outdir, features, tablefile):

    # features to get
    # table file to write
    #OUTF = open(TABLE_FILENAME,'w')
    #OUTF.write('pair|'+'|'.join(features)+'\n')
    print 'get_table indir:', indir
    print 'get_table outdir:', outdir

    # input is in pickle file
    # key = file name (e.g., jimmy.txt)
    input_info = pickle.load(open(indir+'.pickle'))
    
    # go through each pair of conversation
    conv_pairs = input_info.keys()
    feats_all = defaultdict(int)    # count features being used
    inst_all = defaultdict(list)    # store all instances for all conv. pairs
    
    for cp in conv_pairs:
        # make sure it's a text file
        if cp[-4:] != '.txt':
            continue
        # remove extension
        p = re.sub('.txt','',cp)
        #OUTF.write(p)
        #print outdir, p
        
        fset_all = []    # collect all feature sets

        for feat in features:

            output_info = pickle.load(open(os.path.join(outdir, feat+'.pickle')))  
            
            #listing = os.listdir(outdir)
            #print outdir, feat, p+'.txt.out'
            if feat == 'extract-pol-sentiword':
                ext = '.pos.wn.senti.txt'
            else:
                ext = '.txt.out'

            feat_fname = p+ext

            # TODO: skip the following file for now because of empty scene
            # need to fix it at proper place
            fskip = "series-1-episode-05-5-in-leonard's-bedroom.txt.out"
            if feat_fname == fskip:
                print 'NOTE: skip file', fskip, 'because of empty scene'
                continue

            # check if it exists
            #if os.path.isfile(fpath)==False:
            if feat_fname not in output_info:
                print 'NOTE: Feat output does not exist:', feat_fname
                continue

            # check its size
            #size = os.stat(fpath).st_size
            feat_content = output_info[feat_fname]
            if not feat_content:
                print 'NOTE: Empty feat output:', feat, feat_fname
                continue

            # send content to get feat info
            fset = getfeatsinfo.get_feature_info(feat_content, feat)
            if fset:
                fset_all.append(fset)
                for k in fset.keys():
                    feats_all[k] += 1   # get feature name

        inst_all[p]=copy.deepcopy(fset_all)
        #if 'zack-amy' in inst_all.keys():
        #    print p, inst_all[p]
        #    print 'zack-amy', inst_all['zack-amy']

    write_table(inst_all, feats_all.keys(), tablefile)





####
#### make sure to see global FEATURES contains the features you want
####
if __name__ == "__main__":

    #### create table for each
    for i in range(FOLDS):
        """
        indir  = os.path.join(CONV_MULTI_INDIR,str(i),'train')
        outdir = os.path.join(CONV_MULTI_OUTDIR,str(i),'train')
        posdir = os.path.join(POS,str(i),'train') 
        wndir  = os.path.join(WN,str(i),'train')
        table_fname = re.sub('.txt','-'+str(i)+'.txt',TABLE_FILENAME)
        get_table(indir, outdir, FEATURES, table_fname)
        """

        indir  = os.path.join(CONV_MULTI_INDIR,str(i),'test')
        outdir = os.path.join(CONV_MULTI_OUTDIR,str(i),'test')
        posdir = os.path.join(POS,str(i),'test') 
        wndir  = os.path.join(WN,str(i),'test')
        table_fname = re.sub('.txt','-test-'+str(i)+'.txt',TABLE_FILENAME)
        get_table(indir, outdir, FEATURES, table_fname)






