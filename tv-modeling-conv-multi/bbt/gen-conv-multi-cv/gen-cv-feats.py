from __future__ import division     # for division to behave properly

"""
---------------------------------------------------------------
Generate features for cv directories.
---------------------------------------------------------------
"""

import sys, time, os, copy
import re, subprocess, sets, pickle
from collections import defaultdict

#///////////////////////////////////////////////////////////////////// 
THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
import common

FOLDS = 5

GEN_FEAT_SCRIPT_DIR = os.path.join(THISDIR, '..','..', '..', \
                        'tv-modeling-features', 'gen-features-scripts')

CONV_MULTI_INDIR = 'conv-input-all-pair-main-cv'
CONV_MULTI_OUTDIR = 'conv-output-cv'

POS = 'pos-cv'
WN = 'wn-cv'

SWN3_FPATH = os.path.join(os.path.realpath(RESOURCE_DIR), \
                'SentiWordNet_3.0.0', 'data', 'SentiWordNet_3.0.0.txt')

#///////////////////////////////////////////////////////////////////// 

#### features to extract; same as script name
FEATURES = [#'extract-liwc',
            # 'extract-concess-words-pol',
            # 'extract-tag',
            # 'extract-content-words',
            # 'extract-verb-strength',
            # 'extract-hedges',
            # 'extract-verbs', # repeating verbs
            # 'extract-merge',
            # 'extract-words-personage',
            # 'extract-pol-sentiword',
            # 'extract-dialogue-act',
             'extract-basic',
            # 'extract-passive',
            # 'extract-words',
            # 'extract-ngram2',
            # 'extract-pos-ngram2'
            ]

####
#### From characters' files, get features
#### Note: for extract-pol-sentiword, the extra argument is assumed to be
#### a directory with attached '-sent'
####

def get_chars_features(indir, outdir, posdir, wndir, features):
    print '----> Note: will cd into and out of', outdir

    for feat in features:
        # make sure the feat directory exists 
        feat_dir = os.path.join(outdir,feat)
        common.create_directory(feat_dir)

        cmd1 = 'cd '+outdir

        if feat == 'extract-pol-sentiword':
            # use input directory rather than pickle file
            # takes additional directory (with attached '-sent') as input
            # use wordnet tagged input files
            
            # make sure the '-sent' directory exists
            sent_dir = os.path.join(outdir,feat+'-sent')
            common.create_directory(sent_dir)

            cmd2 = 'python '+ \
                os.path.join(GEN_FEAT_SCRIPT_DIR, feat+'.py')+' '+\
                SWN3_FPATH+' '+ \
                os.path.join(THISDIR, wndir)+' '+ \
                os.path.join('.',feat)+' '+ \
                os.path.join('.', feat+'-sent')     # sentences

        elif feat in ['extract-basic', 'extract-concess-words-pol', \
                        'extract-tag', 'extract-content-words', \
                        'extract-verb-strength', 'extract-hedges', \
                        'extract-verbs', 'extract-dialogue-act', \
                        'extract-words-personage', 'extract-merge']:
            # takes additional directory: pos
            cmd2 = 'python '+ \
                os.path.join(GEN_FEAT_SCRIPT_DIR, feat+'.py')+' '+\
                os.path.join(THISDIR, indir+'.pickle')+' '+ \
                os.path.join(THISDIR, posdir+'.pickle')+' '+ \
                os.path.join('.',feat)

        elif feat in ['extract-words']:
            cmd2 = 'python '+ \
                os.path.join(GEN_FEAT_SCRIPT_DIR, feat+'.py')+' '+\
                os.path.join(THISDIR, indir+'.pickle')+' '+ \
                os.path.join('.',feat)

        elif 'extract-ngram' in feat:
            # e.g., 'extract-ngram2'
            m = re.match('(.+)(\d+)', feat)
            feat2 = m.group(1).strip()
            ngram = m.group(2).strip()
            cmd2 = 'python '+ \
                os.path.join(GEN_FEAT_SCRIPT_DIR, feat2+'.py')+' '+\
                os.path.join(THISDIR, indir+'.pickle')+' '+ \
                os.path.join('.',feat)+' '+ \
                ngram

        elif 'extract-pos-ngram' in feat:
            # e.g., 'extract-ngram2'
            m = re.match('(.+)(\d+)', feat)
            feat2 = m.group(1).strip()
            ngram = m.group(2).strip()
            cmd2 = 'python '+ \
                os.path.join(GEN_FEAT_SCRIPT_DIR, feat2+'.py')+' '+\
                os.path.join(THISDIR, indir+'.pickle')+' '+ \
                os.path.join(THISDIR, posdir+'.pickle')+' '+ \
                os.path.join('.',feat)+' '+ \
                ngram

        elif 'extract-liwc' in feat:
            # make sure liwc actually exists
            liwc_file = os.path.join(THISDIR, outdir, \
                            'liwc-2007-output', 'LIWC results.txt')
            if not os.path.exists(liwc_file):
                print 'ERROR. File not exist:', liwc_file, '\n'
                continue

            cmd2 = 'python '+ \
                os.path.join(GEN_FEAT_SCRIPT_DIR, feat+'.py')+' '+\
                '"'+liwc_file+'" '+ \
                os.path.join('.',feat)

        else:
            cmd2 = 'python '+ \
                os.path.join(GEN_FEAT_SCRIPT_DIR,feat+'.py')+' '+ \
                os.path.join(THISDIR,indir)+' '+ \
                os.path.join('.',feat) # each feature has its own dir
        
        # run commands
        cmd3 = 'cd ..'
        result=subprocess.check_output(cmd1+';'+cmd2+';'+cmd3, shell=True)
        print result




####
#### Get POS and write to output files
#### Directories and corresponding files have "-pos" attached
####
def get_pos(indir, outdir):
    print 'Get POS (PENN):', indir, outdir
    listing = os.listdir(indir)
    for infile in listing:
        #print "current file is: " + infile
        if infile[0]=='.' or infile[-4:]!='.txt':
            print 'Incorrect file format and skip:', infile
            continue
        outfile = os.path.join(outdir,re.sub('.txt','.pos.txt',infile))
        common.get_and_write_pos_doc(indir+os.sep+infile, outfile)
    print 'Done.'



####
#### convert PENN (after running get_pos()) to WORDNET tags
####
def convert_penn2wn(pos_dir, wn_dir):
    #convert-PENN-to-WN.pl --input_dir <dir> --output_dir <dir>
    print 'Convert POS (PENN) to WN tagged:', pos_dir, wn_dir
    common.penn2wn_dirs(pos_dir, wn_dir)
    print 'Done.'


####
#### make sure to see global FEATURES contains the features you want
####
if __name__ == "__main__":

    #### get features
    for i in [1]: #range(FOLDS):
        """
        indir  = os.path.join(CONV_MULTI_INDIR,str(i),'train')
        outdir = os.path.join(CONV_MULTI_OUTDIR,str(i),'train')
        posdir = os.path.join(POS,str(i),'train') 
        wndir  = os.path.join(WN,str(i),'train')
        get_chars_features(indir, outdir, posdir, wndir, FEATURES)
        """

        indir  = os.path.join(CONV_MULTI_INDIR,str(i),'test')
        outdir = os.path.join(CONV_MULTI_OUTDIR,str(i),'test')
        posdir = os.path.join(POS,str(i),'test') 
        wndir  = os.path.join(WN,str(i),'test')
        get_chars_features(indir, outdir, posdir, wndir, FEATURES)       






