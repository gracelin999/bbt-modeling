
import os, sys
import random, re, pickle
from shutil import copyfile, rmtree

#///////////////////////////////////////////////////////////////////// 

THISDIR = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.join(THISDIR, '..', '..', '..', 'resources')
sys.path.append(RESOURCE_DIR)
from common import create_directory


CONV_MULTI_INDIR = 'conv-input-all-pair-main-cv'
CONV_MULTI_INDIR_ORIG = os.path.join(CONV_MULTI_INDIR, 'conv-input-all-pair-main')
POS_DIR = 'pos-cv'
POS_DIR_ORIG = os.path.join(POS_DIR,'pos')
WN_DIR = 'wn-cv'
WN_DIR_ORIG = os.path.join(WN_DIR,'wn')

SEED = 42	# for random number
FOLDS = 5


#///////////////////////////////////////////////////////////////////// 

# make sure directories are created
for i in range(FOLDS):
	test_dir = os.path.join(CONV_MULTI_INDIR,str(i),'test')
	train_dir = os.path.join(CONV_MULTI_INDIR,str(i),'train')
	create_directory(test_dir)
	create_directory(train_dir)

	# also include utt test
	utt_test_dir = os.path.join(CONV_MULTI_INDIR,str(i),'test-utt')
	if os.path.isdir(utt_test_dir):
		rmtree(utt_test_dir)
	create_directory(utt_test_dir)

	# pos
	pos_test_dir = os.path.join(POS_DIR,str(i),'test')
	pos_train_dir = os.path.join(POS_DIR,str(i),'train')
	create_directory(pos_test_dir)
	create_directory(pos_train_dir)
	# wn
	wn_test_dir = os.path.join(WN_DIR,str(i),'test')
	wn_train_dir = os.path.join(WN_DIR,str(i),'train')
	create_directory(wn_test_dir)
	create_directory(wn_train_dir)



def get_test_train(idx_start, idx_end, idx_shuffle, lines):
	portion_test_idx = idx_shuffle[idx_start:idx_end]
	portion_train_idx = idx_shuffle[0:idx_start]+idx_shuffle[idx_end:]
	portion_test = [lines[j] for j in portion_test_idx] 
	portion_train = [lines[j] for j in portion_train_idx] 
	return portion_test, portion_train





def check_utt_files(lines, f):

	err_str = ''

	utt_lines = open(os.path.join('..','gen-conv-multi-utt', \
				'conv-input-all-pair-main', f)).read().split('\n')
	
	if len(lines) != len(utt_lines):
		err_str += 'ERROR. lines and utt_lines not same size: '+ \
					str(len(lines))+', '+str(len(utt_lines))

	for i, utt_line in enumerate(utt_lines):
		if not utt_line: continue

		uttf = re.sub('.txt','-'+str(i)+'.txt',f)

		arr = open(os.path.join('..','gen-conv-multi-utt', \
				'conv-input-all-pair-main-utt',uttf)).read().split('\n')
		if len(arr)!= 1:
			err_str += 'ERROR. Utt file contains > 1 line: line '+str(i)+'\n'

		uttf_line = arr[0]

		if utt_line!=uttf_line:
			err_str += 'ERROR. Mismatch line: '+str(i)+'\n'
			err_str += '\t utt_line: '+utt_line+'\n'
			err_str += '\t uttf_line: '+uttf_line+'\n'

	return err_str


def get_utt_files(idx_start, idx_end, idx_shuffle, lines, f, fold):
	# sanity check on the matching of utterances
	err = check_utt_files(lines, f)
	if err:
		print err
		return

	# no error found; continue
	portion_test_idx = idx_shuffle[idx_start:idx_end]

	for idx in portion_test_idx:
		line = lines[idx]
		if not line: continue

		uttf = re.sub('.txt','-'+str(idx)+'.txt',f)
		src = os.path.join('..','gen-conv-multi-utt','conv-input-all-pair-main-utt',uttf)
		dst = os.path.join(CONV_MULTI_INDIR, str(fold),'test-utt',uttf)
		copyfile(src,dst)



for f in os.listdir(CONV_MULTI_INDIR_ORIG):
	if f[0]=='.': continue # skip hidden files

	print '---- splitting content to folds:', f

	lines = open(os.path.join(CONV_MULTI_INDIR_ORIG,f)).read()
	lines = lines.split('\n')

	pos_f = re.sub('.txt','.pos.txt',f)
	lines_pos = open(os.path.join(POS_DIR_ORIG, pos_f)) .read()
	lines_pos = lines_pos.split('\n')

	wn_f = re.sub('.txt','.pos.wn.txt',f)
	lines_wn = open(os.path.join(WN_DIR_ORIG, wn_f)).read()
	lines_wn = lines_wn.split('\n')

	print 'num lines, pos, wn:', len(lines), len(lines_pos), len(lines_wn), \
			len(lines)==len(lines_pos)==len(lines_wn)

	if not len(lines)==len(lines_pos)==len(lines_wn):
		print 'ERROR. Number of lines do not match across files.'
		continue

	# shuffle indices
	idx_shuffle = range(len(lines))
	random.seed(SEED)
	random.shuffle(idx_shuffle)

	# split indices
	size = len(lines)/FOLDS
	
	total = 0

	for i in range(FOLDS):	
		# get start/end index
		idx_start = i*size
		if i==(FOLDS-1):
			# last portion; make sure to get all the rest
			idx_end = len(lines)
		else:
			idx_end = (i+1)*size

		get_utt_files(idx_start, idx_end, idx_shuffle, lines, f, i)
		
		# get corresponding test/train sets
		portion_test, portion_train = \
			get_test_train(idx_start, idx_end, idx_shuffle, lines)
		portion_test_pos, portion_train_pos = \
			get_test_train(idx_start, idx_end, idx_shuffle, lines_pos)
		portion_test_wn, portion_train_wn = \
			get_test_train(idx_start, idx_end, idx_shuffle, lines_wn)

		print 'fold,idx start,end,test_size, train_size:', \
			i, idx_start, idx_end, len(portion_test), len(portion_train), \
			len(portion_test)+len(portion_train)==len(lines)

		total += len(portion_test)

		# write to fold directory
		test_dir = os.path.join(CONV_MULTI_INDIR,str(i),'test')
		test_f = os.path.join(test_dir, f)
		train_dir = os.path.join(CONV_MULTI_INDIR,str(i),'train')
		train_f = os.path.join(train_dir, f)
		open(test_f, 'wb').write('\n'.join(portion_test)+'\n')
		open(train_f, 'wb').write('\n'.join(portion_train)+'\n')

		# write to pos fold directory
		test_pos_dir = os.path.join(POS_DIR,str(i),'test') 
		test_pos_f = os.path.join(test_pos_dir, pos_f)
		train_pos_dir = os.path.join(POS_DIR,str(i),'train') 
		train_pos_f = os.path.join(train_pos_dir, pos_f)
		open(test_pos_f, 'wb').write('\n'.join(portion_test_pos)+'\n')
		open(train_pos_f, 'wb').write('\n'.join(portion_train_pos)+'\n')

		# write to pos fold directory (train only)
		test_wn_dir = os.path.join(WN_DIR,str(i),'test')
		test_wn_f = os.path.join(test_wn_dir, wn_f)
		train_wn_dir = os.path.join(WN_DIR,str(i),'train')
		train_wn_f = os.path.join(train_wn_dir, wn_f)
		open(test_wn_f, 'wb').write('\n'.join(portion_test_wn)+'\n')
		open(train_wn_f, 'wb').write('\n'.join(portion_train_wn)+'\n')

	print 'total portion:', total, total==len(lines)
		

# write to pickle files

for maindir in [CONV_MULTI_INDIR, POS_DIR, WN_DIR]:
	for i in range(FOLDS):
		for t in ['test', 'train', 'test-utt']:
			content = {}
			t_dir = os.path.join(maindir,str(i),t)
			if not os.path.isdir(t_dir): continue

			for f in os.listdir(t_dir):
				c = open(os.path.join(t_dir,f)).read()
				content[f]=c
			# write to pickle
			pname = t+'.pickle'
			pickle.dump(content, open(os.path.join(maindir,str(i),pname),'wb'))





