####
#### common functions
####
import sys, os, subprocess, re, string, copy

#RESOURCE_DIR = '/Users/grace/dialogue/repos/resources/'
#sys.path.append(RESOURCE_DIR+'tools')
from tools import stanford_corenlp


THISDIR = os.path.dirname(os.path.realpath(__file__))

# located in current directory
SENTIWORDNET = os.path.join(THISDIR,'SentiWordNet_3.0.0','data','SentiWordNet_3.0.0.txt')

####
#### process file form pos/ rather than re-parse
####
def process_posfile(line):
    # return this format:
    # words:  ['if', 'it', "'s", 'unobserved', 'it', 'will', ','] 
    # tagged: [('if', 'IN'), ('it', 'PRP'), ("'s", 'VBZ'), ('unobserved', 'JJ'), ('it', 'PRP'), ('will', 'MD'), (',', ',')]
    tagged = []
    tokens = []
    sents = []
    
    arr = line.split()

    tag = []
    token = []
    sent = ''

    prevpos = ''
    found_quote = False

    wordseq = ''

    for i in range(len(arr)):

        item = arr[i].strip()
        arr2 = item.split('/')
        #print arr2

        if len(arr2)>2:
            # e.g., TCP/IP/NN
            word = '/'.join(arr2[:-1]).strip()
            pos = arr2[-1].strip()
        elif len(arr2)==1:
            # e.g., . . ./: 
            # go to next one until there's a '/'
            wordseq += arr2[0].strip()
            continue
        else:
            if wordseq != '':
                wordseq += arr2[0].strip()
                word = wordseq
                wordseq = ''
                print 'NOTE: found word sequence and pos:', \
                    word, arr2[1].strip(), line
            else:
                word = arr2[0].strip()
            pos = arr2[1].strip()


        if (prevpos=='.' and pos!='.' and pos!='-RRB-' and pos!="''") or \
            (prevpos=='.' and pos=='.' and i==(len(arr)-1)):
            sents.append(sent.strip())
            sent = ''
            tagged.append(copy.deepcopy(tag))
            tag = []
            tokens.append(copy.deepcopy(token))
            token = []



        tag.append((word, pos))
        token.append(word)
        
        if len(token)==1 and token[0] in string.punctuation:
            sent += word
        elif (pos=="``") and found_quote==False:
            sent += ' '+word
            found_quote = True
        elif (pos=="''") and found_quote==True:
            sent += word
            found_quote = False
        elif len(sent)>0 and sent[-2:]==' $' and re.search(r'^\d+', word):
            sent += word
        elif word == '$':
            sent += ' '+word
        elif pos in string.punctuation or word in string.punctuation or \
            word == "n't" or (word[0] == "'" and word[1] !="'"):
            sent += word
        else:
            sent += ' '+word

        if (i==(len(arr)-1)) or (prevpos=='.' and (pos=='-RRB-' or pos=="''")): # last item
            sents.append(sent.strip())
            sent = ''
            tagged.append(copy.deepcopy(tag))
            tag = []
            tokens.append(copy.deepcopy(token))
            token = []

        prevpos = pos
            
    return tokens, tagged, sents



def posfile_sents_check(line, posfile_sents):
    # sanity check - should match with line
    linex = re.sub(' ', '', line)
    poslinex = re.sub(' ', '', ''.join(posfile_sents))

    linex = re.sub(r'(``|\'\')','"', linex)
    linex = re.sub('<','{', linex)
    linex = re.sub('>','}', linex)

    poslinex = re.sub(r'(``|\'\')','"', poslinex)

    if linex == poslinex:
        correct = True
    else:
        correct = False

    return correct, linex, poslinex



####
#### combine features
####
def process_attributes(inst, feats_all):
    existing_feats = []
    existing_insts = {}
    for fset in inst:
        existing_feats += fset.keys()
        existing_insts = dict(existing_insts.items()+fset.items())

    s1 = set(existing_feats)
    s2 = set(feats_all)
    add_feats = s2.difference(s1)
    #print add_feats
    new_inst = existing_insts
    for feat in add_feats:  # add these features and give them zeros
        if feat in existing_insts.keys():
            print "ERROR!"
        new_inst[feat] = 0.0
    return new_inst




"""
Calculate mean and standard deviation of data x[]:
    mean = {\sum_i x_i \over n}
    std = sqrt(\sum_i (x_i - mean)^2 \over n-1)
From: https://www.physics.rutgers.edu/~masud/computing/WPark_recipes_in_python.html
"""
def meanstdv(x):
    from math import sqrt
    n, mean, std = len(x), 0, 0
    for a in x:
	mean = mean + a
    mean = mean / float(n)
    for a in x:
	std = std + (a - mean)**2
    std = sqrt(std / float(n-1))
    return mean, std

####
#### get parses from stanford corenlp
####
def split_sents(text):
    """ @note: calls Stanford's core nlp to split sentences only
        @return: list of sentences
    """
    sents = stanford_corenlp.split_sents(text) # returns an ArrayList
    #print sents
    #print sents.size()
    return sents


####
#### Stanford's CoreNLP tokenize
####
def get_tokens(text):
    """ @note: calls Stanford's core nlp to get tokens and dependencies
        @return: sentences objects, tokenized sentences, trees, and dependencies
    """
    tokenized_sentences = stanford_corenlp.get_tokens(text)
    return tokenized_sentences

####
#### tokenize the sent and return original text
####
def get_tokens_original_text(sent):
    tokens_obj = get_tokens(sent) # use CoreNLP to get POS
    #tokens_arr = tokens_obj[0]
    tokens = []
    for tokens_arr in tokens_obj:
        for token_item in tokens_arr:
            tokens.append(token_item['OriginalText'])
    return tokens


####
#### tokenize the sent and return tuples [(word1, pos1), (word2, pos2), ...]
####
def get_tokens_pos(sent):
    tokens_obj = get_tokens(sent) # use CoreNLP to get POS
    #tokens_arr = tokens_obj[0]
    # form an array of tuples (text, pos)
    tokens = []
    pos = []
    for tokens_arr in tokens_obj:
        for token_item in tokens_arr:
            pos.append(
                (token_item['OriginalText'],
                token_item['PartOfSpeech']))
            tokens.append(token_item['OriginalText'])
    return tokens, pos



def get_pos_to_file(sent, outfile):
    tokens, pos = get_tokens_pos(sent)
    for (w,p) in pos:
        outfile.write(w+'/'+p+' ')
    outfile.write('\n')


####
#### Get POS
####
def get_and_write_pos_doc(infilename, outfilename):
    open(outfilename, 'w') # clear the file since we need to append
    lines = open(infilename)
    for line in lines:
        outfile = open(outfilename, 'a')
        get_pos_to_file(line, outfile)



def penn2wn_dirs(indir, outdir):
    cmd = 'perl '+ \
            os.path.join(THISDIR,'convert-PENN-to-WN.pl')+ \
            ' --input_dir '+indir+ \
            ' --output_dir '+outdir
    print cmd
    p = subprocess.Popen(cmd, shell=True, \
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print p.communicate()[0]



####
#### Penn tagset
####
wnTag = {
    'JJ':'a',
    'JJR':'a',
    'JJS':'a',
    'CD':'a',
    'RB':'r',
    'RBR':'r',
    'RBS':'r',
    'RP':'r',
    'WRB':'CLOSED',
    'CC':'CLOSED',
    'IN':'r',
    'DT':'CLOSED',
    'PDT':'CLOSED',
    'CC':'CLOSED',
    'PRP$':'CLOSED',
    'PRP':'CLOSED',
    'WDT':'CLOSED',
    'WP$':'CLOSED',
    'NN':'n',
    'NNS':'n',
    'NNP':'n',
    'NNPS':'n',
    'PRP':'CLOSED',
    'WP':'CLOSED',
    'EX':'CLOSED',
    'VBP':'v',
    'VB':'v',
    'VBD':'v',
    'VBG':'v',
    'VBN':'v',
    'VBZ':'v',
    'VBP':'v',
    'MD':'v',
    'TO':'CLOSED',
    'POS':'undef',
    'UH':'CLOSED',
    '.':'undef',
    ':':'undef',
    ',':'undef',
    '_':'undef',
    '$':'undef',
    '(':'undef',
    ')':'undef',
    "\"":'undef',
    'FW':'NOINFO',
    'SYM':'undef',
    'LS':'undef',
}

def penn2wn(ptag):
    if ptag in wnTag:
        return wnTag[ptag]
    else:
        return 'undef'


####
#### SentiWordNet
####

f = open(SENTIWORDNET, 'rU')
sentiwordnet = f.read().split('\n')
f.close()

temp = {}
SWN = {}

for line in sentiwordnet:
    if not line: continue
    if line[0] == "#": continue
    arr = line.split('\t')
    pos = float(arr[2])
    neg = float(arr[3])
    score = pos - neg
    words = arr[4].split(" ")
    for w in words:
        w_n = w.split("#")
        w_n[0]+="#"+arr[0]  # <word>#<pos>
        index = int(w_n[1])-1
        #if (w_n[0]=="happy#a"):
        #    print w_n[1]
        # if word already exists, add to vector
        if (w_n[0] in temp):
            v = temp[w_n[0]]
            if index > len(v):
                for i in range(len(v), index):
                    v.append(0.0)
            v.insert(index, score)
            #if (w_n[0]=="happy#a"):
            #    print v
            temp[w_n[0]] = v
        else:
            v = []
            for i in range(index):
                v.append(0.0)
            v.insert(index, score)
            temp[w_n[0]]=v


temp_keys = temp.keys()
for key in temp_keys:
    v = temp[key]
    #if (key=="happy#a"):
    #    print v
    score = 0
    sum = 0
    for i in range(len(v)):
        score += (1/(i+1))*v[i]
    for i in range(1, len(v)+1):
        sum += 1/i
    score /= sum
    SWN[key]=score
    #print key, score


def swn_extract(word, pos):
    key = word+"#"+pos
    if key in SWN:
        return SWN[key]
    else:
        return 'null'


####
#### replace certain punctuations
####
def proc_str(str):
    str2 = re.sub('\s','',str)     # avoid spaces
    str2 = re.sub('\'','',str2)     # get rid of apostrophe
    str2 = re.sub('\.','',str2)     # get rid of period
    str2 = re.sub('\&','',str2)    # avoid ampersand
    str2 = re.sub(',','',str2)     # avoid comma
    str2 = re.sub('\%','',str2)
    str2 = re.sub('"','',str2)
    return str2

####
#### replace punctuations with words
####
def proc_str_words(str):
    str2 = re.sub('\s','SPACE',str)
    str2 = re.sub('\'','SQUOTE',str2)
    str2 = re.sub('\.','PERIOD',str2)
    str2 = re.sub('\&','AMP',str2)
    str2 = re.sub(',','COMMA',str2)
    str2 = re.sub('\%','PERCENT',str2)
    str2 = re.sub('"','DQUOTE',str2)
    #str2 = re.sub('==','=',str2)
    #if str2[0]=='-':
    #    str2 = str2[1:]
    #if str2[-1]=='-':
    return str2

###
###  create directory if doesn't exist
###
def create_directory(dirpath):
    # create directory if doesn't exist

    # split path and check parent dir to see if it exists
    path, folder = os.path.split(dirpath)

    if path and not os.path.isdir(path):
        create_directory(path)

    cmd = 'mkdir '+dirpath
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT)
    lines = p.stdout.readlines()


if __name__ == '__main__':
    #For testing
    print "hello, you've reached common tools"

